/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import capri.fabiani.model.rest.IdentityDTO;

/**
 * The Interface ProductResponseService.
 */
//It's supposed to be on the agent node : )
public interface ProductResponseService {
	
	/**
	 * Reply.
	 *
	 * @param response the response
	 */
	public void reply(IdentityDTO response);
}
