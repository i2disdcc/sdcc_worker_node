/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.DTO;

/**
 * The Interface BusinessService.
 */
public interface BusinessService {
	
	/**
	 * Delete product.
	 *
	 * @param datumCredentials the datum credentials
	 */
	public DTO deleteProduct(BusinessDTO datumCredentials);
	
	/**
	 * Gets the product.
	 *
	 * @param productCredentials the product credentials
	 * @return the product
	 */
	public DTO getProduct(BusinessDTO productCredentials);
	
	/**
	 * Register product.
	 *
	 * @param newProduct the new product
	 */
	public DTO registerProduct(BusinessDTO newProduct);
	
	/**
	 * Request product.
	 *
	 * @param productClass the product class
	 */
	public DTO requestProduct(int productClass);
	
	/**
	 * Update product.
	 *
	 * @param credentialsAndData the credentials and data
	 */
	public DTO updateProduct(BusinessDTO credentialsAndData);
	
}
