/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import capri.fabiani.model.rest.RetirementRequestDTO;
import capri.fabiani.model.rest.RetirementVoteDTO;

/**
 * The Interface RetirementService.
 */
public interface RetirementService {
	
	/**
	 * May i retire.
	 *
	 * @param dto the dto
	 */
	public void mayIRetire(RetirementRequestDTO dto);
	
	/**
	 * I am done.
	 *
	 * @param retireeKey the retiree key
	 */
	public void iAmDone(long retireeKey);
	
	/**
	 * You may retire.
	 *
	 * @param dto the dto
	 */
	public void youMayRetire(RetirementVoteDTO dto);
}
