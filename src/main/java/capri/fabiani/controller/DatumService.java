/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import java.util.List;
import capri.fabiani.model.rest.BusinessDTO;

/**
 * The Interface DatumService.
 */
public interface DatumService {
	
	/**
	 * Creates the.
	 *
	 * @param dto the dto
	 * @return the business dto
	 */
	public BusinessDTO create(BusinessDTO dto);
	
	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	public void delete(long id);
	
	/**
	 * Delete.
	 *
	 * @param dto the dto
	 */
	public void delete(BusinessDTO dto);
	
	/**
	 * Save bulk.
	 *
	 * @param dtoList the dto list
	 */
	public void saveBulk(List<BusinessDTO> dtoList);
	
	/**
	 * Update.
	 *
	 * @param dto the dto
	 */
	public void update(BusinessDTO dto);
	
	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the business dto
	 */
	public BusinessDTO findById(long id);
	
	/**
	 * Find by id range.
	 *
	 * @param startId the start id
	 * @param endId the end id
	 * @return the list
	 */
	public List<BusinessDTO> findByIdRange(long startId, long endId);
	
	/**
	 * Delete by range.
	 *
	 * @param startId the start id
	 * @param endId the end id
	 * @return the long
	 */
	public Long deleteByRange(long startId, long endId);
	
	/**
	 * Find by product class.
	 *
	 * @param classId the class id
	 * @return the list
	 */
	public List<BusinessDTO> findByProductClass(long classId);
	
	/**
	 * Find by product class and owner.
	 *
	 * @param productClass the product class
	 * @param owner the owner
	 * @return the list
	 */
	public List<BusinessDTO> findByProductClassAndOwner(long productClass, String owner);

	/**
	 * Count by id range.
	 *
	 * @param startId the start id
	 * @param endId the end id
	 * @return the long
	 */
	public Long countByIdRange(long startId, long endId);

	/**
	 * Find median.
	 *
	 * @param startId the start id
	 * @param endId the end id
	 * @return the long
	 */
	public long findMedian(long startId, long endId);
}
