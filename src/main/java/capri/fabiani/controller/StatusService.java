/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import capri.fabiani.model.rest.StatusDTO;
import capri.fabiani.model.rest.StatusExtDTO;

/**
 * The Interface StatusService.
 */
public interface StatusService {
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public StatusDTO getStatus();
	
	public StatusExtDTO getStatusExtended();
}
