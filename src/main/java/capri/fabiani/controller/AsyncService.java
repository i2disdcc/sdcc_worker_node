/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

/**
 * The Interface AsyncService.
 */
public interface AsyncService {
	
	/**
	 * Run.
	 *
	 * @param runnable the runnable
	 */
	public void run(final Runnable runnable);
}
