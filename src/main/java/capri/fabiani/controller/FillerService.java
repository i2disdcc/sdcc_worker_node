/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import capri.fabiani.rest.FillDTO;

/**
 * The Interface FillerService.
 */
public interface FillerService {
 
 /**
  * Fill keys.
  *
  * @param dto the dto
  * @return the response entity
  */
 public ResponseEntity<String> fillKeys(@RequestBody FillDTO dto);

}
