/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import java.net.URI;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import com.google.common.base.Throwables;

import capri.fabiani.components.WorkerNode;
import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.DatumService;
import capri.fabiani.controller.MasterPersistenceService;
import capri.fabiani.controller.MasterPersistenceService.CommitMasterMsgType;
import capri.fabiani.controller.MasterPersistenceService.CommitOpType;
import capri.fabiani.controller.SlavePersistenceService;
import capri.fabiani.model.RedundancyManagementData;
import capri.fabiani.model.SlaveCommitMgtDatum;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.MasterCommitDTO;
import capri.fabiani.model.rest.SlaveCommitDTO;
import capri.fabiani.model.rest.SlaveInitPrimeDTO;
import capri.fabiani.model.rest.SlavePutInChargeDTO;
import capri.fabiani.model.rest.SlavePutInChargeResponseDTO;
import capri.fabiani.model.rest.TransportKeysDTO;
import capri.fabiani.threads.HttpRequestDelegateRunnable;
import capri.fabiani.util.InternalServerErrorException;
import capri.fabiani.util.Logger;

/**
 * The Class SlavePersistenceServiceImplementation.
 */
@Service
public class SlavePersistenceServiceImplementation implements SlavePersistenceService {

	/** The self. */
	@Autowired
	private WorkerNode self;

	/** The datum service. */
	@Autowired
	private DatumService datumService;

	/**
	 * Instantiates a new slave persistence service implementation.
	 */
	public SlavePersistenceServiceImplementation() {

	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.controller.SlavePersistenceService#putInCharge(capri.fabiani.model.rest.SlavePutInChargeDTO)
	 */
	@Override
	public SlavePutInChargeResponseDTO putInCharge(SlavePutInChargeDTO boss) {
		boolean isToEmployersWrUnlock = false;
		boolean isToDismissUnlock = false;
		boolean isToInitWrUnlock = false;
		boolean isToTimestampUnlock = false;

		Map<Long, RedundancyManagementData> employersTable;
		RedundancyManagementData mgtData = null;

		long minVal = Constants.MAX_VALUE;
		long bossMin = boss.getLowEnd(), bossMax = boss.getKey();
		SlavePutInChargeResponseDTO responseDto;

		try {
			responseDto = new SlavePutInChargeResponseDTO();

			self.employersWrLock();
			isToEmployersWrUnlock = true;

			employersTable = self.getEmployersTable();

			// Insert the new/updated entry
			mgtData = employersTable.get(bossMax);

			if (mgtData == null) {
				Logger.status("Put in charge by node " + boss.getKey() + ", ip " + boss.getAddress()
						+ " for the first time.");

				mgtData = new RedundancyManagementData();
				mgtData.setKey(bossMax);
				
				//This mgtData is not yet in the employersTable, so there's no risk for race conditions.
				mgtData.setTimestamp(System.currentTimeMillis());

				employersTable.put(bossMax, mgtData);

				if (bossMax >= bossMin) {
					responseDto.setCount(bossMax - bossMin + 1);
				} else {
					// From 0 to bossMax, and from bossMin to
					// Constants.MAX_VALUE
					responseDto.setCount(Constants.MAX_VALUE - bossMin + 1 + bossMax + 1);
				}
				responseDto.setLowerEnd(bossMin);
				responseDto.setUpperEnd(bossMax);

			} else {
				Logger.status("Put in charge by node " + boss.getKey() + ", ip " + boss.getAddress() + " again.");
				// If it was in dismissing state, clear it.

				mgtData.dismissLock();
				isToDismissUnlock = true;

				mgtData.setDismissing(false);

				mgtData.dismissUnlock();
				isToDismissUnlock = false;
		
				

				minVal = mgtData.getLowEnd();

				if (bossMax >= bossMin) {
					if (bossMin < minVal) {
						responseDto.setCount(minVal - bossMin);
					} else {
						responseDto.setCount(0);

						mgtData.initWrLock();
						isToInitWrUnlock = true;

						mgtData.initReboot();
						mgtData.initPrime(0);
						mgtData.initAdvance(0);

						mgtData.initWrUnlock();
						isToInitWrUnlock = false;
					}
				} else {
					if (minVal < bossMax) {
						responseDto.setCount(Constants.MAX_VALUE - bossMin + 1 + minVal + 1);
					} else {
						if (bossMin < minVal) {
							responseDto.setCount(minVal - bossMin);
						} else {
							responseDto.setCount(0);

							mgtData.initWrLock();
							isToInitWrUnlock = true;

							mgtData.initPrime(0);
							mgtData.initAdvance(0);

							mgtData.initWrUnlock();
							isToInitWrUnlock = false;
						}
					}
				}
				responseDto.setLowerEnd(bossMin);
				responseDto.setUpperEnd(minVal - 1);
			}
			mgtData.setAddress(boss.getAddress());
			mgtData.setLowEnd(boss.getLowEnd());

			if (responseDto.getCount() > 0) {
				mgtData.initWrLock();
				isToInitWrUnlock = true;

				mgtData.initReboot();

				mgtData.initWrUnlock();
				isToInitWrUnlock = false;
			}

			mgtData.timestampLock();
			isToTimestampUnlock = true;

			mgtData.setTimestamp(System.currentTimeMillis());

			mgtData.timestampUnlock();
			isToTimestampUnlock = false;

			employersTable = null;
			mgtData = null;

			self.employersWrUnlock();
			isToEmployersWrUnlock = false;

			return responseDto;

		} catch (Exception e) {
			if (isToEmployersWrUnlock)
				self.employersWrUnlock();
			if (isToDismissUnlock)
				mgtData.dismissUnlock();
			if (isToInitWrUnlock)
				mgtData.initWrUnlock();
			if (isToTimestampUnlock) {
				mgtData.setTimestamp(System.currentTimeMillis());
				mgtData.timestampUnlock();
			}

			Logger.error(Throwables.getStackTraceAsString(e));

			throw new InternalServerErrorException();
		}

	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.SlavePersistenceService#dismiss(long)
	 */
	@Override
	/**
	 * This function marks a RedundancyManagementData object for removal. This
	 * shall be removed when all pending commits are terminated by the Daemon.
	 */
	public void dismiss(long bossKey) {

		boolean isToEmployersWrUnlock = false;
		boolean isToDismissUnlock = false;
		RedundancyManagementData mgtData = null;

		Logger.status("Being dismissed by node " + bossKey);

		try {
			self.employersWrLock();
			isToEmployersWrUnlock = true;

			mgtData = self.getEmployersTable().get(bossKey);
			if (mgtData != null) {
				mgtData.dismissLock();
				isToDismissUnlock = true;

				mgtData.setDismissing(false);

				mgtData.dismissUnlock();
				isToDismissUnlock = false;
			}

			self.employersWrUnlock();
			isToEmployersWrUnlock = false;
		} catch (Exception e) {
			if (isToEmployersWrUnlock)
				self.employersWrUnlock();
			if (isToDismissUnlock)
				mgtData.dismissUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			throw new InternalServerErrorException();
		}

	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.SlavePersistenceService#commit(capri.fabiani.model.rest.MasterCommitDTO)
	 */
	@Override
	public void commit(MasterCommitDTO dto) {
		CommitMasterMsgType type = dto.getMsgType();

		boolean isToPendingCommitsUnlock = false;
		boolean isToEmployersRdUnlock = false;
		boolean isToDismissUnlock = false;
		boolean isToIdentityRdUnlock = false;
		boolean isToInitRdUnlock = false;
		boolean isToTimestampUnlock = false;

		SlaveCommitMgtDatum mgtDatum = null;
		SlaveCommitMgtDatum.SlaveState state;
		BusinessDTO businessDto;
		MasterPersistenceService.CommitOpType opType;

		RedundancyManagementData redMgtData = null;
		final long lowEnd, highEnd;
		String masterIp;

		int commitIndex = dto.getCommitIndex();

		String url;
		URI uri;
		RequestEntity<SlaveCommitDTO> request;
		Runnable runnable;
		Thread delegate;

		SlaveCommitDTO slaveDto;

		Logger.status("Commit " + dto.getCommitIndex() + " by node " + dto.getMasterKey() + " - "
				+ dto.getMsgType().toString());

		try {

			self.employersRdLock();
			isToEmployersRdUnlock = true;

			redMgtData = self.getEmployersTable().get(dto.getMasterKey());
			lowEnd = redMgtData.getLowEnd();
			highEnd = redMgtData.getKey();

			self.employersRdUnlock();
			isToEmployersRdUnlock = false;

			// If NullPointerException, it's handled by the try/catch pair
			masterIp = redMgtData.getAddress();

			switch (type) {

			case GLOBAL_ABORT:
				// Drop the commit by removing mgt data
				redMgtData.pendingCommitsLock();
				isToPendingCommitsUnlock = true;

				mgtDatum = redMgtData.getPendingCommits().get(commitIndex);

				if (mgtDatum != null && mgtDatum.getState() != SlaveCommitMgtDatum.SlaveState.PRECOMMIT) {
					redMgtData.getPendingCommits().remove(commitIndex);

					// Send ack
					slaveDto = new SlaveCommitDTO();
					slaveDto.setMsgType(CommitSlaveMsgType.ACK);

					self.identityRdLock();
					isToIdentityRdUnlock = true;

					slaveDto.setSlaveKey(self.getIdentity().getKey());

					self.identityRdUnlock();
					isToIdentityRdUnlock = false;

					url = Constants.HTTP_PRIMER + masterIp + Constants.APP_NAME_PATH + Constants.COMMIT_MASTER
							+ commitIndex + "/";
					uri = new URI(url);
					request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(slaveDto);

					runnable = new HttpRequestDelegateRunnable(request);
					delegate = new Thread(runnable);
					delegate.start();
				}

				redMgtData.pendingCommitsUnlock();
				isToPendingCommitsUnlock = false;

				break;

			case GLOBAL_COMMIT:

				redMgtData.pendingCommitsLock();
				isToPendingCommitsUnlock = true;

				// If a NullPointerException gets thrown, it means this message
				// was not meant to reach this node.
				mgtDatum = redMgtData.getPendingCommits().get(commitIndex);

				state = mgtDatum.getState();
				businessDto = mgtDatum.getBusinessData();
				opType = mgtDatum.getCommitOpType();

				mgtDatum = null;

				redMgtData.pendingCommitsUnlock();
				isToPendingCommitsUnlock = false;

				if (state != SlaveCommitMgtDatum.SlaveState.PRECOMMIT) {
					// Nothing to do
					break;
				}

				// Finalize commit
				switch (opType) {
				case CREATE:
					datumService.create(businessDto);
					break;
				case DELETE:
					datumService.delete(businessDto.getKey());
					break;
				case UPDATE:
					datumService.update(businessDto);
					break;
				}

				// Send ack to master
				slaveDto = new SlaveCommitDTO();
				slaveDto.setMsgType(CommitSlaveMsgType.ACK);

				self.identityRdLock();
				isToIdentityRdUnlock = true;

				slaveDto.setSlaveKey(self.getIdentity().getKey());

				self.identityRdUnlock();
				isToIdentityRdUnlock = false;

				url = Constants.HTTP_PRIMER + masterIp + Constants.APP_NAME_PATH + Constants.COMMIT_MASTER + commitIndex
						+ "/";
				uri = new URI(url);
				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(slaveDto);

				runnable = new HttpRequestDelegateRunnable(request);
				delegate = new Thread(runnable);
				delegate.start();

				// Remove mgtData
				redMgtData.pendingCommitsLock();
				isToPendingCommitsUnlock = true;

				redMgtData.getPendingCommits().remove(commitIndex);

				redMgtData.pendingCommitsUnlock();
				;
				isToPendingCommitsUnlock = false;

				break;

			case PREPARE_COMMIT:
				redMgtData.pendingCommitsLock();
				isToPendingCommitsUnlock = true;

				mgtDatum = redMgtData.getPendingCommits().get(commitIndex);

				if (mgtDatum.getState() != SlaveCommitMgtDatum.SlaveState.READY) {
					redMgtData.pendingCommitsUnlock();
					isToPendingCommitsUnlock = false;
					break;
				}

				mgtDatum.setState(SlaveCommitMgtDatum.SlaveState.PRECOMMIT);

				mgtDatum = null;

				redMgtData.pendingCommitsUnlock();
				isToPendingCommitsUnlock = false;

				// Send READY_COMMIT
				slaveDto = new SlaveCommitDTO();
				slaveDto.setMsgType(CommitSlaveMsgType.READY_COMMIT);

				self.identityRdLock();
				isToIdentityRdUnlock = true;

				slaveDto.setSlaveKey(self.getIdentity().getKey());

				self.identityRdUnlock();
				isToIdentityRdUnlock = false;

				url = Constants.HTTP_PRIMER + masterIp + Constants.APP_NAME_PATH + Constants.COMMIT_MASTER + commitIndex
						+ "/";
				uri = new URI(url);
				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(slaveDto);

				runnable = new HttpRequestDelegateRunnable(request);
				delegate = new Thread(runnable);
				delegate.start();

				break;

			case VOTE_REQUEST:

				boolean mustAbort;
				BusinessDTO payload = dto.getDatum(), dummy;
				long requestedKey = payload.getKey();

				slaveDto = new SlaveCommitDTO();

				self.identityRdLock();
				isToIdentityRdUnlock = true;

				slaveDto.setSlaveKey(self.getIdentity().getKey());

				self.identityRdUnlock();
				isToIdentityRdUnlock = false;

				url = Constants.HTTP_PRIMER + masterIp + Constants.APP_NAME_PATH + Constants.COMMIT_MASTER + commitIndex
						+ "/";
				uri = new URI(url);

				redMgtData.dismissLock();
				isToDismissUnlock = true;

				mustAbort = redMgtData.isDismissing();

				redMgtData.dismissUnlock();
				isToDismissUnlock = false;

				if (mustAbort) {
					Logger.warning("Cannot advance commit, replica is dismissing.");
					slaveDto.setMsgType(CommitSlaveMsgType.VOTE_ABORT);

					request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(slaveDto);

					runnable = new HttpRequestDelegateRunnable(request);
					delegate = new Thread(runnable);
					delegate.start();
					break;
				}

				redMgtData.initRdLock();
				isToInitRdUnlock = true;

				mustAbort = !redMgtData.isReady();

				redMgtData.initRdUnlock();
				isToInitRdUnlock = false;

				if (mustAbort) {
					Logger.warning("Cannot advance commit, replica not ready.");
					slaveDto.setMsgType(CommitSlaveMsgType.VOTE_ABORT);

					request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(slaveDto);

					runnable = new HttpRequestDelegateRunnable(request);
					delegate = new Thread(runnable);
					delegate.start();
					break;
				}

				redMgtData.pendingCommitsLock();
				isToPendingCommitsUnlock = true;

				// If a NullPointerException gets thrown, it means this message
				// was not meant to reach this node.
				mgtDatum = redMgtData.getPendingCommits().get(commitIndex);

				redMgtData.pendingCommitsUnlock();
				isToPendingCommitsUnlock = false;

				if (mgtDatum != null) {
					// Nothing to do, ignore message
					break;
				}

				// Do not accept new transaction if the requested key does not
				// fall within the proper range
				if ((lowEnd <= highEnd && !(lowEnd <= requestedKey && requestedKey <= highEnd)) || // Range
																								// not
																								// overlapping
																								// the
																								// limit
																								// region
						(lowEnd > highEnd && !(requestedKey >= lowEnd || requestedKey <= highEnd)) // range
																									// overlapping
																									// the
																									// limit
																									// region
				) {
					Logger.warning("Cannot advance commit, requested key out of range.");
					slaveDto.setMsgType(CommitSlaveMsgType.VOTE_ABORT);

					request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(slaveDto);

					runnable = new HttpRequestDelegateRunnable(request);
					delegate = new Thread(runnable);
					delegate.start();
					break;
				}

				opType = dto.getOpType();

				dummy = datumService.findById(requestedKey);

				// Abort transaction is any of the following conditions on the
				// datum holds.
				if (opType == null || // Invalid op type
						(dummy != null && ((opType == CommitOpType.CREATE)
								|| !(dummy.getOwner().equals(payload.getOwner()))))
						|| // Create an already existing key, or modify one with
							// inappropriate credentials.
						(dummy == null && opType != CommitOpType.CREATE)) { // Modify
																			// a
																			// non-existent
																			// key

					Logger.warning("Cannot advance commit, " + (opType == null ? "nullOp" : opType.toString()) + ", " +
					(dummy==null ? "nullDummy" : "dummy: " + dummy.toString()) + ", payload owner: " + payload.getOwner());

					slaveDto.setMsgType(CommitSlaveMsgType.VOTE_ABORT);

					request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(slaveDto);

					runnable = new HttpRequestDelegateRunnable(request);
					delegate = new Thread(runnable);
					delegate.start();
					break;
				}

				// Prepare SlaveCommitMgtDatum
				mgtDatum = new SlaveCommitMgtDatum(opType, dto.getCommitIndex());
				mgtDatum.setBusinessData(payload);
				mgtDatum.setState(SlaveCommitMgtDatum.SlaveState.READY);
				// We'll set the timestamp right before sending the reply

				// Prepare to send VOTE_COMMIT
				slaveDto.setMsgType(CommitSlaveMsgType.VOTE_COMMIT);

				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(slaveDto);

				runnable = new HttpRequestDelegateRunnable(request);
				delegate = new Thread(runnable);

				redMgtData.pendingCommitsLock();
				isToPendingCommitsUnlock = true;

				// Commit requests with duplicate commit indexes shall be
				// ignored (first to get the lock wins, "duplicates" get
				// ignored)
				if (redMgtData.getPendingCommits().get(commitIndex) == null) {
					redMgtData.getPendingCommits().put(commitIndex, mgtDatum); // Add
																				// SlaveCommitMgtDatum
					mgtDatum.setLastHeard(System.currentTimeMillis()); // Add
																		// LastHeard
					delegate.start(); // Send VOTE_COMMIT
				}

				redMgtData.pendingCommitsUnlock();
				isToPendingCommitsUnlock = false;

				break;

			}

			Logger.status("State advance for commit " + dto.getCommitIndex() + ", master " + dto.getMasterKey());

			redMgtData.timestampLock();
			isToTimestampUnlock = true;

			redMgtData.setTimestamp(System.currentTimeMillis());

			redMgtData.timestampUnlock();
			isToTimestampUnlock = false;
		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToPendingCommitsUnlock)
				redMgtData.pendingCommitsUnlock();
			if (isToDismissUnlock)
				redMgtData.dismissUnlock();
			if (isToInitRdUnlock)
				redMgtData.initRdUnlock();
			if (isToTimestampUnlock) {
				redMgtData.setTimestamp(System.currentTimeMillis());
				redMgtData.timestampUnlock();
			}
			if (isToEmployersRdUnlock)
				self.employersRdUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));

			throw new InternalServerErrorException();
		}
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.SlavePersistenceService#getProduct(capri.fabiani.model.rest.BusinessDTO, long)
	 */
	@Override
	public void getProduct(BusinessDTO productCredentials, long masterId) {
		boolean isToTimestampUnlock = false;
		boolean isToEmployersRdUnlock = false;

		long low, requestedKey;
		boolean wrongRange = false;

		RedundancyManagementData redMgtData = null;

		BusinessDTO datumDto;

		String url;
		URI uri;
		RequestEntity<BusinessDTO> request;
		Runnable runnable;
		Thread delegate;

		Logger.status("Requested product " + productCredentials.getKey() + ", owner " + productCredentials.getOwner()
				+ " via master " + masterId);
		try {
			self.employersRdLock();
			isToEmployersRdUnlock = true;

			redMgtData = self.getEmployersTable().get(masterId);
			low = redMgtData.getLowEnd();

			self.employersRdUnlock();
			isToEmployersRdUnlock = false;

			requestedKey = productCredentials.getKey();

			if (low > masterId) {
				wrongRange = requestedKey < low && requestedKey > masterId;
			} else {
				wrongRange = requestedKey < low || requestedKey > masterId;
			}

			if (wrongRange) {
				Logger.warning("Requested key (GET) out of range.");
				datumDto = new BusinessDTO();
				datumDto.setError(Constants.ERROR_SLAVE_INVALID_RANGE);
			} else {
				datumDto = datumService.findById(requestedKey);

				// If credentials do not match
				if(datumDto == null) {
					Logger.warning("Requested key (GET) not found.");
					datumDto = new BusinessDTO();
					datumDto.setError(Constants.ERROR_NOT_FOUND_GET);
				}
				else if (!datumDto.getOwner().equals(productCredentials.getOwner())) {
					Logger.warning("Invalid credentials (GET)");
					datumDto = new BusinessDTO();
					datumDto.setError(Constants.ERROR_SLAVE_INVALID_CREDENTIALS);
				}
			}

			datumDto.setRequestId(productCredentials.getRequestId());
			
			url = productCredentials.getResponseURL();
			uri = new URI(url);
			request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(datumDto);
			runnable = new HttpRequestDelegateRunnable(request);
			delegate = new Thread(runnable);
			delegate.start();

			redMgtData.timestampLock();
			isToTimestampUnlock = true;

			redMgtData.setTimestamp(System.currentTimeMillis());

			redMgtData.timestampUnlock();
			isToTimestampUnlock = false;

		} catch (Exception e) {
			if (isToEmployersRdUnlock)
				self.employersRdUnlock();
			if (isToTimestampUnlock) {
				redMgtData.setTimestamp(System.currentTimeMillis());
				redMgtData.timestampUnlock();
			}

			Logger.error(Throwables.getStackTraceAsString(e));
			throw new InternalServerErrorException();
		}

	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.SlavePersistenceService#requestProduct(int, long)
	 */
	@Override
	public void requestProduct(int productClass, long masterId) {
		// TODO Auto-generated method stub
		// TODO future developments
	}

	/**
	 * Timeouts need to be dealt with at a higher level.
	 *
	 * @param dto the dto
	 * @return the string
	 */
	@Override
	public String transferKeys(TransportKeysDTO dto) {
		boolean isToEmployersRdUnlock = false;
		boolean isToInitWrUnlock = false;
		boolean isToTimestampUnlock = false;

		RedundancyManagementData redMgtData = null;
		int requestedIndex, receivedIndex;

		Logger.status("Being primed by node " + dto.getIdSender() + ", message " + dto.getMessageIndex());

		try {

			self.employersRdLock();
			isToEmployersRdUnlock = true;

			redMgtData = self.getEmployersTable().get(dto.getIdSender());

			self.employersRdUnlock();
			isToEmployersRdUnlock = false;

			if (redMgtData == null) {
				Logger.error("RedundancyManagementData not found for master " + dto.getIdSender());
				throw new Exception();
			}

			redMgtData.timestampLock();
			isToTimestampUnlock = true;

			redMgtData.initWrLock();
			isToInitWrUnlock = true;

			requestedIndex = redMgtData.getInitNextMsgIndex();
			receivedIndex = dto.getMessageIndex();

			// No data lost.
			if (receivedIndex <= requestedIndex) {
				// New data
				if (receivedIndex == requestedIndex) {
					datumService.saveBulk(dto.getDatas());
					redMgtData.initAdvance(receivedIndex);
				}
				redMgtData.setTimestamp(System.currentTimeMillis());
			} else { // Something went wrong and at least one message was lost.
				throw new Exception();
			}

			redMgtData.initWrUnlock();
			isToInitWrUnlock = false;

			redMgtData.timestampUnlock();
			isToTimestampUnlock = false;
		} catch (Exception e) {
			if (isToEmployersRdUnlock)
				self.employersRdUnlock();
			if (isToInitWrUnlock)
				redMgtData.initWrUnlock();
			if (isToTimestampUnlock) {
				redMgtData.setTimestamp(System.currentTimeMillis());
				redMgtData.timestampUnlock();
			}

			Logger.error(Throwables.getStackTraceAsString(e));

			return ""; // TODO Better error message?
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.SlavePersistenceService#transferInit(capri.fabiani.model.rest.SlaveInitPrimeDTO, long)
	 */
	@Override
	public String transferInit(SlaveInitPrimeDTO dto, long masterId) {
		boolean isToEmployersRdUnlock = false;
		boolean isToInitWrUnlock = false;
		boolean isToTimestampUnlock = false;

		RedundancyManagementData redMgtData = null;
		boolean ready;

		Logger.status("Initializing key transfer by master node " + masterId + ", " + dto.getNumMessages()
				+ " messages expected.");

		try {
			self.employersRdLock();
			isToEmployersRdUnlock = true;

			redMgtData = self.getEmployersTable().get(masterId);

			self.employersRdUnlock();
			isToEmployersRdUnlock = false;

			Logger.status("Initializing key transfer for master " + masterId);

			redMgtData.timestampLock();
			isToTimestampUnlock = true;

			redMgtData.initWrLock();
			isToInitWrUnlock = true;

			ready = redMgtData.isReady();

			// Can't initialize a transfer if the node's already ready.
			if (ready) {
				redMgtData.initWrUnlock();
				isToInitWrUnlock = false;

				Logger.warning("Replica already initialized.");

				throw new Exception();
			} else {

				Logger.status("Ready to receive keys.");

				redMgtData.initPrime(dto.getNumMessages());
			}

			redMgtData.initWrUnlock();
			isToInitWrUnlock = false;

			redMgtData.setTimestamp(System.currentTimeMillis());

			redMgtData.timestampUnlock();
			isToTimestampUnlock = false;

		} catch (Exception e) {
			if (isToEmployersRdUnlock)
				self.employersRdUnlock();
			if (isToInitWrUnlock)
				redMgtData.initWrUnlock();
			if (isToTimestampUnlock) {
				redMgtData.setTimestamp(System.currentTimeMillis());
				redMgtData.timestampUnlock();
			}

			Logger.error(Throwables.getStackTraceAsString(e));

			return ""; // TODO better generic error message?
		}

		return null;
	}

}
