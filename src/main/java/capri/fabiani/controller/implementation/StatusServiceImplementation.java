/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Throwables;

import capri.fabiani.components.WorkerNode;
import capri.fabiani.controller.DatumService;
import capri.fabiani.controller.StatusService;
import capri.fabiani.model.Identity;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.IdentityDTO;
import capri.fabiani.model.rest.StatusDTO;
import capri.fabiani.model.rest.StatusExtDTO;
import capri.fabiani.util.InternalServerErrorException;
import capri.fabiani.util.Logger;

/**
 * The Class StatusServiceImplementation.
 */
@Service
public class StatusServiceImplementation implements StatusService {

	/** The self. */
	@Autowired
	private WorkerNode self;
	
	/** The datum service. */
	@Autowired
	private DatumService datumService;
	
	/* (non-Javadoc)
	 * @see capri.fabiani.controller.StatusService#getStatus()
	 */
	@Override
	public StatusDTO getStatus() {
		boolean isToIdentityRdUnlock = false;
		boolean isToPredecessorUnlock = false;
		boolean isToFingerRdUnlock = false;
		boolean isToNextNodesRdUnlock = false;
		boolean isToRedundancyRdUnlock = false;
		boolean isToEmployersRdUnlock = false;
		
		StatusDTO dto = null;
		List<IdentityDTO> idList;
		Identity rawId;
		
		try {
			dto = new StatusDTO();
			
			self.identityRdLock();
			isToIdentityRdUnlock = true;
			
			rawId = self.getIdentity();
			if(rawId != null) {
				dto.setIdentity(new IdentityDTO(rawId));
				rawId = null;
			}
			
			self.identityRdUnlock();
			isToIdentityRdUnlock = false;
			
			self.predecessorRdLock();
			isToPredecessorUnlock = true;
			
			rawId= self.getPredecessor();
			if(rawId!= null) {
				dto.setPredecessor(new IdentityDTO(rawId));
				rawId = null;
			}
			
			self.predecessorRdUnlock();
			isToPredecessorUnlock = false;
			
			
			
			idList = new LinkedList<IdentityDTO>();
			
			self.fingerRdLock();
			isToFingerRdUnlock = true;

			for(Identity id : self.getFingerTable()) {
				idList.add(new IdentityDTO(id));
			}
			
			self.fingerRdUnlock();
			isToFingerRdUnlock = false;
			
			dto.setFingerTable(idList);
			
			
			
			idList = new LinkedList<IdentityDTO>();
			
			self.nextNodesRdLock();
			isToNextNodesRdUnlock = true;
			
			for(Identity id : self.getNextNodes()) {
				idList.add(new IdentityDTO(id));
			}
			
			self.nextNodesRdUnlock();
			isToNextNodesRdUnlock = false;
			
			dto.setNextNodes(idList);
			
			
						
			idList = new LinkedList<IdentityDTO>();
			
			self.redundancyRdLock();
			isToRedundancyRdUnlock = true;
			
			for(Identity id : self.getRedundancyTable()) {
				idList.add(new IdentityDTO(id));
			}
			
			self.redundancyRdUnlock();
			isToRedundancyRdUnlock = false;
			
			dto.setReplicas(idList);
			
			
			
			idList = new LinkedList<IdentityDTO>();
			
			self.employersRdLock();
			isToEmployersRdUnlock = true;
			
			for(Identity id : self.getEmployersTable().values()) {
				idList.add(new IdentityDTO(id));
			}
			
			self.employersRdUnlock();
			isToEmployersRdUnlock = false;
			
			dto.setMasters(idList);
			
			
			if(dto.getIdentity() != null && dto.getPredecessor() != null)
				dto.setKeyCount(datumService.countByIdRange(dto.getRangeLowEnd(), dto.getRangeHighEnd()));
			
			return dto;
		}
		catch(Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToPredecessorUnlock)
				self.predecessorRdUnlock();
			if (isToFingerRdUnlock)
				self.fingerRdUnlock();
			if (isToNextNodesRdUnlock)
				self.nextNodesRdUnlock();
			if (isToRedundancyRdUnlock)
				self.redundancyRdUnlock();
			if (isToEmployersRdUnlock)
				self.employersRdUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));

			throw new InternalServerErrorException();
		}
	}

	@Override
	public StatusExtDTO getStatusExtended() {
		StatusDTO preDto;
		StatusExtDTO dto;
		
		try {
			preDto = this.getStatus();
			dto = new StatusExtDTO();

			if (preDto != null) {
				dto.setFingerTable(preDto.getFingerTable());
				dto.setIdentity(preDto.getIdentity());
				dto.setKeyCount(preDto.getKeyCount());
				dto.setMasters(preDto.getMasters());
				dto.setNextNodes(preDto.getNextNodes());
				dto.setPredecessor(preDto.getPredecessor());
				dto.setReplicas(preDto.getReplicas());

				
				dto.setEntries(datumService.findByIdRange(dto.getRangeLowEnd(), dto.getIdentity().getKey()));
			}
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			throw new InternalServerErrorException();
		}

		return dto;
	}

}
