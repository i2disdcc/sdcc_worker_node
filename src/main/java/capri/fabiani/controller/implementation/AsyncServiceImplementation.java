/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import capri.fabiani.controller.AsyncService;

/**
 * The Class AsyncServiceImplementation.
 */
@Service
public class AsyncServiceImplementation implements AsyncService {

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.AsyncService#run(java.lang.Runnable)
	 */
	@Override
	@Async
	/**
	 * Asynchronously executes the argument runnable's run() method.
	 */
	public void run(Runnable runnable) {
		runnable.run();
	}

}
