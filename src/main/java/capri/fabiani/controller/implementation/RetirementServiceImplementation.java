/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import java.net.URI;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import com.google.common.base.Throwables;

import capri.fabiani.components.WorkerNode;
import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.FingerTableService;
import capri.fabiani.controller.RetirementService;
import capri.fabiani.model.Identity;
import capri.fabiani.model.rest.IdentityDTO;
import capri.fabiani.model.rest.RetirementRequestDTO;
import capri.fabiani.model.rest.RetirementVoteDTO;
import capri.fabiani.threads.HttpRequestDelegateRunnable;
import capri.fabiani.util.InternalServerErrorException;
import capri.fabiani.util.Logger;

/**
 * The Class RetirementServiceImplementation.
 */
@Service
public class RetirementServiceImplementation implements RetirementService {
	
	/** The self. */
	@Autowired
	private WorkerNode self;

	/** The ft service. */
	@Autowired
	private FingerTableService ftService;

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.RetirementService#mayIRetire(capri.fabiani.model.rest.RetirementRequestDTO)
	 */
	@Override
	public void mayIRetire(RetirementRequestDTO dto) {
		boolean isToRetirementUnlock = false;
		boolean isToIdentityRdUnlock = false;

		Identity myId;
		IdentityDTO forwardee;
		long myKey;
		Long retiree;

		boolean goAhead;

		RetirementVoteDTO voteDto;
		RequestEntity<RetirementVoteDTO> voteRequestEntity;

		RequestEntity<RetirementRequestDTO> retirementRequestEntity;

		String url;
		URI uri;
		Runnable runnable;
		Thread delegate;

		Logger.status("Received retirement request from node " + dto.getKey() + ", ip " + dto.getAddress());

		try {
			voteDto = new RetirementVoteDTO();

			self.retirementLock();
			isToRetirementUnlock = true;

			// Set retirement key if this node has not already voted
			retiree = self.getRetirementCandidateKey();
			if (retiree == null) {
				self.setRetirementCandidateKey(dto.getKey());
				self.setRetirementVotingTimestamp(System.currentTimeMillis());
				goAhead = true;

				Logger.status("May grant etirement permission to node " + dto.getKey());
			} else {
				goAhead = false;
				Logger.status("Vote in progress for node " + retiree + "; retirement permission denied to node "
						+ dto.getKey());
			}

			self.retirementUnlock();
			isToRetirementUnlock = false;

			self.identityRdLock();
			isToIdentityRdUnlock = true;

			myId = self.getIdentity();
			myKey = myId.getKey();
			voteDto.setAddress(myId.getAddress());
			voteDto.setKey(myKey);

			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			// Respond to the requesting node. If it's me, delegate the
			// "response" to another thread without passing through an HTTP
			// request.
			if (myKey == voteDto.getKey()) {
				// TODO This should be superfluous, as if the retirement
				// candidate is me, then it's not null, and goAhead is already
				// set to false

				// If I need a vote from myself, there are not enough nodes on
				// the ring.
				voteDto.setGranted(false);

				final RetirementVoteDTO runnDto = voteDto;
				voteDto = null;

				runnable = new Runnable() {

					@Override
					public void run() {
						youMayRetire(runnDto);
					}

				};
				delegate = new Thread(runnable);
				delegate.start();

				return;
			} else {
				Logger.status("Sending vote response to node " + dto.getKey() + "; may retire = " + goAhead);

				voteDto.setGranted(goAhead);
				url = Constants.HTTP_PRIMER + dto.getAddress() + Constants.APP_NAME_PATH + Constants.RETIREMENT_VOTE;
				uri = new URI(url);
				voteRequestEntity = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(voteDto);
				runnable = new HttpRequestDelegateRunnable(voteRequestEntity);
				delegate = new Thread(runnable);
				delegate.start();
			}

			dto.setShortCount(dto.getShortCount() - 1);

			// If this node has granted permission, forward the request if need
			// be.
			if (goAhead && dto.getShortCount() > 0) {

				// The ftService throws a handled Exception if anything happens
				myKey = (myKey + 1) % Constants.MODULUS;
				if (myKey < 0) {
					myKey = myKey + Constants.MODULUS;
				}

				forwardee = ftService.getForwardee(myKey);
				Logger.status("Forwarding vote request to successor: node " + forwardee.getKey() + "; ip "
						+ forwardee.getAddress());

				url = Constants.HTTP_PRIMER + forwardee.getAddress() + Constants.APP_NAME_PATH
						+ Constants.RETIREMENT_REQUEST;
				uri = new URI(url);
				retirementRequestEntity = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(dto);
				runnable = new HttpRequestDelegateRunnable(retirementRequestEntity);
				delegate = new Thread(runnable);
				delegate.start();
				// TODO check for a number of retries?
			}

		} catch (Exception e) {
			if (isToRetirementUnlock)
				self.retirementUnlock();
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
			throw new InternalServerErrorException();
		}
	}

	/**
	 * Clear vote, regardless of whether the former retiree was ultimately
	 * granted enough permissions to retire.
	 *
	 * @param retireeKey the retiree key
	 */
	@Override
	public void iAmDone(long retireeKey) {
		boolean isToRetirementUnlock = false;
		Long retiree;

		Logger.status("Node " + retireeKey + " clearing retirement request...");
		try {
			self.retirementLock();
			isToRetirementUnlock = true;

			retiree = self.getRetirementCandidateKey();
			if (retiree != null && retiree == retireeKey) {
				// Voting terminated
				self.setRetirementCandidateKey(null);

				Logger.status("Node " + retireeKey + " retirement request cleared.");
			} else {
				Logger.status("Node " + retireeKey + " not pending retirement vote.");
			}

			self.retirementUnlock();
			isToRetirementUnlock = false;
		} catch (Exception e) {
			if (isToRetirementUnlock)
				self.retirementUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
			throw new InternalServerErrorException();
		}
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.RetirementService#youMayRetire(capri.fabiani.model.rest.RetirementVoteDTO)
	 */
	@Override
	public void youMayRetire(RetirementVoteDTO dto) {
		boolean isToRetirementUnlock = false;
		boolean isToIdentityRdUnlock = false;

		long voterKey, myKey;
		Long retirementCandidateKey;
		Map<Long, Identity> retirementCouncil;

		Logger.status("Received retirement vote from node " + dto.getKey() + ", ip " + dto.getAddress() + "; vote = "
				+ dto.isGranted());

		try {
			voterKey = dto.getKey();

			self.identityRdLock();
			isToIdentityRdUnlock = true;

			myKey = self.getIdentity().getKey();

			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			self.retirementLock();
			isToRetirementUnlock = true;

			retirementCandidateKey = self.getRetirementCandidateKey();

			// Ignore if I am not trying to go into retirement
			if (retirementCandidateKey != null && retirementCandidateKey == myKey) {
				if (dto.isGranted()) {
					retirementCouncil = self.getRetirementCouncil();

					// Http requests -> TCP -> TCP handles duplicate packets ->
					// if we receive twice the same vote, the request somehow
					// circled around the ring due to lack of enough nodes
					if (retirementCouncil.containsKey(voterKey)) {
						// Will proceed to unlock and return. The thread waiting
						// on the retirement condition won't find enough votes
						// to pass its retirement
						self.retirementCondSignal();
					} else {
						retirementCouncil.put(voterKey, new Identity(dto.getAddress(), voterKey));
						if (retirementCouncil.size() >= Constants.RETIREMENT_QUORUM) {
							// Quota reached
							Logger.status("Permission to retire granted.");
							self.retirementCondSignal();
						}
					}

					retirementCouncil = null;
				} else {
					// Being signaled without having reached the required quota,
					// the vote did not pass.
					Logger.status("Permission to retire denied.");
					self.retirementCondSignal();
				}

			}
			self.retirementUnlock();
			isToRetirementUnlock = false;

		} catch (Exception e) {
			if (isToRetirementUnlock)
				self.retirementUnlock();
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
			throw new InternalServerErrorException();
		}
	}
}
