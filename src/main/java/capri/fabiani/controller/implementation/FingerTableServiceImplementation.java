/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import java.net.URI;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import com.google.common.base.Throwables;

import capri.fabiani.components.FingerTableDaemon;
import capri.fabiani.components.RedundancyDaemon;
import capri.fabiani.components.WorkerNode;
import capri.fabiani.model.Identity;
import capri.fabiani.components.WorkerNode.PendingTask;
import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.FingerDaemonService;
import capri.fabiani.controller.FingerTableService;
import capri.fabiani.model.rest.FingerFillTableDTO;
import capri.fabiani.model.rest.FingerFindPredecessorDTO;
import capri.fabiani.model.rest.FingerNextNodesDTO;
import capri.fabiani.model.rest.FingerNotifyPredecessorDTO;
import capri.fabiani.model.rest.FingerResponseDTO;
import capri.fabiani.model.rest.FingerNotifySuccessorDTO;
import capri.fabiani.model.rest.IdentityDTO;
import capri.fabiani.threads.FingerFillTableForwarderRunnable;
import capri.fabiani.threads.FingerNextNodesForwarderRunnable;
import capri.fabiani.threads.HttpRequestDelegateRunnable;
import capri.fabiani.util.FingerMalformedPacketException;
import capri.fabiani.util.FingerTaskNotFoundException;
import capri.fabiani.util.InternalServerErrorException;
import capri.fabiani.util.Logger;

/**
 * The Class FingerTableServiceImplementation.
 */
@Service
public class FingerTableServiceImplementation implements FingerTableService {

	/** The self. */
	@Autowired
	private WorkerNode self;

	/** The daemon service. */
	@Autowired
	private FingerDaemonService daemonService;

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerTableService#scanNextNodes(capri.fabiani.model.rest.FingerNextNodesDTO)
	 */
	@Override
	/**
	 * "Give me your Identity, and the one of your next K-1 nodes", K included
	 * in the FingerNextNodesDTO passed as argument.
	 * 
	 * @return Identity response with the current WorkerNode's Identity's key
	 *         and address, and with the receiced set shortcount.
	 */
	public String scanNextNodes(FingerNextNodesDTO taskDto) {
		String url;
		URI uri = null;
		HttpRequestDelegateRunnable runnable;
		Thread delegate;
		IdentityDTO src, myId;
		Identity predecessor;

		boolean yetToUnlock = false;
		boolean isToPredecessorRdUnlock = false;
		boolean isToIdentityRdUnlock = false;
		boolean isToPredecessorWrUnlock = false;

		// Respond to the node who requires its next nodes
		try {
			src = taskDto.getRequestSource();

			self.identityRdLock();
			isToIdentityRdUnlock = true;

			myId = new IdentityDTO(self.getIdentity());

			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			// The request looped, wake up.
			if (myId.getKey() == src.getKey()) {
				return null;
			}

			// Update predecessor only when this is the first message of the
			// chain
			// (Otherwise, if this node is replica to N other nodes, it would
			// lock the predecessor field in the WorkerNode structure N times)
			if (taskDto.isFirstSendFailed() && taskDto.getShortCount() == Constants.REDUNDANCY_LEVEL-1) {
				taskDto.setFirstSendFailed(false);

				self.predecessorRdLock();
				isToPredecessorRdUnlock = true;

				predecessor = self.getPredecessor();

				// Update if the predecessor has changed.
				if (predecessor == null || predecessor.getKey() != src.getKey()) {

					self.predecessorRdUnlock();
					isToPredecessorRdUnlock = false;

					Logger.status("Invoking rangeUpdated() in scanNextNodes, new predecessor: " + src.toString());

					daemonService.rangeUpdated(src);
				} else if (!predecessor.getAddress().equals(src.getAddress())) {
					self.predecessorRdUnlock();
					isToPredecessorRdUnlock = false;

					self.predecessorWrLock();
					isToPredecessorWrUnlock = true;

					predecessor.setAddress(src.getAddress());

					self.predecessorWrUnlock();
					isToPredecessorWrUnlock = false;
				} else {
					self.predecessorRdUnlock();
					isToPredecessorRdUnlock = false;
				}

			}

			url = Constants.HTTP_PRIMER + src.getAddress() + Constants.APP_NAME_PATH + Constants.FINGER_RESPONSE
					+ taskDto.getTaskId();
			uri = new URI(url);

			FingerResponseDTO dto;

			dto = new FingerResponseDTO(myId, new Integer(taskDto.getShortCount()));

			RequestEntity<FingerResponseDTO> request;

			request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(dto);

			runnable = new HttpRequestDelegateRunnable(request);
			delegate = new Thread(runnable);

			delegate.start();

			int count = taskDto.getShortCount();

			count--;

			// Forward the request to the next node if need be.
			if (count > 0) {

				taskDto.setShortCount(count);

				Thread forwarder = new Thread(new FingerNextNodesForwarderRunnable(self, taskDto));
				forwarder.start();

			}

			return null;

		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (yetToUnlock)
				self.fingerRdUnlock();
			if (isToPredecessorRdUnlock)
				self.predecessorRdUnlock();
			if (isToPredecessorWrUnlock)
				self.predecessorWrUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			return e.toString();
		}

	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerTableService#fillTable(capri.fabiani.model.rest.FingerFillTableDTO)
	 */
	@Override
	public String fillTable(FingerFillTableDTO dto) {
		boolean isToRdUnlockPred = false;
		boolean isToUnlockFingerRd = false;
		boolean isToIdentityRdUnlock = false;

		long key;
		long predKey;
		long idKey;

		Identity id;

		try {
			Logger.status("Received finger table refresh request by node " + dto.getRequestSource().getKey()
					+ ", address " + dto.getRequestSource().getAddress() + " for key " + dto.getReqKey());

			key = dto.getReqKey();
			if (key < 0 || key > Constants.MAX_VALUE) {
				Logger.warning("Finger table refresh requested key out of key space.");
				return null;
			}

			// Gets unlocked after the predecessor field has been locked and
			// unlocked, later on
			self.identityRdLock();
			isToIdentityRdUnlock = true;

			id = self.getIdentity();

			// Drop the packet if the requestSource is me
			if (id.getKey() == dto.getRequestSource().getKey()) {
				self.identityRdUnlock();
				isToIdentityRdUnlock = false;
				return null;
			}

			String url = null;
			URI uri = null;
			HttpRequestDelegateRunnable runnable;
			Thread delegate;

			// Check if the key is mine.
			idKey = id.getKey();

			RequestEntity<FingerResponseDTO> request;
			IdentityDTO srcDto = dto.getRequestSource();
			int ftIndex = dto.getFtIndex();
			FingerResponseDTO respDto;
			List<Integer> responseIndexList;

			self.predecessorRdLock();
			isToRdUnlockPred = true;

			// TODO remove
			if (self.getPredecessor() == null) {
				Logger.warning("Attention, null predecessor.");
			}

			predKey = self.getPredecessor().getKey();

			// If this dto bears this flag, we try contacting our predecessor.
			// Notice that this makes sense only for the first step (or few
			// first steps) of the finger table refreshing process,
			// and that this is a SYNCHRONOUS step in the execution (i.e. the
			// execution won't proceed until a response has been obtained)
			if (dto.isFirstSendFailed() && dto.getFtIndex() == 0) {
				// In any case, this flag is not usable for nodes past the one
				// who eventually enters this block.
				dto.setFirstSendFailed(false);

				// Our predecessor has fallen. We take charge of its
				// keys and mark the request source as our
				// predecessor.

				Logger.status(
						"rangeUpdated invoked in fillTable(), new predecessor: " + dto.getRequestSource().toString());
				daemonService.rangeUpdated(dto.getRequestSource());

			}

			uri = null;

			responseIndexList = new LinkedList<Integer>();

			// This key is mine IFF if falls between me and my predecessor's key
			// (idKey > predKey && key<=idKey && key>predKey) || //My
			// predecessor and I do not span across the overflow border.
			// (idKey <= predKey && (key <= idKey || key > predKey)) //we do
			// (the "=" case should never be verified)
			while ((idKey > predKey ? (key <= idKey && key > predKey) : (key <= idKey || key > predKey))) {

				// respond to the original requesting node
				responseIndexList.add(ftIndex);

				// set next key to find, and finger table index
				ftIndex++;

				if (Constants.F_TABLE_SIZE <= ftIndex || 0 > ftIndex)
					break;

				key = (srcDto.getKey() + new Double(Math.pow(2.0, ftIndex)).longValue()) % Constants.MODULUS;

				// check overflow
				if (key < 0)
					key = key + Constants.MODULUS;

			}

			// If we enter here, then it means ftIndex has changed and it needs
			// to be updated in the FingerFillTableDTO
			if (!responseIndexList.isEmpty()) {

				url = Constants.HTTP_PRIMER + srcDto.getAddress() + Constants.APP_NAME_PATH + Constants.FINGER_RESPONSE
						+ dto.getTaskId();
				uri = new URI(url);

				respDto = new FingerResponseDTO(new IdentityDTO(id), responseIndexList);

				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(respDto);
				runnable = new HttpRequestDelegateRunnable(request);
				delegate = new Thread(runnable);

				delegate.start();

				dto.setFtIndex(ftIndex);
				dto.setReqKey(key);
			}

			self.predecessorRdUnlock();
			isToRdUnlockPred = false;

			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			// Forward to the next greatest node whose successor may have the
			// requested key, or to my next node if the key is its.
			if (dto.getFtIndex() < Constants.F_TABLE_SIZE) {

				delegate = new Thread(new FingerFillTableForwarderRunnable(self, dto));
				delegate.start();
			}

			return null;
		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToRdUnlockPred)
				self.predecessorRdUnlock();
			if (isToUnlockFingerRd)
				self.fingerRdUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			return e.toString();
		}
	}

	// @Override
	// public void keepAlive(Identity requestingId) {
	//
	//
	// }

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerTableService#notifySuccessor(capri.fabiani.model.rest.FingerNotifySuccessorDTO)
	 */
	@Override
	public String notifySuccessor(FingerNotifySuccessorDTO requestDto) {

		boolean toWrUnlockPred = false;
		try {

			IdentityDTO newPredDto = requestDto.getNotifierId();
			Identity newPred = new Identity();

			newPred.setAddress(newPredDto.getAddress());
			newPred.setKey(newPredDto.getKey());

			// The bootstrap node knows which nodes exist, and will assign each
			// new node a predecessor and a successor.
			// If a node notifies another node, then there should not be any
			// other node between the two, on the ring.

			Logger.status("Notified by new predecessor: " + newPred.toString());

			self.predecessorWrLock();
			toWrUnlockPred = true;

			self.setPredecessor(newPred);

			self.predecessorWrUnlock();
			toWrUnlockPred = false;

			String url = Constants.HTTP_PRIMER + newPredDto.getAddress() + Constants.APP_NAME_PATH
					+ Constants.FINGER_RESPONSE + requestDto.getTaskId();
			URI uri = new URI(url);

			// FingerResponseDTO respDto = new FingerResponseDTO();

			RequestEntity<FingerResponseDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
					.body(null);
			Runnable runnable = new HttpRequestDelegateRunnable(request);
			Thread delegate = new Thread(runnable);

			delegate.start();

			return null;
		} catch (Exception e) {
			if (toWrUnlockPred)
				self.predecessorWrUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			return e.toString();
		}
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerTableService#notifyPredecessor(capri.fabiani.model.rest.FingerNotifyPredecessorDTO)
	 */
	@Override
	public String notifyPredecessor(FingerNotifyPredecessorDTO requestDto) {
		Logger.status("Notified by new successor, key = " + requestDto.getNotifierId().getKey() + ", IP = "
				+ requestDto.getNotifierId().getAddress());

		boolean isToUnlockTable = false;
		boolean isToUnlockNextNodes = false;
		boolean isToNextNodesCandidatesUnlock = false;
		boolean isToFingerCandidatesUnlock = false;

		Identity i, j = null;

		List<Identity> idList;
		List<FingerResponseDTO> nextNodesCandidates;
		try {
			// If a node notifies another node, then there should not be any
			// other node between the two, on the ring.
			IdentityDTO idDto = requestDto.getNotifierId();

			self.fingerCandidatesLock();
			isToFingerCandidatesUnlock = true;
			self.fingerWrLock();
			isToUnlockTable = true;

			self.getFingerCandidates().put(-1, new Identity(idDto.getAddress(), idDto.getKey()));

			self.fingerCandidatesUnlock();
			isToFingerCandidatesUnlock = false;

			idList = self.getFingerTable();

			// As of now, this allows for this method to be used as a keepalive
			// of some sort.
			// The FT gets updated only if a node other than the current
			// successor requested this method
			if (idList.isEmpty() || ((i = idList.get(0)) != null
					&& (i.getKey() != idDto.getKey() || !i.getAddress().equals(idDto.getAddress())))) {
				Logger.status("Added new successor (finger), key = " + idDto.getKey() + ", IP = " + idDto.getAddress());

				j = new Identity();
				j.setAddress(idDto.getAddress());
				j.setKey(idDto.getKey());

				// The finger table is ordered. If I get a new successor, this
				// shall be the first node in my finger table.
				// The FT still needs to be checked again.
				idList.add(0, j);
			}

			idList = null;
			i = null;

			self.fingerWrUnlock();
			isToUnlockTable = false;

			self.nextNodesCandidatesLock();
			isToNextNodesCandidatesUnlock = true;
			self.nextNodesWrLock();
			isToUnlockNextNodes = true;

			nextNodesCandidates = self.getNextNodesCandidates();
			nextNodesCandidates.add(0, new FingerResponseDTO(idDto, Constants.REDUNDANCY_LEVEL));

			self.nextNodesCandidatesUnlock();
			isToNextNodesCandidatesUnlock = false;

			idList = self.getNextNodes();

			if (idList.isEmpty() || (((i = idList.get(0)) != null)
					&& (i.getKey() != idDto.getKey() || !i.getAddress().equals(idDto.getAddress())))) {
				Logger.status(
						"Added new successor (next node), key = " + idDto.getKey() + ", IP = " + idDto.getAddress());

				if (j == null) {
					j = new Identity();
					j.setAddress(idDto.getAddress());
					j.setKey(idDto.getKey());
				}

				idList.add(0, j);
			}

			idList = null;

			self.nextNodesWrUnlock();
			isToUnlockNextNodes = false;

			if (j != null)
				daemonService.notifiedAsPredecessor();

			return null;
		} catch (Exception e) {
			if (isToUnlockTable)
				self.fingerWrUnlock();
			if (isToUnlockNextNodes)
				self.nextNodesWrUnlock();
			if (isToFingerCandidatesUnlock)
				self.fingerCandidatesUnlock();
			if (isToNextNodesCandidatesUnlock)
				self.nextNodesCandidatesUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));

			return e.toString();
		}
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerTableService#nodeResponse(capri.fabiani.model.rest.FingerResponseDTO, int)
	 */
	@Override
	public void nodeResponse(@NotNull FingerResponseDTO response, int taskId) {
		// TODO see if there's anything to do with Successor_Replication
		boolean isToTasksUnlock = false;
		boolean isToFingerCandUnlock = false;
		boolean isToNextNodesCandidatesUnlock = false;
		boolean isTaskToUnlock = false;
		boolean isToPredecessorWrUnlock = false;
		PendingTask task = null;

		int errCode = 0;

		try {
			self.tasksLock();
			isToTasksUnlock = true;
			Map<Integer, PendingTask> tasks = self.getPendingTasks();
			task = tasks.get(taskId);

			if (task != null) {

				PendingTask.TaskType type = task.getType();

				switch (type) {

				case SUCCESSOR_FINGER:

					Map<Integer, Identity> candidatesSet;
					IdentityDTO responder = response.getResponseIdentity();

					// Add new finger entry
					self.fingerCandidatesLock();
					;
					isToFingerCandUnlock = true;

					candidatesSet = self.getFingerCandidates();

					// An identity is equal to another if it has the same key
					// (regardless of IP address).
					// This ensures any old instance is removed before adding
					// the current object, and that there shan't be any
					// duplicates
					for (Integer i : (List<Integer>) response.getExtras()) {
						if (i > Constants.F_TABLE_SIZE || i < 0) {
							Logger.warning("FT refresh - received ft index out of range.");
						} else {
							candidatesSet.remove(i);
							candidatesSet.put(i, new Identity(responder.getAddress(), responder.getKey()));
						}
					}

					self.fingerCandidatesUnlock();

					isToFingerCandUnlock = false;

					// Remove the pending task if needed - notice that the task
					// shan't be removed if it's not of the proper type.
					task.taskLock();
					isTaskToUnlock = true;
					task.setExtras(((Integer) task.getExtras()) - 1);
					if (((Integer) task.getExtras()) == 0) {
						task.condSignal();
					}
					task.taskUnlock();
					isTaskToUnlock = false;

					break;

				case SUCCESSOR_REPLICATION:

					List<FingerResponseDTO> candidates;
					boolean good = response.getExtras() instanceof Integer;

					if (good) {
						self.nextNodesCandidatesLock();
						isToNextNodesCandidatesUnlock = true;

						candidates = self.getNextNodesCandidates();

						candidates.remove(response);
						candidates.add(response);

						self.nextNodesCandidatesUnlock();
						isToNextNodesCandidatesUnlock = false;

						task.taskLock();
						isTaskToUnlock = true;
						task.setExtras(((Integer) task.getExtras()) - 1);
						if (((Integer) task.getExtras()) == 0) {
							task.condSignal();
						}
						task.taskUnlock();
						isTaskToUnlock = false;
					}

					break;

				// case PREDECESSOR:
				//
				// self.predecessorWrLock();
				// isToPredecessorWrUnlock = true;
				//
				// IdentityDTO idDto = response.getResponseIdentity();
				// self.setPredecessor(new Identity(idDto.getAddress(),
				// idDto.getKey()));
				//
				// self.predecessorWrUnlock();
				// isToPredecessorWrUnlock = false;
				//
				// break;

				default:
					// Invalid task type -> malformed packet
					errCode = 2;
				}

			} else {
				// NotFound
				errCode = 1;
			}

			self.tasksUnlock();
			isToTasksUnlock = false;

		} catch (ClassCastException e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			if (isToNextNodesCandidatesUnlock)
				self.nextNodesCandidatesUnlock();
			if (isToFingerCandUnlock)
				self.fingerCandidatesUnlock();
			;
			if (isTaskToUnlock)
				task.taskUnlock();
			if (isToTasksUnlock)
				self.tasksUnlock();
			if (isToPredecessorWrUnlock)
				self.predecessorWrUnlock();

			// Some Extras Object was not of the expected type -> malformed
			// packet
			errCode = 2;
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			if (isToNextNodesCandidatesUnlock)
				self.nextNodesCandidatesUnlock();
			if (isToFingerCandUnlock)
				self.fingerCandidatesUnlock();
			;
			if (isTaskToUnlock)
				task.taskUnlock();
			if (isToTasksUnlock)
				self.tasksUnlock();

			// Some other error ->InternalServerError
			errCode = 3;
		}

		switch (errCode) {
		case 1:
			throw new FingerTaskNotFoundException();
		case 2:
			throw new FingerMalformedPacketException();
		case 3:
			throw new InternalServerErrorException();
		}

	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerTableService#findPredecessor(capri.fabiani.model.rest.FingerFindPredecessorDTO)
	 */
	@Override
	public String findPredecessor(FingerFindPredecessorDTO dto) {
		boolean isToFingerRdUnlock = false;
		boolean isToIdentityRdUnlock = false;

		try {
			String url;
			URI uri;
			Runnable runnable;
			Thread delegate;

			boolean checkPassed = false;
			long reqKey = dto.getReqKey();

			List<Identity> ft;

			self.identityRdLock();
			isToIdentityRdUnlock = true;

			// If not, I'll check whether the key should be in my successor's
			// range, in case I'll answer with my own identity
			self.fingerRdLock();
			isToFingerRdUnlock = true;

			ft = self.getFingerTable();

			// TODO send error response if ft is empty?
			checkPassed = !ft.isEmpty() && reqKey > self.getIdentity().getKey() && reqKey <= ft.get(0).getKey();

			ft = null;

			self.fingerRdUnlock();
			isToFingerRdUnlock = false;

			if (checkPassed) {
				Identity myIdentity = self.getIdentity();
				FingerResponseDTO respDto;

				url = Constants.HTTP_PRIMER + myIdentity.getAddress() + Constants.APP_NAME_PATH
						+ Constants.FINGER_RESPONSE + dto.getTaskId();
				uri = new URI(url);

				respDto = new FingerResponseDTO(new IdentityDTO(myIdentity.getAddress(), myIdentity.getKey()), null);

				RequestEntity<FingerResponseDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
						.body(respDto);

				runnable = new HttpRequestDelegateRunnable(request);
				delegate = new Thread(runnable);

				delegate.start();

				self.identityRdUnlock();
				isToIdentityRdUnlock = false;

				return null;
			}

			/*------------------------------------------------------------*/

			// If not, forward the request as in fingerFillTable()

			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			self.fingerRdLock();
			isToFingerRdUnlock = true;

			Identity probe, id;
			Iterator<Identity> fTabIterator = self.getFingerTable().iterator();
			boolean grind = true;

			// My very first successor owns the required key
			probe = fTabIterator.next();
			if (reqKey <= probe.getKey())
				grind = false;

			// recycle "id" as temporary target variable
			id = probe;

			if (grind) {
				while (fTabIterator.hasNext()) {
					probe = fTabIterator.next();

					// "probe" may or may not be "id"'s next node. We are
					// safe in forwarding the request to "id"
					if (reqKey <= probe.getKey())
						break;

					id = probe;
				}
			}
			// In case we come out of this loop because
			// fTabIterator.hasNext()==false, then we forward the request to our
			// furthest node.

			self.fingerRdUnlock();
			isToFingerRdUnlock = false;

			url = Constants.HTTP_PRIMER + id.getAddress() + Constants.APP_NAME_PATH + Constants.FINGER_FILL_TABLE;
			uri = new URI(url);

			RequestEntity<FingerFindPredecessorDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
					.body(dto);

			runnable = new HttpRequestDelegateRunnable(request);
			delegate = new Thread(runnable);

			delegate.start();

			return null;
		} catch (Exception e) {
			if (isToFingerRdUnlock)
				self.fingerRdUnlock();
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			return e.toString();
		}

	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerTableService#getForwardee(long)
	 */
	@Override
	public IdentityDTO getForwardee(long requestedKey) {
		long highEnd, lowEnd;
		boolean isToPredecessorRdUnlock = false;
		boolean isToFtRdUnlock = false;
		boolean isToIdentityRdUnlock = false;

		IdentityDTO dto;
		Identity prev, curr;
		Iterator<Identity> iter;

		try {
			self.identityRdLock();
			isToIdentityRdUnlock = true;

			highEnd = self.getIdentity().getKey();

			self.predecessorRdLock();
			isToPredecessorRdUnlock = true;

			lowEnd = self.getPredecessor().getKey();

			self.predecessorRdUnlock();
			isToPredecessorRdUnlock = false;

			// Return myself
			if ((lowEnd < highEnd && lowEnd < requestedKey && requestedKey <= highEnd) || // Range
																							// not
																							// overlapping
																							// the
																							// limit
																							// region
					(lowEnd > highEnd && (requestedKey > lowEnd || requestedKey <= highEnd)) // Range
																								// overlapping
																								// the
																								// limit
																								// region
			) {

				IdentityDTO idDto = new IdentityDTO(self.getIdentity());

				self.identityRdUnlock();
				isToIdentityRdUnlock = false;

				return idDto;
			} else { // Return the closest node on the FT

				// Preparing to iterate on the fingers
				prev = self.getIdentity();
				lowEnd = highEnd;

				self.identityRdUnlock();
				isToIdentityRdUnlock = false;

				self.fingerRdLock();
				isToFtRdUnlock = true;

				iter = self.getFingerTable().iterator();

				curr = iter.next();
				highEnd = curr.getKey();

				// Check whether if the requested key falls between me and my
				// successor or not. This if statement Is the same as above
				if ((lowEnd < highEnd && lowEnd < requestedKey && requestedKey <= highEnd) || // Range
																								// not
																								// overlapping
																								// the
																								// limit
																								// region
						(lowEnd > highEnd && (requestedKey > lowEnd || requestedKey <= highEnd)) // Range
																									// overlapping
																									// the
																									// limit
																									// region
				) {

					dto = new IdentityDTO(curr);

				} else { // Look for the closest key on the FT preceding the
							// requested key
					prev = curr;
					lowEnd = highEnd;

					while (iter.hasNext()) {
						curr = iter.next();
						highEnd = curr.getKey();

						// Same as before. If the key falls between prev and
						// curr, the requested forwardee is "prev" (there may be
						// other nodes
						// in the FT between prev and curr after the first step)
						if ((lowEnd < highEnd && lowEnd < requestedKey && requestedKey <= highEnd) || // Range
																										// not
																										// overlapping
																										// the
																										// limit
																										// region
								(lowEnd > highEnd && (requestedKey > lowEnd || requestedKey <= highEnd)) // Range
																											// overlapping
																											// the
																											// limit
																											// region
						) {

							dto = new IdentityDTO(prev);

						}

						prev = curr;
						lowEnd = highEnd;
					}

					dto = new IdentityDTO(prev);

				}

				self.fingerRdUnlock();
				isToFtRdUnlock = false;

				return dto;
			}

		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToPredecessorRdUnlock)
				self.predecessorRdUnlock();
			if (isToFtRdUnlock)
				self.fingerRdUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			return null;
		}
	}

}
