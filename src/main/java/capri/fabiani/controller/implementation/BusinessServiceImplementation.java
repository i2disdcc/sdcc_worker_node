/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import java.net.URI;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import com.google.common.base.Throwables;

import capri.fabiani.components.WorkerNode;
import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.AsyncService;
import capri.fabiani.controller.BusinessService;
import capri.fabiani.controller.FingerTableService;
import capri.fabiani.controller.MasterPersistenceService;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.threads.HttpRequestDelegateRunnable;
import capri.fabiani.util.Logger;

/**
 * Coordinates CRUD operations of data on the ring by invoking methods from the
 * MasterPersistenceService, or by forwarding a request to another node.
 * 
 * @author Ultimate
 *
 */
@Service
public class BusinessServiceImplementation implements BusinessService {

	/**
	 * The Enum BusinessOp.
	 */
	private enum BusinessOp {

		/** The business delete. */
		BUSINESS_DELETE,
		/** The business get. */
		BUSINESS_GET,
		/** The business register. */
		BUSINESS_REGISTER,
		/** The business update. */
		BUSINESS_UPDATE
	};

	/** The self. */
	@Autowired
	private WorkerNode self;

	/** The master persistence service. */
	@Autowired
	private MasterPersistenceService masterPersistenceService;

	/** The finger table service. */
	@Autowired
	private FingerTableService fingerTableService;

	/** The async service. */
	@Autowired
	private AsyncService asyncService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * capri.fabiani.controller.BusinessService#deleteProduct(capri.fabiani.
	 * model.rest.BusinessDTO)
	 */
	@Override
	public DTO deleteProduct(BusinessDTO datumCredentials) {
		return new DTO(generalOp(datumCredentials, BusinessOp.BUSINESS_DELETE));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * capri.fabiani.controller.BusinessService#getProduct(capri.fabiani.model.
	 * rest.BusinessDTO)
	 */
	@Override
	public DTO getProduct(BusinessDTO productCredentials) {
		return new DTO(generalOp(productCredentials, BusinessOp.BUSINESS_GET));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * capri.fabiani.controller.BusinessService#registerProduct(capri.fabiani.
	 * model.rest.BusinessDTO)
	 */
	@Override
	public DTO registerProduct(BusinessDTO newProduct) {
		return new DTO(generalOp(newProduct, BusinessOp.BUSINESS_REGISTER));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see capri.fabiani.controller.BusinessService#requestProduct(int)
	 */
	@Override
	public DTO requestProduct(int productClass) {
		// TODO For future developement
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * capri.fabiani.controller.BusinessService#updateProduct(capri.fabiani.
	 * model.rest.BusinessDTO)
	 */
	@Override
	public DTO updateProduct(BusinessDTO productCredentialsAndData) {
		return new DTO(generalOp(productCredentialsAndData, BusinessOp.BUSINESS_UPDATE));
	}

	/**
	 * General op.
	 *
	 * @param datumCredentials
	 *            the datum credentials
	 * @param op
	 *            the op
	 */
	private String generalOp(BusinessDTO datumCredentials, BusinessOp op) {
		long highEnd, lowEnd, requestedKey;
		boolean isToPredecessorRdUnlock = false;
		boolean isToIdentityRdUnlock = false;

		String url, opString = null;
		String addressString;

		URI uri;
		RequestEntity<BusinessDTO> request;
		Runnable runnable;
		Thread delegate;

		try {
			Logger.status("Requesting operation " + op.toString() + " for datum " + datumCredentials.getKey()
					+ ", owner " + datumCredentials.getOwner() + "...");

			self.identityRdLock();
			isToIdentityRdUnlock = true;

			highEnd = self.getIdentity().getKey();

			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			self.predecessorRdLock();
			isToPredecessorRdUnlock = true;

			if (self.getPredecessor() != null) {
				lowEnd = (self.getPredecessor().getKey() + 1) % Constants.MODULUS;
				// Overflow
				if (lowEnd < 0) {
					lowEnd = lowEnd + Constants.MODULUS;
				}
			}
			else {
				lowEnd = Constants.MIN_VALUE;
			}

			self.predecessorRdUnlock();
			isToPredecessorRdUnlock = false;

			requestedKey = datumCredentials.getKey();

			// I am in charge of the requested key
			if ((lowEnd <= highEnd && lowEnd <= requestedKey && requestedKey <= highEnd) || // Range
																							// not
																							// overlapping
																							// the
																							// limit
																							// region
					(lowEnd > highEnd && (requestedKey >= lowEnd || requestedKey <= highEnd)) // Range
																								// overlapping
																								// the
																								// limit
																								// region
			) {

				// Read
				if (op == BusinessOp.BUSINESS_GET) {
					if (!masterPersistenceService.getProduct(datumCredentials)) {
						addressString = datumCredentials.getResponseURL();
						uri = new URI(addressString);

						Logger.status("Could not retreive requested datum.");

						datumCredentials.setError(Constants.ERROR_MASTER_GET);
						request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(datumCredentials);
						runnable = new HttpRequestDelegateRunnable(request);
						delegate = new Thread(runnable);
						delegate.start();
					}
				} else { // Write -> commit
					final MasterPersistenceService.CommitOpType masterOpType;
					final BusinessDTO businessCredentials = datumCredentials;
					switch (op) {
					case BUSINESS_DELETE:
						// Asyncronally deletes the product and answers to the
						// response link.
						masterOpType = MasterPersistenceService.CommitOpType.DELETE;
						break;
					case BUSINESS_REGISTER:
						// Asyncronally registers the product and answers to the
						// response link.
						masterOpType = MasterPersistenceService.CommitOpType.CREATE;
						break;
					case BUSINESS_UPDATE:
						// Asyncronally updates the product and answers to the
						// response link.
						masterOpType = MasterPersistenceService.CommitOpType.UPDATE;
						break;
					default:
						masterOpType = null;
						break;
					}

					runnable = new Runnable() {
						@Override
						public void run() {
							writeAndRespond(masterOpType, businessCredentials);
						}
					};

					asyncService.run(runnable);
				}

			} else { // Forward

				switch (op) {
				case BUSINESS_DELETE:
					opString = Constants.BUSINESS_DELETE_PRODUCT;
					break;
				case BUSINESS_GET:
					opString = Constants.BUSINESS_GET_PRODUCT;
					break;
				case BUSINESS_REGISTER:
					opString = Constants.BUSINESS_REGISTER_PRODUCT;
					break;
				case BUSINESS_UPDATE:
					opString = Constants.BUSINESS_UPDATE_PRODUCT;
					break;
				}

				// Forward to the closest node on the FT, which possibly has the
				// required key within its range.
				addressString = fingerTableService.getForwardee(requestedKey).getAddress();
				url = Constants.HTTP_PRIMER + addressString + Constants.APP_NAME_PATH + opString;
				uri = new URI(url);

				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(datumCredentials);
				runnable = new HttpRequestDelegateRunnable(request);
				delegate = new Thread(runnable);
				delegate.start();
			}

			return null;

		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToPredecessorRdUnlock)
				self.predecessorRdUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
			return e.toString();
		}
	}

	/**
	 * Executes a C-UD operation via the MasterPersistenceService and then
	 * responds to the response url given in the BusinessDTO passed as
	 * parameter.
	 *
	 * @param opType
	 *            the op type
	 * @param businessDto
	 *            the business dto
	 */
	private void writeAndRespond(@NotNull MasterPersistenceService.CommitOpType opType,
			@NotNull BusinessDTO businessDto) {
		try {
			boolean allClear = false;
			switch (opType) {
			case CREATE:
				allClear = masterPersistenceService.registerProduct(businessDto);
				break;
			case DELETE:
				allClear = masterPersistenceService.deleteProduct(businessDto);
				break;
			case UPDATE:
				allClear = masterPersistenceService.updateProduct(businessDto);
				break;
			}

			String url;
			URI uri;
			RequestEntity<BusinessDTO> request;
			BusinessDTO responseDto = new BusinessDTO();

			responseDto.setRequestId(businessDto.getRequestId());
			if (!allClear) {
				responseDto.setError(Constants.ERROR_MASTER_COMMIT);
			}

			url = businessDto.getResponseURL();
			uri = new URI(url);

			request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(responseDto);
			HttpEntityConfiguration.getTemplate().exchange(request, String.class);

		} catch (Exception e) {
			// If there's any problem, the request on the agent side will just
			// expire.
			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

}
