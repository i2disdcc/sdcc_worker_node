/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Throwables;

import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.DatumService;
import capri.fabiani.model.Datum;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.repositories.DatumRepository;
import capri.fabiani.util.Logger;

/**
 * The Class DatumServiceImplementation.
 */
@Service
public class DatumServiceImplementation implements DatumService {

	/** The datum repo. */
	@Autowired
	private DatumRepository datumRepo;

	/** The mapper. */
	private ModelMapper mapper = new ModelMapper();
	
	/** The datum. */
	private Datum datum;

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#create(capri.fabiani.model.rest.BusinessDTO)
	 */
	@Override
	@Transactional
	public BusinessDTO create(BusinessDTO dto) {
		BusinessDTO datumDto = null;
		Logger.status("Creating datum " + dto.getKey() + ", owner " + dto.getOwner() + "...");
		try {
			Datum datum = businessToDatum(dto);

			datum = datumRepo.save(datum);
			datumDto = mapper.map(datum, BusinessDTO.class);

			Logger.status("Created.");
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			datumDto = null;
		}
		return datumDto;
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#delete(long)
	 */
	@Override
	@Transactional
	public void delete(long id) {
		BusinessDTO datumDto = null;
		try {
			datumDto = findById(id);
			Logger.status("Deleting datum by ID " + datumDto.getKey() + ", owner " + datumDto.getOwner() + "...");
			if (datumDto != null) {
				datum = mapper.map(datumDto, Datum.class);
				datumRepo.deleteByKey(id);
				Logger.status("Datum " + datumDto.getKey() + " deleted.");
			}
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#delete(capri.fabiani.model.rest.BusinessDTO)
	 */
	@Override
	@Transactional
	public void delete(BusinessDTO dto) {
		try {
			Logger.status("Deleting datum " + dto.getKey() + ", owner " + dto.getOwner() + "...");
			if (dto != null) {
				datum = mapper.map(dto, Datum.class);
				datumRepo.deleteByKey(datum.getKey());
				Logger.status("Datum " + dto.getKey() + " deleted.");
			}
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}

	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#saveBulk(java.util.List)
	 */
	@Override
	@Transactional
	public void saveBulk(List<BusinessDTO> dtoList) {
		List<Datum> datumList;

		Logger.status("Saving data bulk of " + dtoList.size() + " elements" + "...");
		try {
			datumList = new LinkedList<Datum>();
			for (BusinessDTO dto : dtoList) {
				datumList.add(businessToDatum(dto));
			}

			datumRepo.save(datumList);
			Logger.status("Bulk saved.");
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#update(capri.fabiani.model.rest.BusinessDTO)
	 */
	@Override
	@Transactional
	public void update(BusinessDTO dto) {
		try {
			Logger.status("Updating datum " + dto.toString());
			Datum datum = businessToDatum(dto);
			datum = datumRepo.save(datum);
			Logger.status("Updated datum " + mapper.map(datum, BusinessDTO.class).toString());
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#findById(long)
	 */
	@Override
	public BusinessDTO findById(long id) {
		BusinessDTO datumDto = null;
		Logger.status("Finding datum with ID " + id + "...");
		try {
			datum = datumRepo.findByKey(id);
			
			if (datum == null) {
				Logger.status("Not found.");
			} else {
				Logger.status("Found.");
				datumDto = mapper.map(datum, BusinessDTO.class);
			}
			
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			datumDto = null;
		}

		return datumDto;
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#findByIdRange(long, long)
	 */
	@Override
	public List<BusinessDTO> findByIdRange(long startId, long endId) {
		List<Datum> dataList;
		List<BusinessDTO> dtoList = new LinkedList<BusinessDTO>();
		Datum tempDatum;

		Logger.status("Finding by range: startId = " + startId + ", endId = " + endId + "...");
		try {
			if (startId <= endId) {
				dataList = datumRepo.findByKeyBetween(startId, endId);
				orderList(dataList);
				for (Datum d : dataList) {
					dtoList.add(mapper.map(d, BusinessDTO.class));
				}
			} else {
				dataList = datumRepo.findByKeyBetween(startId, Constants.MAX_VALUE);
				orderList(dataList);
				for (Datum d : dataList) {
					dtoList.add(mapper.map(d, BusinessDTO.class));
				}
				if ((tempDatum = datumRepo.findByKey(Constants.MAX_VALUE)) != null)
					dtoList.add(mapper.map(tempDatum, BusinessDTO.class));

				dataList = datumRepo.findByKeyBetween(Constants.MIN_VALUE, endId);
				orderList(dataList);
				if ((tempDatum = datumRepo.findByKey(Constants.MIN_VALUE)) != null)
					dtoList.add(0, mapper.map(tempDatum, BusinessDTO.class));
				for (Datum d : dataList) {
					dtoList.add(mapper.map(d, BusinessDTO.class));
				}
			}

			if ((tempDatum = datumRepo.findByKey(endId)) != null)
				dtoList.add(mapper.map(tempDatum, BusinessDTO.class));
			if ((tempDatum = datumRepo.findByKey(startId)) != null)
				dtoList.add(0, mapper.map(tempDatum, BusinessDTO.class));

		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Found " + dtoList.size() + " elements");

		return dtoList;
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#deleteByRange(long, long)
	 */
	@Override
	public Long deleteByRange(long startId, long endId) {
		Logger.status("Deleting by range: startId = " + startId + "; endId = " + endId + "...");
		long result = 0;
		try {
			if (endId == -1 && startId == -1) {
				startId = Constants.MIN_VALUE;
				endId = Constants.MAX_VALUE;
			}
			Logger.status("Deleting from " + startId + " to " + endId);
			if (startId <= endId) {
				result += datumRepo.deleteByKeyBetween(startId, endId);
			} else {
				result += datumRepo.deleteByKeyBetween(startId, Constants.MAX_VALUE);
				result += datumRepo.deleteByKey(Constants.MAX_VALUE);
				result += datumRepo.deleteByKeyBetween(Constants.MIN_VALUE, endId);
				result += datumRepo.deleteByKey(Constants.MIN_VALUE);
			}

			result += datumRepo.deleteByKey(endId);
			result += datumRepo.deleteByKey(startId);

			Logger.status("Deleted " + result + " elements");
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#findByProductClass(long)
	 */
	@Override
	public List<BusinessDTO> findByProductClass(long classId) {
		List<BusinessDTO> datumDtoList = new LinkedList<BusinessDTO>();
		List<Datum> datumList;

		Logger.status("Finding products of class " + classId + "...");
		try {
			datumList = datumRepo.findByProductClass(classId);

			for (Datum dat : datumList) {
				datumDtoList.add(mapper.map(dat, BusinessDTO.class));
			}
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Found " + datumDtoList.size() + " elements.");
		return datumDtoList;
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#findByProductClassAndOwner(long, java.lang.String)
	 */
	@Override
	public List<BusinessDTO> findByProductClassAndOwner(long productClass, String owner) {
		List<Datum> dataList;
		List<BusinessDTO> dtoList = new LinkedList<BusinessDTO>();

		Logger.status("Finding products of class " + productClass + " owned by " + owner + "...");

		try {
			dataList = datumRepo.findByProductClassAndOwner(productClass, owner);
			for (Datum d : dataList) {
				dtoList.add(mapper.map(d, BusinessDTO.class));
			}
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Found " + dtoList.size() + " elements.");
		return dtoList;
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#findMedian(long, long)
	 */
	@Override
	public long findMedian(long startId, long endId) {
		List<Datum> dataList = null;
		Datum tempDatum = null;

		Logger.status("Finding median: startId = " + startId + ", endId = " + endId + "...");

		try {
			if (startId <= endId) {
				dataList = datumRepo.findByKeyBetween(startId, endId);
				tempDatum = datumRepo.findByKey(endId);
				if (tempDatum != null)
					dataList.add(tempDatum);
				tempDatum = datumRepo.findByKey(startId);
				if (tempDatum != null)
					dataList.add(0, tempDatum);
				orderList(dataList);
			} else {
				dataList = datumRepo.findByKeyBetween(startId, Constants.MAX_VALUE);

				if ((tempDatum = datumRepo.findByKey(Constants.MAX_VALUE)) != null)
					dataList.add(tempDatum);
				if ((tempDatum = datumRepo.findByKey(startId)) != null)
					dataList.add(tempDatum);

				orderList(dataList);
				List<Datum> temp = datumRepo.findByKeyBetween(Constants.MIN_VALUE, endId);

				if ((tempDatum = datumRepo.findByKey(Constants.MIN_VALUE)) != null)
					temp.add(tempDatum);
				if ((tempDatum = datumRepo.findByKey(endId)) != null)
					temp.add(tempDatum);

				orderList(temp);

				dataList.addAll(temp);
			}
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Found " + dataList.size() + " elements.");
		if (dataList.size() <= 0) {
			return -1;
		}
		Datum pivot = dataList.get(dataList.size() / 2);

		Logger.status("Pivot key = " + pivot.getKey());
		return pivot.getKey();
	}

	/**
	 * Order list.
	 *
	 * @param datums the datums
	 */
	private void orderList(List<Datum> datums) {

		Collections.sort(datums, new Comparator<Datum>() {
			@Override
			public int compare(Datum o1, Datum o2) {
				if (o1.getKey() < o2.getKey())
					return -1;
				if (o1.getKey() > o2.getKey())
					return 1;
				return 0;
			}

		});
	}

	/**
	 * Business to datum.
	 *
	 * @param businessDto the business dto
	 * @return the datum
	 */
	private Datum businessToDatum(BusinessDTO businessDto) {
		Datum datum = new Datum();

		datum.setKey(businessDto.getKey());
		datum.setOwner(businessDto.getOwner());
		datum.setProductClass(businessDto.getProductClass());

		// TODO it makes sense in some scenarios that the body is null, but are
		// we missing something? May there be an error somewhere higher up in
		// the hierarchy?
		if (businessDto.getBody() != null)
			datum.setBody(businessDto.getBody().unpack());

		return datum;
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.DatumService#countByIdRange(long, long)
	 */
	@Override
	public Long countByIdRange(long startId, long endId) {
		Logger.status("Counting by ID range: startId = " + startId + ", endId = " + endId + "...");

		long count = 0;

		try {
			if (startId > endId) {
				count = datumRepo.countByKeyBetween(startId, Constants.MAX_VALUE);
				if (datumRepo.findByKey(Constants.MAX_VALUE) != null)
					count++;
				count += datumRepo.countByKeyBetween(Constants.MIN_VALUE, endId);
				if (datumRepo.findByKey(Constants.MIN_VALUE) != null)
					count++;
			} else {
				count = datumRepo.countByKeyBetween(startId, endId);
			}
			// Under the hypothesis that a single-value query method returns
			// null in
			// case the filter is not matched
			if (datumRepo.findByKey(startId) != null)
				count++;
			if (datumRepo.findByKey(endId) != null)
				count++;

		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Found " + count + " elements.");
		return count;
	}

}
