/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capri.fabiani.components.FingerTableDaemon;
import capri.fabiani.components.RedundancyDaemon;
import capri.fabiani.controller.FingerDaemonService;
import capri.fabiani.model.rest.IdentityDTO;

/**
 * The Class FingerDaemonServiceImplementation.
 */
@Service
public class FingerDaemonServiceImplementation implements FingerDaemonService {
	
	/** The ft daemon. */
	@Autowired
	private FingerTableDaemon ftDaemon;
	
	/** The red daemon. */
	@Autowired
	private RedundancyDaemon redDaemon;
	
	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerDaemonService#rangeUpdated(capri.fabiani.model.rest.IdentityDTO)
	 */
	@Override
	public void rangeUpdated(IdentityDTO newPredecessor) {
		redDaemon.rangeUpdated(newPredecessor);
	}

	/* (non-Javadoc)
	 * @see capri.fabiani.controller.FingerDaemonService#notifiedAsPredecessor()
	 */
	@Override
	public void notifiedAsPredecessor() {
		ftDaemon.refreshFtAsync();
		ftDaemon.refreshnextNodesAsync();
	}
}
