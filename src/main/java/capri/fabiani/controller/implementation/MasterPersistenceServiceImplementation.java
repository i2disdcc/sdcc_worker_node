/*
 * @author Dan&Dan
 */
package capri.fabiani.controller.implementation;

import java.net.URI;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import com.google.common.base.Throwables;

import capri.fabiani.components.WorkerNode;
import capri.fabiani.model.Identity;
import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.MasterPersistenceService;
import capri.fabiani.controller.SlavePersistenceService.CommitSlaveMsgType;
import capri.fabiani.model.MasterCommitMgtDatum;
import capri.fabiani.model.MasterCommitMgtDatum.MasterState;
import capri.fabiani.model.RedundancyIdentity;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.MasterCommitDTO;
import capri.fabiani.model.rest.SlaveCommitDTO;
import capri.fabiani.threads.HttpRequestDelegateRunnable;
import capri.fabiani.util.InternalServerErrorException;
import capri.fabiani.util.Logger;

/**
 * The Class MasterPersistenceServiceImplementation.
 */
@Service
public class MasterPersistenceServiceImplementation implements MasterPersistenceService {

	/** The self. */
	@Autowired
	private WorkerNode self;

	/*
	 * (non-Javadoc)
	 * 
	 * @see capri.fabiani.controller.MasterPersistenceService#getProduct(capri.
	 * fabiani.model.rest.BusinessDTO)
	 */
	/**
	 * Issues a get request to a random valid replica.
	 * 
	 * @return true if an available replica was found, false if no available
	 *         replica was found or the requested key was undergoing a commit at
	 *         the time of the request.
	 */
	@Override
	public boolean getProduct(BusinessDTO productCredentials) {

		boolean isToRedundancyRdUnlock = false;
		boolean foundReadyReplica = false;
		boolean isToReadyRdUnlock = false;
		boolean isToIdentityRdUnlock = false;
		boolean isToKeysPendingCommitUnlock = false;

		int i;
		long myKey;
		List<RedundancyIdentity> replicas;
		RedundancyIdentity id = null;

		boolean isPendingCommit = false;

		String url;
		URI uri;
		RequestEntity<BusinessDTO> request;
		Runnable runnable;
		Thread delegate;

		Logger.status("Requested product key " + productCredentials.getKey() + " , owner "
				+ productCredentials.getOwner() + "...");

		try {
			self.keysPendingCommitLock();
			isToKeysPendingCommitUnlock = true;

			isPendingCommit = self.getKeysPendingCommit().contains(productCredentials.getKey());

			self.keysPendingCommitUnlock();
			isToKeysPendingCommitUnlock = false;

			// Return false if the requested key is undergoing a commit.
			if (isPendingCommit) {
				Logger.warning("Requested key " + productCredentials.getKey()
						+ " is pending a commit. GET request NOT forwarded to replicas.");
				return false;
			}

			self.identityRdLock();
			isToIdentityRdUnlock = true;

			myKey = self.getIdentity().getKey();

			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			replicas = new LinkedList<RedundancyIdentity>();

			self.redundancyRdLock();
			isToRedundancyRdUnlock = true;

			replicas.addAll(self.getRedundancyTable());

			// Select a ready replica
			while (!replicas.isEmpty()) {
				i = (new Double(Math.floor(Math.random() * replicas.size()))).intValue();
				id = replicas.get(i);

				id.readyRdLock();
				isToReadyRdUnlock = true;
				if (id.isReady()) {
					foundReadyReplica = true;
				}
				id.readyRdUnlock();
				isToReadyRdUnlock = false;

				if (foundReadyReplica) {
					break;
				}

				replicas.remove(i);
			}

			// Found a ready replica
			if (foundReadyReplica) {
				Logger.status("Forwarding GET request to node " + id.getKey() + ", address " + id.getAddress());

				url = Constants.HTTP_PRIMER + id.getAddress() + Constants.APP_NAME_PATH + Constants.SLAVE_GET + myKey;
				uri = new URI(url);
				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(productCredentials);
				runnable = new HttpRequestDelegateRunnable(request);
				delegate = new Thread(runnable);
				delegate.start();
			}

			self.redundancyRdUnlock();
			isToRedundancyRdUnlock = false;

			return foundReadyReplica;
		} catch (Exception e) {
			if (isToKeysPendingCommitUnlock)
				self.keysPendingCommitUnlock();
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToRedundancyRdUnlock)
				self.redundancyRdUnlock();
			if (isToReadyRdUnlock)
				id.readyRdUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
			throw new InternalServerErrorException();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * capri.fabiani.controller.MasterPersistenceService#requestProduct(int)
	 */
	@Override
	public void requestProduct(int productClass) {
		// TODO For future developements

		// TODO must first write the new product with a distributed commit, then
		// reply as if it had been requested. The two phases do not need to be
		// atomic.
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * capri.fabiani.controller.MasterPersistenceService#registerProduct(capri.
	 * fabiani.model.rest.BusinessDTO)
	 */
	@Override
	public boolean registerProduct(BusinessDTO newProduct) {
		return startCommit(CommitOpType.CREATE, newProduct);
	}

	/**
	 * This function starts a commit to delete a certain datum consistently in
	 * all replicas (and the main node). It does not: - Check whether the key to
	 * delete falls within the master's range. - Check whether the datum
	 * credentials are valid (i.e. if the user requesting the deletion of the
	 * object is its owner).
	 * 
	 * Checks are taken care of by each replica in any case.
	 * 
	 * This function is SYNCHRONOUS, and will return only once the commit has
	 * either been completed or aborted
	 *
	 * @param datumCredentials
	 *            the datum credentials
	 * @return true, if successful
	 */
	@Override
	public boolean deleteProduct(BusinessDTO datumCredentials) {
		return startCommit(CommitOpType.DELETE, datumCredentials);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * capri.fabiani.controller.MasterPersistenceService#updateProduct(capri.
	 * fabiani.model.rest.BusinessDTO)
	 */
	@Override
	public boolean updateProduct(BusinessDTO productCredentialsAndData) {
		return startCommit(CommitOpType.UPDATE, productCredentialsAndData);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * capri.fabiani.controller.MasterPersistenceService#commit(capri.fabiani.
	 * model.rest.SlaveCommitDTO, int)
	 */
	@Override
	public void commit(SlaveCommitDTO dto, int commitIndex) {

		CommitSlaveMsgType type = dto.getMsgType();

		boolean toRdUnlockMasterCommitData = false;

		MasterCommitMgtDatum datum = null;
		boolean toDatumMgtUnlock = false;

		int shortCount;
		Set<Long> checkedSlaves;
		Long slaveKey;

		try {
			switch (type) {

			case VOTE_COMMIT:

				self.masterCommitDataRdLock();
				toRdUnlockMasterCommitData = true;

				datum = self.getMasterCommitMgtData().get(commitIndex);

				self.masterCommitDataRdUnlock();
				toRdUnlockMasterCommitData = false;

				if (datum != null) {
					checkedSlaves = datum.getCheckedSlaves();
					slaveKey = dto.getSlaveKey();

					datum.mgtLock();
					toDatumMgtUnlock = true;
					// Avoid side-effects of duplicate packets and
					// late/malicious packets with a non-valid ID
					if (datum.getState() == MasterState.WAIT && datum.containsInitialSlave(slaveKey)
							&& !checkedSlaves.contains(slaveKey)) {
						shortCount = datum.getShortCount();
						shortCount--;

						datum.setShortCount(shortCount);

						if (shortCount == 0) {
							checkedSlaves.clear();
							datum.mgtConditionSignalAll();
						} else {
							checkedSlaves.add(slaveKey);
						}

					}

					datum.mgtUnlock();
					toDatumMgtUnlock = false;

				}

				break;

			case VOTE_ABORT:
				self.masterCommitDataRdLock();
				toRdUnlockMasterCommitData = true;

				datum = self.getMasterCommitMgtData().get(commitIndex);

				self.masterCommitDataRdUnlock();
				toRdUnlockMasterCommitData = false;

				if (datum != null) {
					checkedSlaves = datum.getCheckedSlaves();
					slaveKey = dto.getSlaveKey();

					datum.mgtLock();
					toDatumMgtUnlock = true;

					// Make sure a VOTE_ABORT can only be received in the right
					// state and by the right replicas
					if (datum.getState() == MasterState.WAIT && datum.containsInitialSlave(slaveKey)) {
						datum.setAborted(true);
						datum.mgtConditionSignalAll();
					}

					datum.mgtUnlock();
					toDatumMgtUnlock = false;

				}

				break;

			case READY_COMMIT:

				self.masterCommitDataRdLock();
				toRdUnlockMasterCommitData = true;

				datum = self.getMasterCommitMgtData().get(commitIndex);

				self.masterCommitDataRdUnlock();
				toRdUnlockMasterCommitData = false;

				if (datum != null) {
					checkedSlaves = datum.getCheckedSlaves();
					slaveKey = dto.getSlaveKey();

					datum.mgtLock();
					toDatumMgtUnlock = true;
					// Avoid side-effects of duplicate packets and
					// late/malicious packets with a non-valid ID
					if (datum.getState() == MasterState.PRECOMMIT && datum.containsInitialSlave(slaveKey)
							&& !checkedSlaves.contains(slaveKey)) {
						shortCount = datum.getShortCount();
						shortCount--;

						datum.setShortCount(shortCount);

						if (shortCount == 0) {
							checkedSlaves.clear();
							datum.mgtConditionSignalAll();
						} else {
							checkedSlaves.add(slaveKey);
						}

					}

					datum.mgtUnlock();
					toDatumMgtUnlock = false;

				}

				break;

			case ACK:

				// Nothing to do, actually

				break;

			default:
				break;
			}
		} catch (Exception e) {
			if (toRdUnlockMasterCommitData)
				self.masterCommitDataRdUnlock();
			if (toDatumMgtUnlock)
				datum.mgtUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			throw new InternalServerErrorException();
		}
	}

	/**
	 * Start commit.
	 *
	 * @param opType
	 *            the op type
	 * @param datumToCommit
	 *            the datum to commit
	 * @return true, if successful
	 */
	/*
	 * a slave could request a commit after selecting a key for a product
	 * (requestProduct operation rather than registerProduct). This feature is
	 * currently unimplemented and this parameter can be passed as null
	 */
	private boolean startCommit(CommitOpType opType, BusinessDTO datumToCommit) {
		boolean toUnlockIndex = false;
		boolean toWrUnlockCommitData = false;
		boolean toRedundancyTableRdUnlock = false;
		boolean toIdentityRdUnlock = false;
		boolean toKeysUnderCommitUnlock = false;

		MasterCommitMgtDatum mgtDatum = null;
		boolean toMgtDatumUnlock = false;

		long masterKey;
		int commitIndex;
		boolean aborted = false;
		int shortCount, replicaListSize;

		Set<Long> keysPendingCommit;
		boolean commitInProgress = false;

		List<RedundancyIdentity> replicas = new LinkedList<RedundancyIdentity>();
		Iterator<RedundancyIdentity> iter;
		RedundancyIdentity rid = null;
		boolean isToReadyRdUnlock = false;

		try {			
			Logger.status("Starting " + opType.toString() + " commit for key " + datumToCommit.getKey() + ", owner "
					+ datumToCommit.getOwner());
			
			self.keysPendingCommitLock();
			toKeysUnderCommitUnlock = true;
			
			keysPendingCommit = self.getKeysPendingCommit();
			commitInProgress =  keysPendingCommit.contains(datumToCommit.getKey());
			
			if(!commitInProgress) {
				keysPendingCommit.add(datumToCommit.getKey());
			}
					
			keysPendingCommit = null;
			
			self.keysPendingCommitUnlock();
			toKeysUnderCommitUnlock = false;
			
			if(commitInProgress) {
				Logger.warning("Commit for key " + datumToCommit.getKey() + " already in progress. Not proceeding with new commit.");
				return false;
			}

			self.identityRdLock();
			toIdentityRdUnlock = true;

			masterKey = self.getIdentity().getKey();

			self.identityRdUnlock();
			toIdentityRdUnlock = false;

			mgtDatum = new MasterCommitMgtDatum();
			replicas = new LinkedList<RedundancyIdentity>();

			// This list will contain all the nodes partecipating in this commit
			// (which won't change regardless of node churning for its entire
			// duration)
			self.redundancyRdLock();
			toRedundancyTableRdUnlock = true;

			replicas.addAll(self.getRedundancyTable());
			replicaListSize = replicas.size();

			self.redundancyRdUnlock();
			toRedundancyTableRdUnlock = false;

			// Can't proceed with a commit if there are no replicas.
			if (replicaListSize < 1) {
				Logger.warning("No ready replica found.");
				
				self.keysPendingCommitLock();
				toKeysUnderCommitUnlock = true;
				
				self.getKeysPendingCommit().remove(datumToCommit.getKey());		
				
				self.keysPendingCommitUnlock();
				toKeysUnderCommitUnlock = false;
				
				return false;
			}

			iter = replicas.iterator();

			// Can't proceed with a commit if not all replicas are ready
			while (iter.hasNext()) {
				rid = iter.next();

				rid.readyRdLock();
				isToReadyRdUnlock = true;

				aborted = !rid.isReady();

				rid.readyRdUnlock();
				isToReadyRdUnlock = false;

				if (aborted) {					
					break;
				}
			}

			if (aborted) {
				Logger.warning("Node " + rid.getKey() + " (at least) not ready for commit. Cannot proceed to commit.");
				
				self.keysPendingCommitLock();
				toKeysUnderCommitUnlock = true;
				
				self.getKeysPendingCommit().remove(datumToCommit.getKey());		
				
				self.keysPendingCommitUnlock();
				toKeysUnderCommitUnlock = false;
				
				return false;
			}

			iter = null;
			rid = null;

			self.commitIndexLock();
			toUnlockIndex = true;

			commitIndex = self.getCommitIndex();
			self.setCommitIndex(commitIndex + 1);

			self.commitIndexUnlock();
			toUnlockIndex = false;

			mgtDatum.setCommitIndex(commitIndex);
			mgtDatum.setState(MasterState.WAIT);
			mgtDatum.setShortCount(replicaListSize);
			mgtDatum.setStagedDatum(datumToCommit);

			self.masterCommitDataWrLock();
			toWrUnlockCommitData = true;

			self.getMasterCommitMgtData().put(commitIndex, mgtDatum);

			self.masterCommitDataWrUnlock();
			toWrUnlockCommitData = false;

			mgtDatum.mgtLock();
			toMgtDatumUnlock = true;

			Logger.status("Sending VOTE REQUEST to all replicas for commit " + commitIndex + "...");

			mgtDatum.setLastHeard(System.currentTimeMillis());
			// The current time is read prior to sending the commit messages to
			// other replicas.

			// SEND VOTE-REQUEST TO ALL REPLICAS
			commitPhaseAdvance(replicas, commitIndex, masterKey, CommitMasterMsgType.VOTE_REQUEST, opType, mgtDatum);

			// Wait for the next phase
			Logger.status("Waiting for READY for commit " + commitIndex + "...");

			while (mgtDatum.getShortCount() > 0
					&& (System.currentTimeMillis() - mgtDatum.getLastHeard()) < Constants.COMMIT_TIMEOUT) {

				mgtDatum.mgtConditionTimedWait(Constants.COMMIT_TIMEOUT);

			}

			aborted = mgtDatum.isAborted();
			shortCount = mgtDatum.getShortCount();

			if (aborted || shortCount > 0) { // Timeout elapsed
				Logger.warning("Cannot proceed to commit. Aborting commit " + commitIndex);
				commitPhaseAdvance(replicas, commitIndex, masterKey, CommitMasterMsgType.GLOBAL_ABORT, null, null);
				mgtDatum.mgtUnlock();
				toMgtDatumUnlock = false;

				// Remove master commit data
				self.masterCommitDataWrLock();
				toWrUnlockCommitData = true;

				self.getMasterCommitMgtData().remove(commitIndex);

				self.masterCommitDataWrUnlock();
				toWrUnlockCommitData = false;
				
				self.keysPendingCommitLock();
				toKeysUnderCommitUnlock = true;
				
				self.getKeysPendingCommit().remove(datumToCommit.getKey());		
				
				self.keysPendingCommitUnlock();
				toKeysUnderCommitUnlock = false;

				// throw new InternalServerErrorException();
				return false;
			}

			// else move to the next phase, PRECOMMIT

			mgtDatum.setState(MasterState.PRECOMMIT);
			mgtDatum.setShortCount(replicaListSize);

			Logger.status("Sending PREPARE COMMIT to all replicas for commit " + commitIndex + "...");

			mgtDatum.setLastHeard(System.currentTimeMillis());

			commitPhaseAdvance(replicas, commitIndex, masterKey, CommitMasterMsgType.PREPARE_COMMIT, null, null);

			Logger.status("Waiting for READY COMMIT for commit " + commitIndex + "...");

			while (mgtDatum.getShortCount() > 0
					&& (System.currentTimeMillis() - mgtDatum.getLastHeard()) < Constants.COMMIT_TIMEOUT) {

				mgtDatum.mgtConditionTimedWait(Constants.COMMIT_TIMEOUT);

			}

			aborted = mgtDatum.isAborted();
			shortCount = mgtDatum.getShortCount();

			if (aborted || shortCount > 0) { // Timeout elapsed
				Logger.warning("Cannot proceed with commit. Aborting commit " + commitIndex);
				commitPhaseAdvance(replicas, commitIndex, masterKey, CommitMasterMsgType.GLOBAL_ABORT, null, null);
				mgtDatum.mgtUnlock();
				toMgtDatumUnlock = false;

				// Remove master commit data
				self.masterCommitDataWrLock();
				toWrUnlockCommitData = true;

				self.getMasterCommitMgtData().remove(commitIndex);

				self.masterCommitDataWrUnlock();
				toWrUnlockCommitData = false;
				
				self.keysPendingCommitLock();
				toKeysUnderCommitUnlock = true;
				
				self.getKeysPendingCommit().remove(datumToCommit.getKey());		
				
				self.keysPendingCommitUnlock();
				toKeysUnderCommitUnlock = false;

				// throw new InternalServerErrorException();
				return false;
			}

			// else move to the next phase, COMMIT

			mgtDatum.setState(MasterState.COMMIT);
			mgtDatum.setShortCount(replicaListSize);

			Logger.status("Sending COMMIT to all replicas for commit " + commitIndex + ". Success!");

			mgtDatum.setLastHeard(System.currentTimeMillis());

			commitPhaseAdvance(replicas, commitIndex, masterKey, CommitMasterMsgType.GLOBAL_COMMIT, null, null);

			// Whatever happens, the replicas will go to the COMMIT phase
			// henceforth

			// while(mgtDatum.getShortCount()>0 &&
			// (System.currentTimeMillis() - mgtDatum.getLastHeard()) <
			// Constants.COMMIT_TIMEOUT
			// ) {
			//
			// mgtDatum.mgtConditionTimedWait(Constants.COMMIT_TIMEOUT);
			//
			// }

			// aborted = mgtDatum.isAborted();
			// shortCount = mgtDatum.getShortCount();
			//
			// if(aborted || shortCount > 0) { //Timeout elapsed
			// commitPhaseAdvance(commitIndex, CommitMasterMsgType.GLOBAL_ABORT,
			// null, null);
			// mgtDatum.mgtUnlock();
			// toMgtDatumUnlock = false;
			//
			// //Remove master commit data
			// self.masterCommitDataWrLock();
			// toWrUnlockCommitData = true;
			//
			// self.getMasterCommitMgtData().remove(commitIndex);
			//
			// self.masterCommitDataWrUnlock();
			// toWrUnlockCommitData = false;
			//
			// throw new InternalServerErrorException();
			// return false;
			// }

			mgtDatum.mgtUnlock();
			toMgtDatumUnlock = false;

			// Remove master commit mgt datum
			self.masterCommitDataWrLock();
			toWrUnlockCommitData = true;

			self.getMasterCommitMgtData().remove(commitIndex);

			self.masterCommitDataWrUnlock();
			toWrUnlockCommitData = false;
			
			self.keysPendingCommitLock();
			toKeysUnderCommitUnlock = true;
			
			self.getKeysPendingCommit().remove(datumToCommit.getKey());		
			
			self.keysPendingCommitUnlock();
			toKeysUnderCommitUnlock = false;

			return true;

		} catch (Exception e) {
			if (toKeysUnderCommitUnlock)
				self.keysPendingCommitUnlock();
			if (toIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToReadyRdUnlock)
				rid.readyRdUnlock();
			if (toUnlockIndex)
				self.commitIndexUnlock();
			if (toRedundancyTableRdUnlock)
				self.redundancyRdUnlock();
			if (toWrUnlockCommitData)
				self.masterCommitDataWrUnlock();
			if (toMgtDatumUnlock)
				mgtDatum.mgtUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			return false;
		}
	}

	/**
	 * Commit phase advance.
	 *
	 * @param replicas
	 *            - A list of the nodes to send the commit messages to.
	 * @param commitIndex
	 *            the commit index
	 * @param masterKey
	 *            the master key
	 * @param msgType
	 *            the msg type
	 * @param opType
	 *            the op type
	 * @param mgtDatum
	 *            the mgt datum
	 * @throws Exception
	 *             the exception
	 */
	private void commitPhaseAdvance(List<RedundancyIdentity> replicas, int commitIndex, long masterKey,
			CommitMasterMsgType msgType, CommitOpType opType, MasterCommitMgtDatum mgtDatum) throws Exception {
		try {

			String url;
			URI uri;
			MasterCommitDTO masterDto;
			RequestEntity<MasterCommitDTO> request;
			Runnable runnable;
			Thread delegate;

			masterDto = new MasterCommitDTO();
			masterDto.setMsgType(msgType);
			masterDto.setCommitIndex(commitIndex);
			masterDto.setMasterKey(masterKey);
			if (msgType == CommitMasterMsgType.VOTE_REQUEST) {
				masterDto.setOpType(opType);
				masterDto.setDatum(mgtDatum.getStagedDatum());
			}

			if (msgType == CommitMasterMsgType.VOTE_REQUEST) {
				for (Identity id : replicas) {
					mgtDatum.addInitialSlave(id.getKey());
				}
			}

			for (Identity id : replicas) {

				url = Constants.HTTP_PRIMER + id.getAddress() + Constants.APP_NAME_PATH + Constants.COMMIT_SLAVE;
				uri = new URI(url);

				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(masterDto);

				runnable = new HttpRequestDelegateRunnable(request);
				delegate = new Thread(runnable);
				delegate.start();
			}

		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			throw e;
		}
	}
}
