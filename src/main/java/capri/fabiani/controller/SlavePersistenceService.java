/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.MasterCommitDTO;
import capri.fabiani.model.rest.SlaveInitPrimeDTO;
import capri.fabiani.model.rest.SlavePutInChargeDTO;
import capri.fabiani.model.rest.SlavePutInChargeResponseDTO;
import capri.fabiani.model.rest.TransportKeysDTO;

/**
 * The Interface SlavePersistenceService.
 */
public interface SlavePersistenceService {
	
	/**
	 * The Enum CommitSlaveMsgType.
	 */
	public enum CommitSlaveMsgType {
/** The vote abort. */
VOTE_ABORT, 
 /** The vote commit. */
 VOTE_COMMIT, 
 /** The ready commit. */
 READY_COMMIT, 
 /** The ack. */
 ACK};
	
	/**
	 * Put in charge.
	 *
	 * @param boss the boss
	 * @return the slave put in charge response dto
	 */
	public SlavePutInChargeResponseDTO putInCharge(SlavePutInChargeDTO boss);
	
	/**
	 * Dismiss.
	 *
	 * @param bossKey the boss key
	 */
	public void dismiss(long bossKey);

/**
 * Commit.
 *
 * @param dto the dto
 */
//	public void keepAlive(IdentityDTO boss);
	public void commit(MasterCommitDTO dto);
	
	/**
	 * Gets the product.
	 *
	 * @param productCredentials the product credentials
	 * @param masterId the master id
	 * @return the product
	 */
	public void getProduct(BusinessDTO productCredentials, long masterId);
	
	/**
	 * Request product.
	 *
	 * @param productClass the product class
	 * @param masterId the master id
	 */
	public void requestProduct(int productClass, long masterId);
	
	/**
	 * Transfer keys.
	 *
	 * @param dto the dto
	 * @return the string
	 */
	public String transferKeys(TransportKeysDTO dto);
	
	/**
	 * Transfer init.
	 *
	 * @param dto the dto
	 * @param masterId the master id
	 * @return the string
	 */
	public String transferInit(SlaveInitPrimeDTO dto, long masterId);
}
