/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;



import capri.fabiani.model.rest.FingerFillTableDTO;
import capri.fabiani.model.rest.FingerFindPredecessorDTO;
import capri.fabiani.model.rest.FingerNextNodesDTO;
import capri.fabiani.model.rest.FingerNotifyPredecessorDTO;
import capri.fabiani.model.rest.FingerResponseDTO;
import capri.fabiani.model.rest.IdentityDTO;
import capri.fabiani.model.rest.FingerNotifySuccessorDTO;

/**
 * The Interface FingerTableService.
 */
public interface FingerTableService {
	
	/**
	 * Fill table.
	 *
	 * @param dto the dto
	 * @return the string
	 */
	public String fillTable(FingerFillTableDTO dto);
	
	/**
	 * Notify successor.
	 *
	 * @param dto the dto
	 * @return the string
	 */
	public String notifySuccessor(FingerNotifySuccessorDTO dto);
	
	/**
	 * Notify predecessor.
	 *
	 * @param dto the dto
	 * @return the string
	 */
	public String notifyPredecessor(FingerNotifyPredecessorDTO dto);
	
	/**
	 * Scan next nodes.
	 *
	 * @param taskDto the task dto
	 * @return the string
	 */
	public String scanNextNodes(FingerNextNodesDTO taskDto);
	
	/**
	 * Node response.
	 *
	 * @param responseDto the response dto
	 * @param taskId the task id
	 */
	public void nodeResponse(FingerResponseDTO responseDto, int taskId);
	
	/**
	 * Find predecessor.
	 *
	 * @param dto the dto
	 * @return the string
	 */
	public String findPredecessor(FingerFindPredecessorDTO dto);
	
	/**
	 * Gets the forwardee.
	 *
	 * @param requestedKey the requested key
	 * @return the forwardee
	 */
	public IdentityDTO getForwardee(long requestedKey);
//	public void keepAlive(Identity requestingId);
//	public void leaveSuccessor(Identity predecessorId);
//	public void leavePredecessor(Identity successorId);
}
