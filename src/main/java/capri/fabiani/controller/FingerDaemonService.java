/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import capri.fabiani.model.rest.IdentityDTO;

/**
 * The Interface FingerDaemonService.
 */
public interface FingerDaemonService {
	
	/**
	 * Range updated.
	 *
	 * @param newPredecessor the new predecessor
	 */
	public void rangeUpdated(IdentityDTO newPredecessor);
	
	/**
	 * Notified as predecessor.
	 */
	public void notifiedAsPredecessor();
}
