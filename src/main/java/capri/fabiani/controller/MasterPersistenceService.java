/*
 * @author Dan&Dan
 */
package capri.fabiani.controller;

import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.SlaveCommitDTO;

/**
 * The Interface MasterPersistenceService.
 */
public interface MasterPersistenceService {
	
	/**
	 * The Enum CommitMasterMsgType.
	 */
	public enum CommitMasterMsgType {
/** The vote request. */
VOTE_REQUEST, 
 /** The global abort. */
 GLOBAL_ABORT, 
 /** The prepare commit. */
 PREPARE_COMMIT, 
 /** The global commit. */
 GLOBAL_COMMIT};
	
	/**
	 * The Enum CommitOpType.
	 */
	public enum CommitOpType {
/** The create. */
CREATE, 
 /** The update. */
 UPDATE, 
 /** The delete. */
 DELETE};
	
	/**
	 * Commit.
	 *
	 * @param dto the dto
	 * @param commitIndex the commit index
	 */
	public void commit(SlaveCommitDTO dto, int commitIndex);
	
	/**
	 * Gets the product.
	 *
	 * @param productCredentials the product credentials
	 * @return the product
	 */
	public boolean getProduct(BusinessDTO productCredentials);
	
	/**
	 * Register product.
	 *
	 * @param newProduct the new product
	 * @return true, if successful
	 */
	public boolean registerProduct(BusinessDTO newProduct);
	
	/**
	 * Request product.
	 *
	 * @param productClass the product class
	 */
	public void requestProduct(int productClass);
	
	/**
	 * Delete product.
	 *
	 * @param datumCredentials the datum credentials
	 * @return true, if successful
	 */
	public boolean deleteProduct(BusinessDTO datumCredentials);
	
	/**
	 * Update product.
	 *
	 * @param productCredentialsAndData the product credentials and data
	 * @return true, if successful
	 */
	public boolean updateProduct(BusinessDTO productCredentialsAndData);
//	public void write(BusinessDTO datumDto);
}
