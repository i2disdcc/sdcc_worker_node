/*
 * @author Dan&Dan
 */
package capri.fabiani;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class WorkerNodeApplication.
 */
@SpringBootApplication
public class WorkerNodeApplication {

	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
	
		SpringApplication.run(WorkerNodeApplication.class, args);		
		
	}
}
