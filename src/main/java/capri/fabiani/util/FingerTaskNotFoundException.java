/*
 * @author Dan&Dan
 */
package capri.fabiani.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import capri.fabiani.configuration.Constants;

/**
 * The Class FingerTaskNotFoundException.
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND, reason=Constants.NOT_FOUND_MSG)
public class FingerTaskNotFoundException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4652752397095057119L;

}
