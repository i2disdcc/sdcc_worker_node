/*
 * @author Dan&Dan
 */
package capri.fabiani.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import capri.fabiani.model.RedundancyManagementData;

/**
 * The Class KeyRangeUtils.
 */
public class KeyRangeUtils {
	
	/**
	 * Sorts the list performing an "insertion sort". Deemed acceptable as it is
	 * assumed the redundancy level will hardly exceed 20 or so.
	 *
	 * @param listToSort the list to sort
	 * @return the list
	 */
	public static List<RedundancyManagementData> keyRangeSort(List<RedundancyManagementData> listToSort) {

		RedundancyManagementData rightmost, leftmost;
		ListIterator<RedundancyManagementData> iter;
		List<RedundancyManagementData> tempList;
		
		boolean gapFound = false;

		if (listToSort == null || listToSort.size() < 2)
			return listToSort;

		//Sort by upper value, regardless of the lower value
		Collections.sort(listToSort, new Comparator<RedundancyManagementData>() {

			@Override
			public int compare(RedundancyManagementData arg0, RedundancyManagementData arg1) {
				if(arg0.getKey()<arg1.getKey())
					return -1;
				if(arg0.getKey()>arg1.getKey())
					return 1;
				return 0;
			}
			
		});

		/*
		 * We enforce an architecture where each node has a set of consecutive
		 * keys (including both its own keys and other masters'). This list may
		 * be sorted by increasing (upper-)key value, but there may be a gap
		 * within the thus covered range. We look for that gap, and see if we
		 * need to put the rest of the list at its head instead
		 */

		iter = listToSort.listIterator();

		// Won't throw a NullPointerException as listToSort.size() >= 2
		leftmost = iter.next();

		while (iter.hasNext()) {
			rightmost = iter.next();
			// Found the gap
			if (leftmost.getKey() - rightmost.getLowEnd() > 1) {
				gapFound = true;
				break;
			}

			leftmost = rightmost;
		}

		// No gap detected. Range most likely without gaps.
		if (!gapFound) {
			return listToSort;
		}

		// The range should have at most one gap.

		// The range of the element at the end of the list ends right before the
		// range at the beginning of the list.
		if (listToSort.get(listToSort.size() - 1).getKey() == (listToSort.get(0).getLowEnd() - 1)) {
			tempList = new LinkedList<RedundancyManagementData>();
			while (iter.hasNext()) {
				rightmost = iter.next();
				tempList.add(rightmost);
				iter.remove();
			}

			tempList.addAll(listToSort);

			return tempList;
		} else {
			// There's more than one gap. Better not touch anything.
			return listToSort;
		}

	}
}
