/*
 * @author Dan&Dan
 */
package capri.fabiani.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Throwables;

import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.DatumService;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.TransportKeysDTO;

/**
 * The Class SenderReceiverUtils.
 */
public class SenderReceiverUtils {

	/**
	 * Send interval.
	 *
	 * @param begin the begin
	 * @param end the end
	 * @param url the url
	 * @param datumService the datum service
	 * @return true, if successful
	 */
	public static boolean sendInterval(long begin, long end, String url, DatumService datumService) {
		// TODO vedere errori (restituzione false anziché true), vedere dal lato
		// di chi invoca che da chi riceve i dati
		try {
			RequestEntity<TransportKeysDTO> request;
			RestTemplate restTemplate;
			ResponseEntity<String> response;
			TransportKeysDTO tkDTO = new TransportKeysDTO();
			// tkDTO.setLastone(false);
			List<BusinessDTO> toSend = datumService.findByIdRange(begin, end);
			if (toSend.isEmpty()) {
				Logger.warning("list in sendInterval is empty");
			}
			List<BusinessDTO> basicDatums = new ArrayList<>();
			BusinessDTO tempDatum;
			int index = 0;
			long totalNumb = toSend.size();
			Logger.status("sending " + totalNumb + " keys from " + begin + " to " + end);
			while (totalNumb > 0) {
				basicDatums.clear();
				for (int i = 0; i < Constants.NUMBER_OF_MAX_DATUM_TO_SEND && totalNumb > 0; i++) {
					tempDatum = toSend.remove(0);
					totalNumb--;
					basicDatums.add(tempDatum);
					// if(totalNumb==0){
					// tkDTO.setLastone(true);
					// }

				}

				// url = Constants.HTTP_PRIMER +
				// id.getAddress().getHostAddress() +
				// Constants.COMMIT_SLAVE;
				URI uri = null;
				try {
					uri = new URI(url);
				} catch (URISyntaxException e) {

					Logger.error(Throwables.getStackTraceAsString(e));
				}
				tkDTO.setDatas(basicDatums);
				tkDTO.setMessageIndex(index);
				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(tkDTO);
				// TODO non è gestito il caso di errore
				restTemplate = HttpEntityConfiguration.getTemplate();

				response = restTemplate.exchange(request, String.class);

				index++;
				Logger.status("Sent " + index + "° message");
				if (response.getStatusCode() != HttpStatus.OK) {
					Logger.warning(
							"Error on " + index + "° message, status code was: " + response.getStatusCode().toString());
					return false;
				}

			}
		} catch (Exception e) {
			Logger.error("Error sending keys. Printstacktrace: " + Throwables.getStackTraceAsString(e));
			return false;
		}
		return true;
	}

	/**
	 * Receive interval.
	 *
	 * @param transportKeysDTO the transport keys dto
	 * @param datumService the datum service
	 * @return the long[]
	 */
	public static long[] receiveInterval(TransportKeysDTO transportKeysDTO, DatumService datumService) {
		List<BusinessDTO> datas = transportKeysDTO.getDatas();
		long max = 0, min = Long.MAX_VALUE;
		for (BusinessDTO data : datas) {
			if (data.getKey() > max)
				max = data.getKey();
			if (data.getKey() < min)
				min = data.getKey();
			datumService.create(data);

		}
		// TODO non serve più l'intervallo perchè è inserito nel messaggio di
		// hanshake
		long[] interval = { min, max };
		return interval;

	}

}
