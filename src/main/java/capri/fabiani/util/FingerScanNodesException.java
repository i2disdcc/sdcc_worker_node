/*
 * @author Dan&Dan
 */
package capri.fabiani.util;

import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The Class FingerScanNodesException.
 */
//TODO
@ResponseStatus()
public class FingerScanNodesException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8903525299374222130L;

}
