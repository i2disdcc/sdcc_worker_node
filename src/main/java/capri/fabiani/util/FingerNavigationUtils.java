/*
 * @author Dan&Dan
 */
package capri.fabiani.util;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Throwables;

import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.extra.interfaces.FingerTableReaderLocker;
import capri.fabiani.extra.interfaces.NextNodesReaderLocker;
import capri.fabiani.model.Identity;
import capri.fabiani.model.rest.FingerFillTableDTO;
import capri.fabiani.model.rest.FingerNextNodesDTO;

/**
 * The Class FingerNavigationUtils.
 */
public class FingerNavigationUtils {
	
	/**
	 * Forward for next nodes.
	 *
	 * @param readerLocker the reader locker
	 * @param dto the dto
	 * @return true, if successful
	 */
	public static boolean forwardForNextNodes(NextNodesReaderLocker readerLocker, FingerNextNodesDTO dto) {

		boolean isToNextNodesRdUnlock = false;

		String url;
		URI uri;
		RequestEntity<FingerNextNodesDTO> request;
		ResponseEntity<String> response;
		RestTemplate template = HttpEntityConfiguration.getTemplate();

		boolean allClear = false;

		List<Identity> nextNodes;

		try {

			readerLocker.nextNodesRdLock();
			isToNextNodesRdUnlock = true;

			nextNodes = readerLocker.getNextNodes();

			for (Identity node : nextNodes) {
				url = Constants.HTTP_PRIMER + node.getAddress() + Constants.APP_NAME_PATH + Constants.FINGER_NEXT_NODES;
				uri = new URI(url);

				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(dto);

				Logger.status("Forwarding scan next nodes operation to node " + node.toString() + "...");
				try {
					response = template.exchange(request, String.class);
					allClear = response.getStatusCode() == HttpStatus.OK;

					if (allClear) {
						Logger.status(
								"Scan next nodes operation successuflly forwarded to node " + node.toString() + "!");
						break;
					}
				} catch (Exception e) {
					Logger.error(Throwables.getStackTraceAsString(e));
					allClear = false;
				}

				Logger.warning("Failed to forward scan next nodes operation to node " + node.toString() + ".");

				dto.setFirstSendFailed(true);
			}

			nextNodes = null;

			readerLocker.nextNodesRdUnlock();
			isToNextNodesRdUnlock = false;

			if (!allClear) {
				Logger.warning("Could not forward scan next nodes operation to any successor node.");
			}

		} catch (Exception e) {
			if (isToNextNodesRdUnlock)
				readerLocker.nextNodesRdUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
			allClear = false;
		}

		return allClear;
	}

	/**
	 * Forward for fill table.
	 *
	 * @param readerLocker the reader locker
	 * @param dto the dto
	 * @return true, if successful
	 */
	public static boolean forwardForFillTable(FingerTableReaderLocker readerLocker, FingerFillTableDTO dto) {

		boolean isToFingerRdUnlock = false;
		boolean isToNextNodesRdUnlock = false;

		String url;
		URI uri;
		RequestEntity<FingerFillTableDTO> request;
		ResponseEntity<String> response;
		RestTemplate template = HttpEntityConfiguration.getTemplate();

		boolean allClear = false;

		List<Identity> primaryNodeList;

		Identity node;
		long reqKey = dto.getReqKey();

		try {

			readerLocker.fingerRdLock();
			isToFingerRdUnlock = true;

			primaryNodeList = readerLocker.getFingerTable();

			node = primaryNodeList.get(0);

			// If this is null, so is the first next node
			if (node != null) {

				if (reqKey <= node.getKey()) {
					primaryNodeList = null;
					node = null;

					readerLocker.fingerRdUnlock();
					isToFingerRdUnlock = false;

					readerLocker.nextNodesRdLock();
					isToNextNodesRdUnlock = true;

					primaryNodeList = readerLocker.getNextNodes();

					for (Identity forwardee : primaryNodeList) {
						url = Constants.HTTP_PRIMER + forwardee.getAddress() + Constants.APP_NAME_PATH
								+ Constants.FINGER_FILL_TABLE;
						uri = new URI(url);

						request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(dto);

						Logger.status("Forwarding fill table operation to primary successor node "
								+ forwardee.toString() + "...");
						try {
							response = template.exchange(request, String.class);
							allClear = response.getStatusCode() == HttpStatus.OK;

							if (allClear) {
								Logger.status("Fill table operation successuflly forwarded to primary successor node "
										+ forwardee.toString() + "!");
								break;
							}
						} catch (Exception e) {
							Logger.error(Throwables.getStackTraceAsString(e));
							allClear = false;
						}
						Logger.warning("Failed to forward fill table operation to primary successor node "
								+ forwardee.toString() + ".");

						dto.setFirstSendFailed(true);
					}

					primaryNodeList = null;

					readerLocker.nextNodesRdUnlock();
					isToNextNodesRdUnlock = false;

					if (!allClear) {
						Logger.warning("Could not forward fill table operation to any successor node.");
					}

					return allClear;

				} else {
					// TODO forward on fingerTable

					List<Identity> fallBackNodes = new ArrayList<Identity>(Constants.F_TABLE_SIZE);
					Iterator<Identity> iterator = primaryNodeList.iterator();

					// Fill the backup nodes list.
					while (iterator.hasNext()) {
						node = iterator.next();

						if (reqKey > node.getKey()) {
							fallBackNodes.add(node);
						} else {
							break;
						}
					}

					// Try to forward to the furthest node available
					for (int i = fallBackNodes.size() - 1; i > -1; i--) {
						node = fallBackNodes.get(i);

						url = Constants.HTTP_PRIMER + node.getAddress() + Constants.APP_NAME_PATH
								+ Constants.FINGER_FILL_TABLE;
						uri = new URI(url);

						request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(dto);

						Logger.status("Forwarding fill table operation to finger node " + node.toString() + "...");
						try {
							response = template.exchange(request, String.class);
							allClear = response.getStatusCode() == HttpStatus.OK;

							if (allClear) {
								Logger.status("Fill table operation successuflly forwarded to finger node "
										+ node.toString() + "!");
								break;
							}
						} catch (Exception e) {
							Logger.error(Throwables.getStackTraceAsString(e));
							allClear = false;
						}
						Logger.warning(
								"Failed to forward fill table operation to finger node " + node.toString() + ".");

					}

				}

			}

			primaryNodeList = null;
			node = null;

			readerLocker.fingerRdUnlock();
			isToFingerRdUnlock = false;

		} catch (Exception e) {
			if (isToFingerRdUnlock)
				readerLocker.fingerRdUnlock();
			if (isToNextNodesRdUnlock)
				readerLocker.nextNodesRdUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		return allClear;
	}
}
