/*
 * @author Dan&Dan
 */
package capri.fabiani.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import capri.fabiani.configuration.Constants;

/**
 * The Class InternalServerErrorException.
 */
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason = Constants.INTERNAL_SERVER_ERROR_MSG)
public class InternalServerErrorException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5922313515352274522L;

}
