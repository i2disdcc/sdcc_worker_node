/*
 * @author Dan&Dan
 */
package capri.fabiani.util;

import java.time.Instant;
import java.time.ZoneId;

import capri.fabiani.configuration.Constants;

/**
 * The Class Logger.
 */
public class Logger {

	/**
	 * Warning.
	 *
	 * @param s the s
	 */
	public synchronized static void warning(String s) {
		if (Constants.VERBOSE)
			System.out.println(
					"[WARNING @" + getFileName() + "(" + getLineNumber() + ") " + getTimestamp() + "]: " + s);
	}

	/**
	 * Error.
	 *
	 * @param s the s
	 */
	public synchronized static void error(String s) {
		if (Constants.VERBOSE)
			System.out.println(
					"[ERROR @" + getFileName() + "(" + getLineNumber() + ") " + getTimestamp() + "]: " + s);
	}

	/**
	 * Status.
	 *
	 * @param s the s
	 */
	public synchronized static void status(String s) {
		if (Constants.VERBOSE)
			System.out.println(
					"[STATUS @" + getFileName() + "(" + getLineNumber() + ") " + getTimestamp() + "]: " + s);
	}
	
	public static void printStackTrace(){
		if (Constants.VERBOSE){
			System.out.println("[STACKTRACE " + getTimestamp()+"]:");
			StackTraceElement[] st = Thread.currentThread().getStackTrace();
			for (int i = 2; i < st.length; i++) {
				System.out.println(st[i].toString());
			}
			
		}
	}

	/**
	 * Gets the line number.
	 *
	 * @return the line number
	 */
	private static int getLineNumber() {
		return Thread.currentThread().getStackTrace()[3].getLineNumber();
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	private static String getFileName() {
		return Thread.currentThread().getStackTrace()[3].getFileName();
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	private static String getTimestamp() {
		return Instant.now().toString();
	}

}
