/*
 * @author Dan&Dan
 */
package capri.fabiani.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import capri.fabiani.configuration.Constants;

/**
 * The Class FingerMalformedPacketException.
 */
@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason=Constants.MALFORMED_PACKET_MSG)
public class FingerMalformedPacketException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8402764201281238798L;

}
