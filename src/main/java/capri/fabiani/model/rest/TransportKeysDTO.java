/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The Class TransportKeysDTO.
 */
public class TransportKeysDTO extends DTO{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The datas. */
	@JsonInclude(Include.NON_NULL)
	private List<BusinessDTO> datas;
	
	/** The id sender. */
	private long idSender;
	
	/** The message index. */
	private int messageIndex;
	
	
	
	
	/**
	 * Instantiates a new transport keys dto.
	 *
	 * @param datas the datas
	 * @param idSender the id sender
	 * @param messageIndex the message index
	 */
	public TransportKeysDTO(List<BusinessDTO> datas, long idSender, int messageIndex) {
		super();
		this.datas = datas;
		this.idSender = idSender;
		this.messageIndex = messageIndex;
	}
	
	/**
	 * Instantiates a new transport keys dto.
	 */
	public TransportKeysDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Instantiates a new transport keys dto.
	 *
	 * @param error the error
	 */
	public TransportKeysDTO(String error) {
		super(error);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the datas.
	 *
	 * @return the datas
	 */
	public List<BusinessDTO> getDatas() {
		return datas;
	}
	
	/**
	 * Sets the datas.
	 *
	 * @param datas the new datas
	 */
	public void setDatas(List<BusinessDTO> datas) {
		this.datas = datas;
	}
	
	/**
	 * Gets the id sender.
	 *
	 * @return the id sender
	 */
	public long getIdSender() {
		return idSender;
	}
	
	/**
	 * Sets the id sender.
	 *
	 * @param idSender the new id sender
	 */
	public void setIdSender(long idSender) {
		this.idSender = idSender;
	}
	
	/**
	 * Gets the message index.
	 *
	 * @return the message index
	 */
	public int getMessageIndex() {
		return messageIndex;
	}
	
	/**
	 * Sets the message index.
	 *
	 * @param messageIndex the new message index
	 */
	public void setMessageIndex(int messageIndex) {
		this.messageIndex = messageIndex;
	}
	
	
	

}
