package capri.fabiani.model.rest;

import java.util.List;

public class StatusExtDTO extends StatusDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9059482542653938778L;
	private List<BusinessDTO> entries;
	
	public List<BusinessDTO> getEntries() {
		return entries;
	}
	
	public void setEntries(List<BusinessDTO> entries) {
		this.entries = entries;
	}
}
