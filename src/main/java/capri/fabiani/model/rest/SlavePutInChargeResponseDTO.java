/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class SlavePutInChargeResponseDTO.
 */
public class SlavePutInChargeResponseDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7750217431788619178L;
	
	/** The count. */
	private long count;
	
	/** The lower end. */
	private long lowerEnd;
	
	/** The upper end. */
	private long upperEnd;
	
	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	public long getCount() {
		return count;
	}
	
	/**
	 * Sets the count.
	 *
	 * @param count the new count
	 */
	public void setCount(long count) {
		this.count = count;
	}
	
	/**
	 * Gets the lower end.
	 *
	 * @return the lower end
	 */
	public long getLowerEnd() {
		return lowerEnd;
	}
	
	/**
	 * Sets the lower end.
	 *
	 * @param lowerEnd the new lower end
	 */
	public void setLowerEnd(long lowerEnd) {
		this.lowerEnd = lowerEnd;
	}
	
	/**
	 * Gets the upper end.
	 *
	 * @return the upper end
	 */
	public long getUpperEnd() {
		return upperEnd;
	}
	
	/**
	 * Sets the upper end.
	 *
	 * @param upperEnd the new upper end
	 */
	public void setUpperEnd(long upperEnd) {
		this.upperEnd = upperEnd;
	}
}
