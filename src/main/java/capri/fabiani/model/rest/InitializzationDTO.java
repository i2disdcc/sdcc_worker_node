/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The Class InitializzationDTO.
 */
public class InitializzationDTO extends DTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The upper bound. */
	@JsonInclude(Include.NON_NULL)
	private long loweBound, upperBound;
	
	/** The predecessor. */
	@JsonInclude(Include.NON_NULL)
	private String Successor,predecessor;

	/**
	 * Gets the lowe bound.
	 *
	 * @return the lowe bound
	 */
	public long getLoweBound() {
		return loweBound;
	}

	/**
	 * Sets the lowe bound.
	 *
	 * @param loweBound the new lowe bound
	 */
	public void setLoweBound(long loweBound) {
		this.loweBound = loweBound;
	}

	/**
	 * Gets the upper bound.
	 *
	 * @return the upper bound
	 */
	public long getUpperBound() {
		return upperBound;
	}

	/**
	 * Sets the upper bound.
	 *
	 * @param upperBound the new upper bound
	 */
	public void setUpperBound(long upperBound) {
		this.upperBound = upperBound;
	}

	/**
	 * Gets the successor.
	 *
	 * @return the successor
	 */
	public String getSuccessor() {
		return Successor;
	}

	/**
	 * Sets the successor.
	 *
	 * @param successor the new successor
	 */
	public void setSuccessor(String successor) {
		Successor = successor;
	}

	/**
	 * Gets the predecessor.
	 *
	 * @return the predecessor
	 */
	public String getPredecessor() {
		return predecessor;
	}

	/**
	 * Sets the predecessor.
	 *
	 * @param predecessor the new predecessor
	 */
	public void setPredecessor(String predecessor) {
		this.predecessor = predecessor;
	}
	
	
	
}
