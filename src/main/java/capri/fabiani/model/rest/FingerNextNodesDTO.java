/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class FingerNextNodesDTO.
 */
public class FingerNextNodesDTO  extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6983579825573230074L;
	
	/** The request source. */
	private IdentityDTO requestSource;
	
	/** The task id. */
	private int taskId;
	
	/** The short count. */
	private int shortCount;
	
	/** The first send failed. */
	private boolean firstSendFailed = false;;
	
	/**
	 * Instantiates a new finger next nodes dto.
	 */
	public FingerNextNodesDTO() {}
	
	/**
	 * Instantiates a new finger next nodes dto.
	 *
	 * @param requestSource the request source
	 * @param taskId the task id
	 * @param shortCount the short count
	 */
	public FingerNextNodesDTO(IdentityDTO requestSource, int taskId, int shortCount) {
		this.requestSource = requestSource;
		this.taskId = taskId;
		this.shortCount = shortCount;
	}

	/**
	 * Gets the request source.
	 *
	 * @return the request source
	 */
	public IdentityDTO getRequestSource() {
		return requestSource;
	}

	/**
	 * Sets the request source.
	 *
	 * @param requestSource the new request source
	 */
	public void setRequestSource(IdentityDTO requestSource) {
		this.requestSource = requestSource;
	}

	/**
	 * Gets the task id.
	 *
	 * @return the task id
	 */
	public int getTaskId() {
		return taskId;
	}

	/**
	 * Sets the task id.
	 *
	 * @param taskId the new task id
	 */
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	/**
	 * Gets the short count.
	 *
	 * @return the short count
	 */
	public int getShortCount() {
		return shortCount;
	}

	/**
	 * Sets the short count.
	 *
	 * @param shortCount the new short count
	 */
	public void setShortCount(int shortCount) {
		this.shortCount = shortCount;
	}

	/**
	 * Checks if is first send failed.
	 *
	 * @return true, if is first send failed
	 */
	public boolean isFirstSendFailed() {
		return firstSendFailed;
	}

	/**
	 * Sets the first send failed.
	 *
	 * @param firstSendFailed the new first send failed
	 */
	public void setFirstSendFailed(boolean firstSendFailed) {
		this.firstSendFailed = firstSendFailed;
	}
}
