/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class RetirementRequestDTO.
 */
public class RetirementRequestDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6336833651641296220L;
	
	/** The key. */
	private long key;
	
	/** The address. */
	private String address;
	
	/** The short count. */
	private int shortCount;

	/**
	 * Instantiates a new retirement request dto.
	 */
	public RetirementRequestDTO(){}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public long getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(long key) {
		this.key = key;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the short count.
	 *
	 * @return the short count
	 */
	public int getShortCount() {
		return shortCount;
	}

	/**
	 * Sets the short count.
	 *
	 * @param shortCount the new short count
	 */
	public void setShortCount(int shortCount) {
		this.shortCount = shortCount;
	}
}
