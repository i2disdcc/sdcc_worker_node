/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import capri.fabiani.model.Identity;

/**
 * The Class IdentityDTO.
 */
public class IdentityDTO extends DTO implements Comparable<IdentityDTO> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4982720215692522790L;
	
	/** The address. */
	protected String address;
	
	/** The key. */
	protected long key;
	
	/**
	 * Instantiates a new identity dto.
	 */
	public IdentityDTO() {}
	
	/**
	 * Instantiates a new identity dto.
	 *
	 * @param id the id
	 */
	public IdentityDTO(Identity id) {
		this.key = id.getKey();
		this.address = id.getAddress();
	}
	
	/**
	 * Instantiates a new identity dto.
	 *
	 * @param address the address
	 * @param key the key
	 */
	public IdentityDTO(String address, long key) {
		this.address = address;
		this.key = key;
	}
	
	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public long getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(long key) {
		this.key = key;
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.model.rest.DTO#toString()
	 */
	@Override
	public String toString() {
		return "{\"key\" : \""+key+"\", \"address\" : \""+address+"\"}";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(IdentityDTO arg0) {
		
		if(this.key < arg0.getKey())
			return -1;
		
		if(this.key > arg0.getKey())
			return 1;
		
		return 0;
	}
	
}
