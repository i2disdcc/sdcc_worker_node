/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import capri.fabiani.model.Identity;

/**
 * The Class HelloDTO.
 */
public class HelloDTO extends DTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The num messages. */
	@JsonInclude(Include.NON_NULL)
	int numMessages;
	
	/** The upper bound interval. */
	@JsonInclude(Include.NON_NULL)
	long upperBoundInterval=-1;
	
	/** The id pred. */
	@JsonInclude(Include.NON_NULL)
	Identity idPred;
	
	/** The id succ. */
	@JsonInclude(Include.NON_NULL)
	Identity idSucc;
	
	/**
	 * Instantiates a new hello dto.
	 */
	public HelloDTO(){}


	/**
	 * Gets the num messages.
	 *
	 * @return the num messages
	 */
	public int getNumMessages() {
		return numMessages;
	}

	/**
	 * Sets the num messages.
	 *
	 * @param numMessages the new num messages
	 */
	public void setNumMessages(int numMessages) {
		this.numMessages = numMessages;
	}

	/**
	 * Gets the upper bound interval.
	 *
	 * @return the upper bound interval
	 */
	public long getUpperBoundInterval() {
		return upperBoundInterval;
	}

	/**
	 * Sets the upper bound interval.
	 *
	 * @param upperBoundInterval the new upper bound interval
	 */
	public void setUpperBoundInterval(long upperBoundInterval) {
		this.upperBoundInterval = upperBoundInterval;
	}

	/**
	 * Gets the id pred.
	 *
	 * @return the id pred
	 */
	public Identity getIdPred() {
		return idPred;
	}

	/**
	 * Sets the id pred.
	 *
	 * @param idPred the new id pred
	 */
	public void setIdPred(Identity idPred) {
		this.idPred = idPred;
	}

	/**
	 * Gets the id succ.
	 *
	 * @return the id succ
	 */
	public Identity getIdSucc() {
		return idSucc;
	}

	/**
	 * Sets the id succ.
	 *
	 * @param idSucc the new id succ
	 */
	public void setIdSucc(Identity idSucc) {
		this.idSucc = idSucc;
	}
	
}
