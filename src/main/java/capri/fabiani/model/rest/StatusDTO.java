/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import capri.fabiani.configuration.Constants;

/**
 * The Class StatusDTO.
 */
public class StatusDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2659288134714766155L;

	/** The identity. */
	@JsonInclude(Include.NON_NULL)
	private IdentityDTO identity;
	
	/** The predecessor. */
	@JsonInclude(Include.NON_NULL)
	private IdentityDTO predecessor;
	
	/** The finger table. */
	@JsonInclude(Include.NON_NULL)
	private List<IdentityDTO> fingerTable;
	
	/** The next nodes. */
	@JsonInclude(Include.NON_NULL)
	private List<IdentityDTO> nextNodes;
	
	/** The replicas. */
	@JsonInclude(Include.NON_NULL)
	private List<IdentityDTO> replicas;
	
	/** The masters. */
	@JsonInclude(Include.NON_NULL)
	private List<IdentityDTO> masters;
	
	/** The range. */
	@JsonInclude(Include.NON_NULL)
	private long[] range;
	
	/** The key count. */
	@JsonInclude(Include.NON_NULL)
	private long keyCount;
	
	/**
	 * Instantiates a new status dto.
	 */
	public StatusDTO() {
		identity = null;
		predecessor = null;
		fingerTable = null;
		replicas = null;
		masters = null;
		range = new long[]{-1l, -1l};
		keyCount = 0;
	}

	/**
	 * Gets the identity.
	 *
	 * @return the identity
	 */
	public IdentityDTO getIdentity() {
		return identity;
	}

	/**
	 * Sets the identity.
	 *
	 * @param identity the new identity
	 */
	public void setIdentity(IdentityDTO identity) {
		this.identity = identity;
		range[1] = identity.getKey();
	}

	/**
	 * Gets the predecessor.
	 *
	 * @return the predecessor
	 */
	public IdentityDTO getPredecessor() {
		return predecessor;
	}

	/**
	 * Sets the predecessor.
	 *
	 * @param predecessor the new predecessor
	 */
	public void setPredecessor(IdentityDTO predecessor) {
		this.predecessor = predecessor;
		long lowEnd = (predecessor.getKey()+1)%Constants.MODULUS;
		if(lowEnd < 0) {
			lowEnd+=Constants.MODULUS;
		}
		range[0] = lowEnd;
	}

	/**
	 * Gets the finger table.
	 *
	 * @return the finger table
	 */
	public List<IdentityDTO> getFingerTable() {
		return fingerTable;
	}

	/**
	 * Sets the finger table.
	 *
	 * @param fingerTable the new finger table
	 */
	public void setFingerTable(List<IdentityDTO> fingerTable) {
		this.fingerTable = fingerTable;
	}
	
	/**
	 * Gets the next nodes.
	 *
	 * @return the next nodes
	 */
	public List<IdentityDTO> getNextNodes() {
		return nextNodes;
	}
	
	/**
	 * Sets the next nodes.
	 *
	 * @param nextNodes the new next nodes
	 */
	public void setNextNodes(List<IdentityDTO> nextNodes) {
		this.nextNodes = nextNodes;
	}

	/**
	 * Gets the replicas.
	 *
	 * @return the replicas
	 */
	public List<IdentityDTO> getReplicas() {
		return replicas;
	}

	/**
	 * Sets the replicas.
	 *
	 * @param replicas the new replicas
	 */
	public void setReplicas(List<IdentityDTO> replicas) {
		this.replicas = replicas;
	}

	/**
	 * Gets the masters.
	 *
	 * @return the masters
	 */
	public List<IdentityDTO> getMasters() {
		return masters;
	}

	/**
	 * Sets the masters.
	 *
	 * @param masters the new masters
	 */
	public void setMasters(List<IdentityDTO> masters) {
		this.masters = masters;
	}

	/**
	 * Gets the range.
	 *
	 * @return the range
	 */
	public long[] getRange() {
		return range;
	}
	
	/**
	 * Gets the range high end.
	 *
	 * @return the range high end
	 */
	public long getRangeHighEnd() {
		return range[1];
	}
	
	/**
	 * Gets the range low end.
	 *
	 * @return the range low end
	 */
	public long getRangeLowEnd() {
		return range[0];
	}

	/**
	 * Gets the key count.
	 *
	 * @return the key count
	 */
	public long getKeyCount() {
		return keyCount;
	}

	/**
	 * Sets the key count.
	 *
	 * @param keyCount the new key count
	 */
	public void setKeyCount(long keyCount) {
		this.keyCount = keyCount;
	}
	
}
