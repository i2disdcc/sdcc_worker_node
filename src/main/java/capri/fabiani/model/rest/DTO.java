/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class DTO.
 */
public class DTO implements Serializable{

	
	/** The object mapper. */
	protected static ObjectMapper objectMapper = new ObjectMapper();
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The error. */
	@JsonInclude(Include.NON_NULL)
	protected String error = null;
	
	/**
	 * Instantiates a new dto.
	 */
	public DTO(){};
	
	/**
	 * Instantiates a new dto.
	 *
	 * @param error the error
	 */
	public DTO(String error){
		this.error=error;
	}
	
	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	
	/**
	 * Sets the error.
	 *
	 * @param error the new error
	 */
	public void setError(String error) {
		this.error = error;
	}
	
	/** The message. */
	@JsonInclude(Include.NON_NULL)
	protected String message;
	
	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		try {
			return objectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}
	
}
