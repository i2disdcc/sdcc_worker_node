/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import capri.fabiani.components.WorkerNode.PendingTask;
import capri.fabiani.components.WorkerNode.PendingTask.TaskType;

/**
 * The Class PendingTaskDTO.
 */
public class PendingTaskDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6158387578255127114L;
	
	/** The task id. */
	private int taskId;
	
	/** The type. */
	private TaskType type;
	
	/** The extras. */
	private Object extras;
	
	/**
	 * Instantiates a new pending task dto.
	 */
	public PendingTaskDTO() {}
	
	/**
	 * Instantiates a new pending task dto.
	 *
	 * @param taskId the task id
	 * @param type the type
	 * @param extras the extras
	 */
	public PendingTaskDTO(int taskId, TaskType type, Object extras) {
		this.setTaskId(taskId);
		this.setType(type);
		this.setExtras(extras);
	}
	
	/**
	 * Instantiates a new pending task dto.
	 *
	 * @param pt the pt
	 */
	public PendingTaskDTO(PendingTask pt) {
		this.setTaskId(pt.getTaskId());
		this.setType(pt.getType());
		this.setExtras(pt.getExtras());
	}
	
	/**
	 * Gets the task id.
	 *
	 * @return the task id
	 */
	public int getTaskId() {
		return taskId;
	}
	
	/**
	 * Sets the task id.
	 *
	 * @param taskId the new task id
	 */
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public TaskType getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(TaskType type) {
		this.type = type;
	}
	
	/**
	 * Gets the extras.
	 *
	 * @return the extras
	 */
	public Object getExtras() {
		return extras;
	}
	
	/**
	 * Sets the extras.
	 *
	 * @param extras the new extras
	 */
	public void setExtras(Object extras) {
		this.extras = extras;
	}		
}
