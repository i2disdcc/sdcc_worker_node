/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import capri.fabiani.controller.MasterPersistenceService.CommitMasterMsgType;
import capri.fabiani.controller.MasterPersistenceService.CommitOpType;

/**
 * The Class MasterCommitDTO.
 */
public class MasterCommitDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3769204444696365816L;
	
	/** The msg type. */
	private CommitMasterMsgType msgType;
	
	/** The datum. */
	private BusinessDTO datum = null;
	
	/** The op type. */
	private CommitOpType opType = null;
	
	/** The commit index. */
	private int commitIndex;
	
	/** The master key. */
	private long masterKey;
	
	/**
	 * Instantiates a new master commit dto.
	 */
	public MasterCommitDTO() {
		
	}

	/**
	 * Gets the msg type.
	 *
	 * @return the msg type
	 */
	public CommitMasterMsgType getMsgType() {
		return msgType;
	}

	/**
	 * Sets the msg type.
	 *
	 * @param msgType the new msg type
	 */
	public void setMsgType(CommitMasterMsgType msgType) {
		this.msgType = msgType;
	}

	/**
	 * Gets the datum.
	 *
	 * @return the datum
	 */
	public BusinessDTO getDatum() {
		return datum;
	}

	/**
	 * Sets the datum.
	 *
	 * @param datum the new datum
	 */
	public void setDatum(BusinessDTO datum) {
		this.datum = datum;
	}

	/**
	 * Gets the op type.
	 *
	 * @return the op type
	 */
	public CommitOpType getOpType() {
		return opType;
	}

	/**
	 * Sets the op type.
	 *
	 * @param opType the new op type
	 */
	public void setOpType(CommitOpType opType) {
		this.opType = opType;
	}

	/**
	 * Gets the commit index.
	 *
	 * @return the commit index
	 */
	public int getCommitIndex() {
		return commitIndex;
	}

	/**
	 * Sets the commit index.
	 *
	 * @param commitIndex the new commit index
	 */
	public void setCommitIndex(int commitIndex) {
		this.commitIndex = commitIndex;
	}

	/**
	 * Gets the master key.
	 *
	 * @return the master key
	 */
	public long getMasterKey() {
		return masterKey;
	}

	/**
	 * Sets the master key.
	 *
	 * @param masterKey the new master key
	 */
	public void setMasterKey(long masterKey) {
		this.masterKey = masterKey;
	}
	
}
