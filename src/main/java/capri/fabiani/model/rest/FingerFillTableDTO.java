/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import capri.fabiani.model.Identity;

/**
 * The Class FingerFillTableDTO.
 */
public class FingerFillTableDTO  extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6376516613882435658L;
	
	/** The request source. */
	private IdentityDTO requestSource;
	
	/** The task id. */
	private int taskId;
	
	/** The req key. */
	private long reqKey;
	
	/** The ft index. */
	private int ftIndex;
	
	/** The first send failed. */
	private boolean firstSendFailed = false;
	
	/**
	 * Instantiates a new finger fill table dto.
	 */
	public FingerFillTableDTO() {
		
	}
	
	/**
	 * Instantiates a new finger fill table dto.
	 *
	 * @param requestSource the request source
	 * @param taskId the task id
	 * @param reqKey the req key
	 * @param ftIndex the ft index
	 */
	public FingerFillTableDTO(IdentityDTO requestSource, int taskId, long reqKey, int ftIndex) {
		this.requestSource = requestSource;
		this.taskId = taskId;
		this.reqKey = reqKey;
		this.ftIndex = ftIndex;
	}
	
	/**
	 * Instantiates a new finger fill table dto.
	 *
	 * @param requestSource the request source
	 * @param taskId the task id
	 * @param reqKey the req key
	 * @param ftIndex the ft index
	 */
	public FingerFillTableDTO(Identity requestSource, int taskId, long reqKey, int ftIndex) {
		this.requestSource = new IdentityDTO(requestSource);
		this.taskId = taskId;
		this.reqKey = reqKey;
		this.ftIndex = ftIndex;
	}

	/**
	 * Gets the request source.
	 *
	 * @return the request source
	 */
	public IdentityDTO getRequestSource() {
		return requestSource;
	}

	/**
	 * Sets the request source.
	 *
	 * @param requestSource the new request source
	 */
	public void setRequestSource(IdentityDTO requestSource) {
		this.requestSource = requestSource;
	}

	/**
	 * Gets the task id.
	 *
	 * @return the task id
	 */
	public int getTaskId() {
		return taskId;
	}

	/**
	 * Sets the task id.
	 *
	 * @param taskId the new task id
	 */
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	/**
	 * Gets the req key.
	 *
	 * @return the req key
	 */
	public long getReqKey() {
		return reqKey;
	}

	/**
	 * Sets the req key.
	 *
	 * @param reqKey the new req key
	 */
	public void setReqKey(long reqKey) {
		this.reqKey = reqKey;
	}

	/**
	 * Gets the ft index.
	 *
	 * @return the ft index
	 */
	public int getFtIndex() {
		return ftIndex;
	}

	/**
	 * Sets the ft index.
	 *
	 * @param ftIndex the new ft index
	 */
	public void setFtIndex(int ftIndex) {
		this.ftIndex = ftIndex;
	}

	/**
	 * Checks if is first send failed.
	 *
	 * @return true, if is first send failed
	 */
	public boolean isFirstSendFailed() {
		return firstSendFailed;
	}

	/**
	 * Sets the first send failed.
	 *
	 * @param firstSendFailed the new first send failed
	 */
	public void setFirstSendFailed(boolean firstSendFailed) {
		this.firstSendFailed = firstSendFailed;
	}

}
