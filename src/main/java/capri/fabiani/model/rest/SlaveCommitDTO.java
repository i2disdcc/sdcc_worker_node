/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

import capri.fabiani.controller.SlavePersistenceService.CommitSlaveMsgType;

/**
 * The Class SlaveCommitDTO.
 */
public class SlaveCommitDTO extends DTO{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1729407945316581229L;
	
	/** The slave key. */
	private long slaveKey;
	
	/** The commit index. */
	private int commitIndex;
	
	/** The msg type. */
	private CommitSlaveMsgType msgType;
	
	/** The datum. */
	private BusinessDTO datum = null;
	
	/**
	 * Instantiates a new slave commit dto.
	 */
	public SlaveCommitDTO() {
		
	}
	
	/**
	 * Gets the commit index.
	 *
	 * @return the commit index
	 */
	public int getCommitIndex() {
		return this.commitIndex;
	}
	
	/**
	 * Sets the commit index.
	 *
	 * @param commitIndex the new commit index
	 */
	public void setCommitIndex(int commitIndex) {
		this.commitIndex = commitIndex;
	}

	/**
	 * Gets the msg type.
	 *
	 * @return the msg type
	 */
	public CommitSlaveMsgType getMsgType() {
		return msgType;
	}

	/**
	 * Sets the msg type.
	 *
	 * @param msgType the new msg type
	 */
	public void setMsgType(CommitSlaveMsgType msgType) {
		this.msgType = msgType;
	}

	/**
	 * Gets the datum.
	 *
	 * @return the datum
	 */
	public BusinessDTO getDatum() {
		return datum;
	}

	/**
	 * Sets the datum.
	 *
	 * @param datum the new datum
	 */
	public void setDatum(BusinessDTO datum) {
		this.datum = datum;
	}

	/**
	 * Gets the slave key.
	 *
	 * @return the slave key
	 */
	public long getSlaveKey() {
		return slaveKey;
	}

	/**
	 * Sets the slave key.
	 *
	 * @param slaveKey the new slave key
	 */
	public void setSlaveKey(long slaveKey) {
		this.slaveKey = slaveKey;
	}
}
