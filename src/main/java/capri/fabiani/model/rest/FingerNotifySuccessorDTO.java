/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class FingerNotifySuccessorDTO.
 */
public class FingerNotifySuccessorDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 537873334746188028L;
	
	/** The notifier id. */
	private IdentityDTO notifierId;
	
	/** The task id. */
	private int taskId;
	
	/**
	 * Instantiates a new finger notify successor dto.
	 */
	public FingerNotifySuccessorDTO() {}

	/**
	 * Gets the notifier id.
	 *
	 * @return the notifier id
	 */
	public IdentityDTO getNotifierId() {
		return notifierId;
	}

	/**
	 * Sets the notifier id.
	 *
	 * @param notifierId the new notifier id
	 */
	public void setNotifierId(IdentityDTO notifierId) {
		this.notifierId = notifierId;
	}

	/**
	 * Gets the task id.
	 *
	 * @return the task id
	 */
	public int getTaskId() {
		return taskId;
	}

	/**
	 * Sets the task id.
	 *
	 * @param taskId the new task id
	 */
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
}
