/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class SlaveInitPrimeDTO.
 */
public class SlaveInitPrimeDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3614873335220597764L;
	
	/** The num messages. */
	private int numMessages;
	
	/**
	 * Gets the num messages.
	 *
	 * @return the num messages
	 */
	public int getNumMessages() {
		return numMessages;
	}
	
	/**
	 * Sets the num messages.
	 *
	 * @param numMessages the new num messages
	 */
	public void setNumMessages(int numMessages) {
		this.numMessages = numMessages;
	}
	
}
