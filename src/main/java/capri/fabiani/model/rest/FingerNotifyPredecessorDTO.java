/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class FingerNotifyPredecessorDTO.
 */
public class FingerNotifyPredecessorDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 794895025746442206L;
	
	/** The notifier id. */
	private IdentityDTO notifierId;
	
	/**
	 * Instantiates a new finger notify predecessor dto.
	 */
	public FingerNotifyPredecessorDTO() {}

	/**
	 * Gets the notifier id.
	 *
	 * @return the notifier id
	 */
	public IdentityDTO getNotifierId() {
		return notifierId;
	}

	/**
	 * Sets the notifier id.
	 *
	 * @param notifierId the new notifier id
	 */
	public void setNotifierId(IdentityDTO notifierId) {
		this.notifierId = notifierId;
	}

}
