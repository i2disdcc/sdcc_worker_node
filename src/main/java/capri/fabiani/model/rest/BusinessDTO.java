/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class BusinessDTO.
 */
public class BusinessDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4016482324908031409L;
	
	/** The key. */
	private long key;
	
	/** The owner. */
	private String owner;
	
	/** The product class. */
	private long productClass;
	
	/** The request id. */
	private String requestId;
	
	/** The response url. */
	private String responseURL;
	
	/** The body. */
	private BasicDatumDTO body;
	
	/**
	 * Instantiates a new business dto.
	 */
	public BusinessDTO(){}
	
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public long getKey(){return key;};
	
	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(long key){this.key = key;};
	
	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public String getOwner(){return owner;};
	
	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(String owner){this.owner = owner;};
	
	/**
	 * Gets the product class.
	 *
	 * @return the product class
	 */
	public long getProductClass() {return productClass;}
	
	/**
	 * Sets the product class.
	 *
	 * @param productClass the new product class
	 */
	public void setProductClass(long productClass) {this.productClass=productClass;}
	
	/**
	 * Gets the request id.
	 *
	 * @return the request id
	 */
	public String getRequestId() {return requestId;}
	
	/**
	 * Sets the request id.
	 *
	 * @param requestId the new request id
	 */
	public void setRequestId(String requestId) {this.requestId = requestId;}

	/**
	 * Gets the response url.
	 *
	 * @return the response url
	 */
	public String getResponseURL() {return responseURL;}
	
	/**
	 * Sets the response url.
	 *
	 * @param responseURL the new response url
	 */
	public void setResponseURL(String responseURL) {this.responseURL = responseURL;}

	/**
	 * Gets the body.
	 *
	 * @return the body
	 */
	public BasicDatumDTO getBody() {return body;}
	
	/**
	 * Sets the body.
	 *
	 * @param body the new body
	 */
	public void setBody(BasicDatumDTO body) {this.body = body;}
	
}
