/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class RetirementVoteDTO.
 */
public class RetirementVoteDTO extends DTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2822630822508951650L;
	
	/** The key. */
	private long key;
	
	/** The address. */
	private String address;
	
	/** The granted. */
	private boolean granted;
	
	/**
	 * Instantiates a new retirement vote dto.
	 */
	public RetirementVoteDTO() {
		
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public long getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(long key) {
		this.key = key;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Checks if is granted.
	 *
	 * @return true, if is granted
	 */
	public boolean isGranted() {
		return granted;
	}

	/**
	 * Sets the granted.
	 *
	 * @param granted the new granted
	 */
	public void setGranted(boolean granted) {
		this.granted = granted;
	}
	
	

}
