/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class FingerResponseDTO.
 */
public class FingerResponseDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1334546052697403597L;
	
	/** The response identity. */
	private IdentityDTO responseIdentity;
	
	/** The extras. */
	private Object extras;
	
	/**
	 * Instantiates a new finger response dto.
	 */
	public FingerResponseDTO() {
		
	}
	
	/**
	 * Instantiates a new finger response dto.
	 *
	 * @param responseIdentity the response identity
	 * @param extras the extras
	 */
	public FingerResponseDTO(IdentityDTO responseIdentity, Object extras) {
		this.responseIdentity = responseIdentity;
		this.extras = extras;
	}

	/**
	 * Gets the response identity.
	 *
	 * @return the response identity
	 */
	public IdentityDTO getResponseIdentity() {
		return responseIdentity;
	}

	/**
	 * Sets the response identity.
	 *
	 * @param responseIdentity the new response identity
	 */
	public void setResponseIdentity(IdentityDTO responseIdentity) {
		this.responseIdentity = responseIdentity;
	}

	/**
	 * Gets the extras.
	 *
	 * @return the extras
	 */
	public Object getExtras() {
		return extras;
	}

	/**
	 * Sets the extras.
	 *
	 * @param extras the new extras
	 */
	public void setExtras(Object extras) {
		this.extras = extras;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	/**
	 * @return true if the IdentityDTOs returned by getResponseIdentity have the same key, false otherwise.
	 */
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(!(obj instanceof FingerResponseDTO))
			return false;
		
		return this.responseIdentity.getKey() == ((FingerResponseDTO)obj).getResponseIdentity().getKey();
	}

}
