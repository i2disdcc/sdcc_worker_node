/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class SlavePutInChargeDTO.
 */
public class SlavePutInChargeDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6642063629535309734L;
	
	/** The address. */
	private String address;
	
	/** The key. */
	private long key;
	
	/** The low end. */
	private long lowEnd;
	
	/**
	 * Instantiates a new slave put in charge dto.
	 */
	public SlavePutInChargeDTO() {}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public long getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(long key) {
		this.key = key;
	}

	/**
	 * Gets the low end.
	 *
	 * @return the low end
	 */
	public long getLowEnd() {
		return lowEnd;
	}

	/**
	 * Sets the low end.
	 *
	 * @param lowEnd the new low end
	 */
	public void setLowEnd(long lowEnd) {
		this.lowEnd = lowEnd;
	}	
	
}
