/*
 * @author Dan&Dan
 */
package capri.fabiani.model.rest;

/**
 * The Class FingerFindPredecessorDTO.
 */
public class FingerFindPredecessorDTO extends DTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6931244701980331713L;
	
	/** The request source. */
	private IdentityDTO requestSource;
	
	/** The task id. */
	private int taskId;
	
	/** The req key. */
	private long reqKey;
	
	/** The first send failed. */
	private boolean firstSendFailed;
	
	/**
	 * Instantiates a new finger find predecessor dto.
	 */
	public FingerFindPredecessorDTO() {
		
	}
	
	/**
	 * Instantiates a new finger find predecessor dto.
	 *
	 * @param requestSource the request source
	 * @param taskId the task id
	 * @param reqKey the req key
	 */
	public FingerFindPredecessorDTO(IdentityDTO requestSource, int taskId, long reqKey) {
		this.requestSource = requestSource;
		this.taskId = taskId;
		this.reqKey = reqKey;
	}

	/**
	 * Gets the request source.
	 *
	 * @return the request source
	 */
	public IdentityDTO getRequestSource() {
		return requestSource;
	}

	/**
	 * Sets the request source.
	 *
	 * @param requestSource the new request source
	 */
	public void setRequestSource(IdentityDTO requestSource) {
		this.requestSource = requestSource;
	}

	/**
	 * Gets the task id.
	 *
	 * @return the task id
	 */
	public int getTaskId() {
		return taskId;
	}

	/**
	 * Sets the task id.
	 *
	 * @param taskId the new task id
	 */
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	/**
	 * Gets the req key.
	 *
	 * @return the req key
	 */
	public long getReqKey() {
		return reqKey;
	}

	/**
	 * Sets the req key.
	 *
	 * @param reqKey the new req key
	 */
	public void setReqKey(long reqKey) {
		this.reqKey = reqKey;
	}

	/**
	 * Checks if is first send failed.
	 *
	 * @return true, if is first send failed
	 */
	public boolean isFirstSendFailed() {
		return firstSendFailed;
	}

	/**
	 * Sets the first send failed.
	 *
	 * @param firstSendFailed the new first send failed
	 */
	public void setFirstSendFailed(boolean firstSendFailed) {
		this.firstSendFailed = firstSendFailed;
	}
}
