/*
 * @author Dan&Dan
 */
package capri.fabiani.model;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import capri.fabiani.configuration.Constants;
import capri.fabiani.model.rest.BusinessDTO;

/**
 * The Class MasterCommitMgtDatum.
 */
public class MasterCommitMgtDatum {
	
	/**
	 * The Enum MasterState.
	 */
	public enum MasterState {
/** The init. */
INIT, 
 /** The wait. */
 WAIT, 
 /** The abort. */
 ABORT, 
 /** The precommit. */
 PRECOMMIT, 
 /** The commit. */
 COMMIT};
	
	/** The commit index. */
	private int commitIndex;
	
	/** The last heard. */
	private long lastHeard;
	
	/** The state. */
	private MasterState state;

	/** The staged datum. */
	private BusinessDTO stagedDatum;
	
	/** The mgt lock. */
	private Lock mgtLock;
	
	/** The mgt condition. */
	private Condition mgtCondition;
	
	/** The short count. */
	private int shortCount;
	
	/** The aborted. */
	private boolean aborted;
	
	/** The checked slaves. */
	private Set<Long> checkedSlaves;
	
	/** The initial slaves. */
	private Set<Long> initialSlaves;
	
	/**
	 * Instantiates a new master commit mgt datum.
	 */
	public MasterCommitMgtDatum() {
		
		mgtLock = new ReentrantLock();
		mgtCondition = mgtLock.newCondition();
		stagedDatum = null;
		lastHeard = 0;
		aborted = false;
		shortCount = Constants.REDUNDANCY_LEVEL;
		checkedSlaves = new HashSet<Long>();
		initialSlaves = new HashSet<Long>();
	}

	/**
	 * Gets the commit index.
	 *
	 * @return the commit index
	 */
	public int getCommitIndex() {
		return commitIndex;
	}

	/**
	 * Sets the commit index.
	 *
	 * @param commitIndex the new commit index
	 */
	public void setCommitIndex(int commitIndex) {
		this.commitIndex = commitIndex;
	}

	/**
	 * Gets the last heard.
	 *
	 * @return the last heard
	 */
	public long getLastHeard() {
		return lastHeard;
	}

	/**
	 * Sets the last heard.
	 *
	 * @param lastHeard the new last heard
	 */
	public void setLastHeard(long lastHeard) {
		this.lastHeard = lastHeard;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public MasterState getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(MasterState state) {
		this.state = state;
	}

	/**
	 * Gets the short count.
	 *
	 * @return the short count
	 */
	public int getShortCount() {
		return shortCount;
	}

	/**
	 * Sets the short count.
	 *
	 * @param shortCount the new short count
	 */
	public void setShortCount(int shortCount) {
		this.shortCount = shortCount;
	}
	
	/**
	 * Mgt lock.
	 */
	public void mgtLock() {
		mgtLock.lock();
	}
	
	/**
	 * Mgt unlock.
	 */
	public void mgtUnlock() {
		mgtLock.unlock();
	}
	
	/**
	 * Mgt condition timed wait.
	 *
	 * @param millis the millis
	 * @return true, if successful
	 * @throws InterruptedException the interrupted exception
	 */
	public boolean mgtConditionTimedWait(long millis) throws InterruptedException {
		return mgtCondition.await(millis, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Mgt condition signal.
	 */
	public void mgtConditionSignal() {
		mgtCondition.signal();
	}
	
	/**
	 * Mgt condition signal all.
	 */
	public void mgtConditionSignalAll() {
		mgtCondition.signalAll();
	}

	/**
	 * Gets the staged datum.
	 *
	 * @return the staged datum
	 */
	public BusinessDTO getStagedDatum() {
		return stagedDatum;
	}

	/**
	 * Sets the staged datum.
	 *
	 * @param stagedDatum the new staged datum
	 */
	public void setStagedDatum(BusinessDTO stagedDatum) {
		this.stagedDatum = stagedDatum;
	}

	/**
	 * Checks if is aborted.
	 *
	 * @return true, if is aborted
	 */
	public boolean isAborted() {
		return aborted;
	}

	/**
	 * Sets the aborted.
	 *
	 * @param aborted the new aborted
	 */
	public void setAborted(boolean aborted) {
		this.aborted = aborted;
	}

	/**
	 * Gets the checked slaves.
	 *
	 * @return the checked slaves
	 */
	public Set<Long> getCheckedSlaves() {
		return checkedSlaves;
	}
	
	/**
	 * Adds the initial slave.
	 *
	 * @param key the key
	 */
	public void addInitialSlave(long key) {
		initialSlaves.add(key);
	}
	
	/**
	 * Contains initial slave.
	 *
	 * @param key the key
	 * @return true, if successful
	 */
	public boolean containsInitialSlave(long key) {
		return initialSlaves.contains(key);
	}

}
