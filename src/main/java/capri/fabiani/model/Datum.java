/*
 * @author Dan&Dan
 */
package capri.fabiani.model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import capri.fabiani.configuration.Constants;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The Class Datum.
 */
@Document
public class Datum {
	
	/** The key. */
	@Id
	private long key;
	
	/** The owner. */
	@Field("owner")
	private String owner;
	
	/** The last modified. */
	@Temporal(TemporalType.DATE)
	@LastModifiedDate
	private Date lastModified;
	
	/** The created date. */
	@Temporal(TemporalType.DATE)
	@CreatedDate
	private Date createdDate;
	
	/** The product class. */
	@Field("product_class")
	private long productClass;
	
	/** The data. */
	@Field("body")
	private BasicDatum body;
	
	/**
	 * Instantiates a new datum.
	 */
	public Datum() {};
	
	/**
	 * Instantiates a new datum.
	 *
	 * @param key the key
	 * @param owner the owner
	 */
	public Datum(long key, String owner) {
		this.key = key;
		this.owner = owner;
		
	}
	
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public long getKey() {return key;}
	
	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(long key) {this.key=key;}
	
	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public String getOwner() {return owner;}
	
	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(String owner) {this.owner = owner;}
	
	/**
	 * Gets the last modified.
	 *
	 * @return the last modified
	 */
	public Date getLastModified() {return lastModified;}
	
	/**
	 * Sets the last modified.
	 *
	 * @param lastModified the new last modified
	 */
	public void setLastModified(Date lastModified) {this.lastModified=lastModified;}
	
	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate() {return createdDate;}
	
	/**
	 * Sets the created date.
	 *
	 * @param createdDate the new created date
	 */
	public void setCreatedDate(Date createdDate) {this.createdDate=createdDate;}
	
	/**
	 * Gets the product class.
	 *
	 * @return the product class
	 */
	public long getProductClass() {return productClass;}
	
	/**
	 * Sets the product class.
	 *
	 * @param productClass the new product class
	 */
	public void setProductClass(long productClass) {this.productClass=productClass;}
	
	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public BasicDatum getBody() {return this.body;}
	
	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setBody(BasicDatum body) {this.body = body;}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "key="+key+", owner="+owner+", creationDate="+createdDate+", lastModified="+lastModified + ", description="+
				(body == null ? Constants.EMPTY_DESCRIPTION : body.toString());
	}
	
}
