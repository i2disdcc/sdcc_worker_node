/*
 * @author Dan&Dan
 */
package capri.fabiani.model;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.google.common.base.Throwables;

import capri.fabiani.util.Logger;

/**
 * The Class Identity.
 */
public class Identity implements Comparable<Identity> {
	
	/** The address. */
	protected String address;
	
	/** The key. */
	protected long key;
	
	/**
	 * Instantiates a new identity.
	 */
	public Identity() {	
		this.key=-1;
		try {
			this.address=InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			this.address=InetAddress.getLoopbackAddress().getHostAddress();
			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}
	
	/**
	 * Instantiates a new identity.
	 *
	 * @param address the address
	 * @param key the key
	 */
	public Identity(String address, long key) {
		this.address = address;
		this.key = key;
	}
	
	/**
	 * Instantiates a new identity.
	 *
	 * @param key the key
	 */
	public Identity(long key) {
		this.setKey(key);
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public long getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(long key) {
		this.key = key;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "{\"key\" : \""+key+"\", \"address\" : \""+address+"\"}";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Identity arg0) {
		
		if(this.key < arg0.getKey())
			return -1;
		
		if(this.key > arg0.getKey())
			return 1;
		
		return 0;
	}
	
	/**
	 * Returns true IFF arg0 is a non-null instance of Identity, and arg0.getKey() returns the same value as this.getKey().
	 * Does not account for different addresses.
	 *
	 * @param arg0 the arg0
	 * @return true, if successful
	 */
	@Override 
	public boolean equals(Object arg0) {
		if (arg0 == null) 
			return false;
		if (arg0 == this) 
			return true;
		if (!(arg0 instanceof Identity))return false;
		
		return this.key == ((Identity)arg0).getKey();
	}
	
}