/*
 * @author Dan&Dan
 */
package capri.fabiani.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import capri.fabiani.model.rest.BasicDatumDTO;



/**
 * Basic inner Datum field implementation. 
 *
 */
@Document
public class BasicDatum {
	
	/** The description. */
	@Field("desc")
	private String description;
	
	/**
	 * Instantiates a new basic datum.
	 */
	public BasicDatum() {}
	
	/**
	 * Instantiates a new basic datum.
	 *
	 * @param description the description
	 */
	public BasicDatum(String description){
		this.description = description;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription(){return this.description;}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {this.description = description;}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return description;
	}
	
	/**
	 * Pack.
	 *
	 * @return the basic datum dto
	 */
	public BasicDatumDTO pack() {
		BasicDatumDTO dto = new BasicDatumDTO();
		dto.setDescription(description);
		return dto;
	}
}
