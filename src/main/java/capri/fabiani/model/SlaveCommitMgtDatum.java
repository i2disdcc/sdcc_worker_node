/*
 * @author Dan&Dan
 */
package capri.fabiani.model;

import capri.fabiani.controller.MasterPersistenceService;
import capri.fabiani.model.rest.BusinessDTO;

/**
 * The Class SlaveCommitMgtDatum.
 */
public class SlaveCommitMgtDatum {
	
	/**
	 * The Enum SlaveState.
	 */
	public enum SlaveState {
/** The init. */
INIT, 
 /** The ready. */
 READY, 
 /** The abort. */
 ABORT, 
 /** The precommit. */
 PRECOMMIT, 
 /** The commit. */
 COMMIT};
	
	/** The commit index. */
	private int commitIndex;
	
	/** The last heard. */
	private long lastHeard;
	
	/** The state. */
	private SlaveState state;
	
	/** The business data. */
	private BusinessDTO businessData;
	
	/** The commit op type. */
	private MasterPersistenceService.CommitOpType commitOpType;
	
	/**
	 * Instantiates a new slave commit mgt datum.
	 *
	 * @param commitOpType the commit op type
	 * @param commitIndex the commit index
	 */
	public SlaveCommitMgtDatum(MasterPersistenceService.CommitOpType commitOpType, int commitIndex) {
		this.commitOpType = commitOpType;
		this.commitIndex = commitIndex;
	}
	
	/**
	 * Gets the commit index.
	 *
	 * @return the commit index
	 */
	public int getCommitIndex() {
		return commitIndex;
	}
	
	/**
	 * Gets the last heard.
	 *
	 * @return the last heard
	 */
	public long getLastHeard() {
		return lastHeard;
	}
	
	/**
	 * Sets the last heard.
	 *
	 * @param lastHeard the new last heard
	 */
	public void setLastHeard(long lastHeard) {
		this.lastHeard = lastHeard;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public SlaveState getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(SlaveState state) {
		this.state = state;
	}

	/**
	 * Gets the business data.
	 *
	 * @return the business data
	 */
	public BusinessDTO getBusinessData() {
		return businessData;
	}

	/**
	 * Sets the business data.
	 *
	 * @param businessData the new business data
	 */
	public void setBusinessData(BusinessDTO businessData) {
		this.businessData = businessData;
	}
	
	/**
	 * Gets the commit op type.
	 *
	 * @return the commit op type
	 */
	public MasterPersistenceService.CommitOpType getCommitOpType() {
		return commitOpType;
	}
}
