/*
 * @author Dan&Dan
 */
package capri.fabiani.model;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * The Class RedundancyIdentity.
 */
public class RedundancyIdentity extends Identity {
	
	/** The ready. */
	protected boolean ready = false;
	
	/** The ready lock. */
	protected ReadWriteLock readyLock;
	
	/**
	 * Instantiates a new redundancy identity.
	 *
	 * @param address the address
	 * @param key the key
	 * @param ready the ready
	 */
	public RedundancyIdentity(String address, long key, boolean ready) {
		this.setAddress(address);
		this.setKey(key);
		this.setReady(ready);
		readyLock = new ReentrantReadWriteLock(true);
	}
	
	/**
	 * Checks if is ready.
	 *
	 * @return true, if is ready
	 */
	public boolean isReady() {
		return this.ready;
	}
	
	/**
	 * Sets the ready.
	 *
	 * @param ready the new ready
	 */
	public void setReady(boolean ready) {
		this.ready = ready;
	}
	
	/**
	 * Ready rd lock.
	 */
	public void readyRdLock() {
		readyLock.readLock().lock();
	}
	
	/**
	 * Ready rd unlock.
	 */
	public void readyRdUnlock() {
		readyLock.readLock().unlock();
	}
	
	/**
	 * Ready wr lock.
	 */
	public void readyWrLock() {
		readyLock.writeLock().lock();
	}
	
	/**
	 * Ready wr unlock.
	 */
	public void readyWrUnlock() {
		readyLock.writeLock().unlock();
	}
}
