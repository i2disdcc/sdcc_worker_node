/*
 * @author Dan&Dan
 */
package capri.fabiani.model;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import capri.fabiani.model.Identity;

/**
 * The Class RedundancyManagementData.
 */
public class RedundancyManagementData extends Identity {
	
	/** The low end. */
	long lowEnd;
	
	/** The dismiss lock. */
	private Lock dismissLock;
	
	/** The dismissing. */
	private boolean dismissing;
	
	/** The timestamp lock. */
	private Lock timestampLock;
	
	/** The timestamp. */
	private long timestamp;
	
	/** The pending commits lock. */
	private Lock pendingCommitsLock;
	
	/** The pending commits. */
	private Map<Integer, SlaveCommitMgtDatum> pendingCommits;
	
	/** The init lock. */
	private ReadWriteLock initLock;
	
	/** The init status. */
	private InitData initStatus;
	
	/**
	 * Instantiates a new redundancy management data.
	 */
	public RedundancyManagementData() {
		pendingCommits = new HashMap<Integer, SlaveCommitMgtDatum>();
		pendingCommitsLock = new ReentrantLock(true);
		
		dismissing = false;
		dismissLock = new ReentrantLock(true);
		
		initLock = new ReentrantReadWriteLock(true);
		initStatus = new InitData();
		
		timestampLock = new ReentrantLock(true);
	}
	
	/**
	 * Instantiates a new redundancy management data.
	 *
	 * @param boss the boss
	 * @param lowEnd the low end
	 */
	public RedundancyManagementData(Identity boss, long lowEnd) {
		this.address = boss.getAddress();
		this.key = boss.getKey();
		this.lowEnd = lowEnd;
		pendingCommits = new HashMap<Integer, SlaveCommitMgtDatum>();
		pendingCommitsLock = new ReentrantLock(true);
		
		dismissing = false;
		dismissLock = new ReentrantLock(true);
		
		initLock = new ReentrantReadWriteLock(true);
		initStatus = new InitData();
	}
	
	/**
	 * Instantiates a new redundancy management data.
	 * 
	 * @param address
	 * @param key
	 * @param lowEnd
	 */
	public RedundancyManagementData(String address, long key, long lowEnd) {
		this.address = address;
		this.key = key;
		this.lowEnd = lowEnd;
		pendingCommits = new HashMap<Integer, SlaveCommitMgtDatum>();
		pendingCommitsLock = new ReentrantLock(true);
		
		dismissing = false;
		dismissLock = new ReentrantLock(true);
		
		initLock = new ReentrantReadWriteLock(true);
		initStatus = new InitData();
	}
	
	/**
	 * Gets the low end.
	 *
	 * @return the low end
	 */
	public long getLowEnd() {return lowEnd;}
	
	/**
	 * Sets the low end.
	 *
	 * @param lowEnd the new low end
	 */
	public void setLowEnd(long lowEnd) {this.lowEnd = lowEnd;}
	
	/**
	 * Gets the pending commits.
	 *
	 * @return the pending commits
	 */
	public Map<Integer, SlaveCommitMgtDatum> getPendingCommits() {
		return this.pendingCommits;
	}
	
	/**
	 * Sets the dismissing.
	 *
	 * @param dismissing the new dismissing
	 */
	public void setDismissing(boolean dismissing) {this.dismissing = dismissing;}
	
	/**
	 * Checks if is dismissing.
	 *
	 * @return true, if is dismissing
	 */
	public boolean isDismissing() {return this.dismissing;}
	
	/**
	 * Dismiss lock.
	 */
	public void dismissLock() {dismissLock.lock();}
	
	/**
	 * Dismiss unlock.
	 */
	public void dismissUnlock() {dismissLock.unlock();}
	
	/**
	 * Pending commits lock.
	 */
	public void pendingCommitsLock() {
		pendingCommitsLock.lock();
	}
	
	/**
	 * Pending commits unlock.
	 */
	public void pendingCommitsUnlock() {
		pendingCommitsLock.unlock();
	}
	
	/**
	 * Prepares for initialization.
	 *
	 * @param numMessages the num messages
	 */
	public void initPrime(int numMessages) {
		if(initStatus == null || initStatus.getNumMessages() != -1)
			return;
		
		if(numMessages == 0) {
			initStatus = null;
		}
		else {
			initStatus.setNumMessages(numMessages);
		}
	}
	
	/**
	 * Inits the reboot.
	 */
	public void initReboot() {
		initStatus = new InitData();
	}
	
	/**
	 * Inits the advance.
	 *
	 * @param msgIndex the msg index
	 */
	public void initAdvance(int msgIndex) {
		if(initStatus == null)
			return;
		
		if (initStatus.getNextMessageIndex() == msgIndex) {
			initStatus.incMessageIndex();
			if (initStatus.getNextMessageIndex() >= initStatus.getNumMessages())
				initStatus = null;
		}
	}
	
	/**
	 * Inits the rd lock.
	 */
	public void initRdLock() {
		initLock.readLock().lock();
	}
	
	/**
	 * Inits the rd unlock.
	 */
	public void initRdUnlock() {
		initLock.readLock().unlock();
	}
	
	/**
	 * Inits the wr lock.
	 */
	public void initWrLock() {
		initLock.writeLock().lock();
	}
	
	/**
	 * Inits the wr unlock.
	 */
	public void initWrUnlock() {
		initLock.writeLock().unlock();
	}
	
	/**
	 * Checks if is ready.
	 *
	 * @return true, if is ready
	 */
	public boolean isReady() {
		return initStatus == null;
	}
	
	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}
	
	/**
	 * Sets the timestamp.
	 *
	 * @param timestamp the new timestamp
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	/**
	 * Timestamp lock.
	 */
	public void timestampLock() {
		timestampLock.lock();
	}
	
	/**
	 * Timestamp unlock.
	 */
	public void timestampUnlock() {
		timestampLock.unlock();
	}
	
	/**
	 * Gets the inits the next msg index.
	 *
	 * @return the inits the next msg index
	 */
	public int getInitNextMsgIndex() {
		if(initStatus != null)
			return initStatus.getNextMessageIndex();
		
		return -1;
	}

	/**
	 * The Class InitData.
	 */
	private class InitData {
		
		/** The next message index. */
		private int nextMessageIndex;
		
		/** The num messages. */
		private int numMessages;
		
		/**
		 * Instantiates a new inits the data.
		 */
		public InitData() {
			this.nextMessageIndex = 0;
			this.numMessages = -1;
		}

		/**
		 * Gets the next message index.
		 *
		 * @return the next message index
		 */
		public int getNextMessageIndex() {
			return nextMessageIndex;
		}

		/**
		 * Inc message index.
		 */
		public void incMessageIndex() {
			this.nextMessageIndex++;
		}

		/**
		 * Gets the num messages.
		 *
		 * @return the num messages
		 */
		public int getNumMessages() {
			return numMessages;
		}

		/**
		 * Sets the num messages.
		 *
		 * @param numMessages the new num messages
		 */
		public void setNumMessages(int numMessages) {
			this.numMessages = numMessages;
		}
		
		
	}
	
}
