/*
 * @author Dan&Dan
 */
package capri.fabiani.configuration;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

/**
 * The Class Constants.
 */
public class Constants {

	/** The Constant VERBOSE. */
	public final static boolean VERBOSE = true;

	/** The Constant REDUNDANCY_LEVEL. */
	public final static int REDUNDANCY_LEVEL = 3;

	/** The Constant MAX_VALUE. */
	public final static long MAX_VALUE = Long.MAX_VALUE;

	/** The Constant MIN_VALUE. */
	public final static long MIN_VALUE = 0;

	/** The Constant MODULUS. */
	public final static long MODULUS = MAX_VALUE + 1;

	/** The Constant LOOPBACK_ADDRESS. */
	public final static String LOOPBACK_ADDRESS = "127.0.0.1";

	/** The Constant F_TABLE_SIZE. */
	public final static int F_TABLE_SIZE = 63;

	/** The Constant SCHEDULED_THREAD_POOL. */
	public final static int SCHEDULED_THREAD_POOL = 20;

	/** The Constant ASYNC_THREAD_POOL. */
	public final static int ASYNC_THREAD_POOL = 30;

	/** The Constant ASYNC_THREAD_PREFIX. */
	public final static String ASYNC_THREAD_PREFIX = "Async-";

	/** The Constant OK. */
	public final static String OK = "ok";

	/** The Constant HTTP_PRIMER. */
	public final static String HTTP_PRIMER = "http://";

	/** The Constant APP_NAME. */
	private final static String APP_NAME = "workernode";

	/** The Constant APP_NAME_PATH. */
	public final static String APP_NAME_PATH = "/" + APP_NAME;

	/** The Constant BUSINESS_DELETE_PRODUCT. */
	public final static String BUSINESS_DELETE_PRODUCT = "/deleteProduct/";

	/** The Constant BUSINESS_GET_PRODUCT. */
	public final static String BUSINESS_GET_PRODUCT = "/getProduct/";

	/** The Constant BUSINESS_REGISTER_PRODUCT. */
	public final static String BUSINESS_REGISTER_PRODUCT = "/registerProduct/";

	/** The Constant BUSINESS_UPDATE_PRODUCT. */
	public final static String BUSINESS_UPDATE_PRODUCT = "/updateProduct/";

	/** The Constant EMPTY_DESCRIPTION. */
	public final static String EMPTY_DESCRIPTION = "NODESC";

	/** The Constant ERROR_NOT_FOUND_GET. */
	public final static String ERROR_NOT_FOUND_GET = "Requested key not found.";
	
	/** The Constant ERROR_MASTER_GET. */
	public final static String ERROR_MASTER_GET = "Could not retreive the requested data, please try again later.";

	/** The Constant ERROR_MASTER_COMMIT. */
	public final static String ERROR_MASTER_COMMIT = "Could not perform the requested operation. Please try again later.";

	/** The Constant ERROR_SLAVE_INVALID_CREDENTIALS. */
	public final static String ERROR_SLAVE_INVALID_CREDENTIALS = "Trying to access a non-owned resource.";

	/** The Constant ERROR_SLAVE_INVALID_RANGE. */
	public final static String ERROR_SLAVE_INVALID_RANGE = "Service temporary unavailable";

	/** The Constant FINGER_FILL_TABLE. */
	public final static String FINGER_FILL_TABLE = "/fingerFillTable/";

	/** The Constant FINGER_FIND_PREDECESSOR. */
	public final static String FINGER_FIND_PREDECESSOR = "fingerFindPred";

	/** The Constant FINGER_NEXT_NODES. */
	public final static String FINGER_NEXT_NODES = "/fingerNextNodes/";

	/** The Constant FINGER_NOTIFY_PREDECESSOR. */
	public final static String FINGER_NOTIFY_PREDECESSOR = "/fingerNotifyPred/";

	/** The Constant FINGER_NOTIFY_SUCCESSOR. */
	public final static String FINGER_NOTIFY_SUCCESSOR = "/fingerNotifySucc/";

	/** The Constant FINGER_RESPONSE. */
	// public final static String FINGER_PING = "/fingerPing/";
	public final static String FINGER_RESPONSE = "/fingerResponse/";

	/** The Constant COMMIT_SLAVE. */
	public final static String COMMIT_SLAVE = "/commitSlave/";

	/** The Constant COMMIT_MASTER. */
	public final static String COMMIT_MASTER = "/commitMaster/";

	/** The Constant LOAD_RETIREMENT_TIMEOUT_MIN. */
	public final static long LOAD_RETIREMENT_TIMEOUT_MIN = 10000;

	/** The Constant LOAD_RETIREMENT_TIREMOUT_MAX. */
	public final static long LOAD_RETIREMENT_TIREMOUT_MAX = 120000;

	/** The Constant RETIREMENT_ACK. */
	public final static String RETIREMENT_ACK = "/retirementAck/";

	/** The Constant RETIREMENT_REQUEST. */
	public final static String RETIREMENT_REQUEST = "/retirementRequest/";

	/** The Constant RETIREMENT_VOTE. */
	public final static String RETIREMENT_VOTE = "/retirementVote/";

	/** The Constant RETIREMENT_QUORUM. */
	public final static int RETIREMENT_QUORUM = REDUNDANCY_LEVEL;

	/** The Constant RETIREMENT_VOTE_TIMEOUT. */
	public final static long RETIREMENT_VOTE_TIMEOUT = 60000; // 1 min

	/** The Constant RETIREMENT_SWEEP_INITIAL_DELAY. */
	public final static long RETIREMENT_SWEEP_INITIAL_DELAY = 30000; // 30 s

	/** The Constant RETIREMENT_SWEEP_DELAY. */
	public final static long RETIREMENT_SWEEP_DELAY = 60000; // 1 min

	/** The Constant SLAVE_DISMISS. */
	public final static String SLAVE_DISMISS = "/slaveDismiss/";

	/** The Constant SLAVE_GET. */
	public final static String SLAVE_GET = "/slaveGet/";

	/** The Constant SLAVE_PUT_IN_CHARGE. */
	public final static String SLAVE_PUT_IN_CHARGE = "/slavePutInCharge/";

	/** The Constant SLAVE_TRANSFER_INIT. */
	public final static String SLAVE_TRANSFER_INIT = "/slaveTransferInit/";

	/** The Constant SLAVE_TRANSFER_KEYS. */
	public final static String SLAVE_TRANSFER_KEYS = "/slaveTransferKeys/";

	/** The Constant STATUS_GET. */
	public final static String STATUS_GET = "status";
	
	/** The Constant STATUS_EXT_GET */
	public final static String STATUS_EXT_GET = "statusExt";

	/** The Constant INITIALIZATION_PATH. */
	public static final String INITIALIZATION_PATH = "/initializzation";

	/** The Constant INITIALIZATION_CONTROLLER. */
	public static final String INITIALIZATION_CONTROLLER = INITIALIZATION_PATH + "/";

	/** The Constant HELLO. */
	public static final String HELLO = "/getInitialINfo";

	/** The Constant RECEIVE_KEYS. */
	public static final String RECEIVE_KEYS = "/getInitialKeys";

	/** The Constant FILL_TABLE_MAPPING. */
	public static final String FILL_TABLE_MAPPING = "/FILL_TABLE_MAPPING";

	/** The Constant NOT_FOUND_MSG. */
	public final static String NOT_FOUND_MSG = "No such pending finger table task";

	/** The Constant MALFORMED_PACKET_MSG. */
	public final static String MALFORMED_PACKET_MSG = "Malformed packet";

	/** The Constant INTERNAL_SERVER_ERROR_MSG. */
	public final static String INTERNAL_SERVER_ERROR_MSG = "Internal server error";

	/** The Constant READ_TIMEOUT. */
	public static final int READ_TIMEOUT = 10000;

	/** The Constant CONNECTION_TIMEOUT. */
	public static final int CONNECTION_TIMEOUT = 10000;

	/** The Constant FT_REFRESH_TIMER. */
	public static final long FT_REFRESH_TIMER = 150000; // 2.5 min

	/** The Constant NEXT_NODES_CANDIDATES_REFRESH_TIMER. */
	public static final long NEXT_NODES_CANDIDATES_REFRESH_TIMER = 150000; // 2.5

	/** The Constant COMMIT_TIMEOUT. */
	// min
	public final static long COMMIT_TIMEOUT = 60000; // 1 min

	/** The Constant INITIAL_FT_REFRESH_DELAY. */
	public static final long INITIAL_FT_REFRESH_DELAY = 10000; // 10 seconds

	/** The Constant FT_REFRESH_DELAY. */
	public static final long FT_REFRESH_DELAY = 300000; // 5 min

	/** The Constant INITIAL_REDUNDANCY_NODES_REFRESH_DELAY. */
	public static final long INITIAL_REDUNDANCY_NODES_REFRESH_DELAY = 120000; // 2min

	/** The Constant REDUNDANCY_NODES_REFRESH_DELAY. */
	public static final long REDUNDANCY_NODES_REFRESH_DELAY = 240000; // 4 min

	/** The Constant REDUNDANCY_LEASE_RENEW_INITIAL_DELAY. */
	public static final long REDUNDANCY_LEASE_RENEW_INITIAL_DELAY = 18000; // 3
																			// min

	/** The Constant REDUNDANCY_LEASE_RENEW_DELAY. */
	public static final long REDUNDANCY_LEASE_RENEW_DELAY = 300000; // 5 min

	/** The Constant CONFIGURATION_TIMEOUT_INITIAL_DELAY. */
	public static final long CONFIGURATION_TIMEOUT_INITIAL_DELAY = 30000;

	/** The Constant LOAD_CHECK_INITIAL_DELAY. */
	public static final long LOAD_CHECK_INITIAL_DELAY = 30000;

	/** The Constant NUMBER_OF_MAX_DATUM_TO_SEND. */
	public static final int NUMBER_OF_MAX_DATUM_TO_SEND = 10;

	/** The Constant MASTER_TIMEOUT. */
	public static final long MASTER_TIMEOUT = REDUNDANCY_NODES_REFRESH_DELAY * 2; // 4*2
																					// min

	/** The Constant INITIAL_SWEEP_REDUNDANCY_DATA_DELAY. */
	public static final long INITIAL_SWEEP_REDUNDANCY_DATA_DELAY = 240000; // 4

	/** The Constant SWEEP_REDUNDANCY_DATA_DELAY. */
	// min
	public static final long SWEEP_REDUNDANCY_DATA_DELAY = 300000; // 5 min

	/** The Constant INITIAL_DISMISS_OLD_REPLICAS_DELAY. */
	public static final long INITIAL_DISMISS_OLD_REPLICAS_DELAY = 270000; // 4:30

	/** The Constant DISMISS_OLD_REPLICAS_DELAY. */
	// min
	public static final long DISMISS_OLD_REPLICAS_DELAY = 300000; // 5 min

	/** The Constant CREDENTIAL_PATH. */
	public final static String CREDENTIAL_PATH = "/file/credential.sdcc";

	/** The Constant FINGER_PING. */
	public static final String FINGER_PING = "/ping";

	/** The Constant TRANSPORT_NOT_INITIALIZED. */
	public static final String TRANSPORT_NOT_INITIALIZED = "Keys transport not initialized";

	/** The Constant TRANSPORT_MESSAGE_NOT_INITIALIZED. */
	public static final String TRANSPORT_MESSAGE_NOT_INITIALIZED = "It seems that sender didn't initialized the connection.";

	/** The Constant TRANSPORT_ABORT. */
	public static final String TRANSPORT_ABORT = "Keys transport abort";

	/** The Constant TRANSPORT_ABORT_MESSAGE. */
	public static final String TRANSPORT_ABORT_MESSAGE = "Wrong message index, the receiver aborted the trasmission";

	/** The Constant TRANSPORT_SUCCESS. */
	public static final String TRANSPORT_SUCCESS = "Keys trunk succeful transferred";

	/** The Constant HELLO_SUCCEFULL. */
	public static final String HELLO_SUCCEFULL = "Keys transfer succeful initialized";

	/** The Constant TRANSPORT_FINISHED. */
	public static final String TRANSPORT_FINISHED = "All keys transferred";

	/** The Constant SEND_KEYS_TIMEOUT. */
	public static final long SEND_KEYS_TIMEOUT = 80000;

	/** The Constant SEND_KEYS_DELAY_TIMEOUT. */
	public static final long SEND_KEYS_DELAY_TIMEOUT = 40000;

	/** The Constant RETRIES. */
	public static final int RETRIES = 5;

	/** The Constant MAX_LOAD_KEYS. */
	public static final long MAX_LOAD_KEYS = 100;

	/** The Constant MIN_LOAD_KEYS. */
	public static final long MIN_LOAD_KEYS = 20;

	/** The Constant IMAGE_AMI. */
	public static final String IMAGE_AMI = "Worker node-"; // "ami-cad370aa";

	/** The Constant OWNER_ID. */
	public static final String OWNER_ID = "678962425877";

	/** The Constant EC2_ENDPOINT. */
	public static final String EC2_ENDPOINT = "ec2.us-west-2.amazonaws.com";

	/** The Constant VM_POLLING_TIMER. */
	public static final long VM_POLLING_TIMER = 30000;

	/** The Constant VM_POLLING_RETRIES. */
	public static final int VM_POLLING_RETRIES = 10;

	/** The Constant HEALTH_CHECK. */
	public static final String HEALTH_CHECK = "healthCheck";

	/** Amazon Resource Name of the target group for the load balancing policies*/
	public static final String ARN ="arn:aws:elasticloadbalancing:us-west-2:678962425877:targetgroup/WorkerNode/58d9dc287969f709";

	public static final Region AWS_REGION = Region.getRegion(Regions.US_WEST_2);

}
