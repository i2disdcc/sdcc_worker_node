/*
 * @author Dan&Dan
 */
package capri.fabiani.configuration;

import java.util.concurrent.Executor;

import java.util.concurrent.Executors;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;


/**
 * The Class Config.
 */
@Configuration
@EnableMongoRepositories({ "capri.fabiani.repositories" })
@EnableAutoConfiguration(exclude = {
        org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class,
        /*org.springframework.boot.actuate.autoconfigure.ManagementWebSecurityAutoConfiguration.class*/})
@EnableScheduling
@EnableAsync
class Config implements SchedulingConfigurer, AsyncConfigurer {
	
	/* (non-Javadoc)
	 * @see org.springframework.scheduling.annotation.SchedulingConfigurer#configureTasks(org.springframework.scheduling.config.ScheduledTaskRegistrar)
	 */
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(scheduledTaskExecutor());
		
	}

	/**
	 * Scheduled task executor.
	 *
	 * @return the executor
	 */
	@Bean(destroyMethod = "shutdown")
	public Executor scheduledTaskExecutor() {		
		// http://stackoverflow.com/questions/16333244/reinitialize-fix-delay-in-scheduledexecutorservice
		return Executors.newScheduledThreadPool(Constants.SCHEDULED_THREAD_POOL);
	}

	/* (non-Javadoc)
	 * @see org.springframework.scheduling.annotation.AsyncConfigurer#getAsyncExecutor()
	 */
	@Override
	@Bean(destroyMethod = "shutdown")
	public Executor getAsyncExecutor() {
		 ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
         executor.setCorePoolSize(Constants.ASYNC_THREAD_POOL);
//       executor.setMaxPoolSize(Integer.MAX_VALUE);						//Default max pool size
//       executor.setQueueCapacity(Integer.MAX_VALUE);						//Default queue capacity
         executor.setThreadNamePrefix(Constants.ASYNC_THREAD_PREFIX);
//       executor.initialize();												//No need to, since this method hasthe @Bean annotation
         return executor;

	}

	/* (non-Javadoc)
	 * @see org.springframework.scheduling.annotation.AsyncConfigurer#getAsyncUncaughtExceptionHandler()
	 */
	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		// TODO Auto-generated method stub
		return null;
	}


}