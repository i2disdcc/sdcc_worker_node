/*
 * @author Dan&Dan
 */
package capri.fabiani.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * The Class HttpEntityConfiguration.
 */
public class HttpEntityConfiguration {
	
	/** The template. */
	private static RestTemplate template = null;
	
	/**
	 * Gets the template.
	 *
	 * @return the template
	 */
	@Bean
	public static RestTemplate getTemplate() {
		if(template == null)
			template = new RestTemplate(clientHttpRequestFactory());
		return template;
	}
	
	/**
	 * Client http request factory.
	 *
	 * @return the client http request factory
	 */
	private static ClientHttpRequestFactory clientHttpRequestFactory() {
	    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
	    factory.setReadTimeout(Constants.READ_TIMEOUT);
	    factory.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
	    return factory;
	}   
}
