/*
 * @author Dan&Dan
 */
package capri.fabiani.components;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Throwables;

import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.DatumService;
import capri.fabiani.ec2.InstanceManager;
import capri.fabiani.model.Identity;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.model.rest.FingerNotifyPredecessorDTO;
import capri.fabiani.model.rest.HelloDTO;
import capri.fabiani.model.rest.IdentityDTO;
import capri.fabiani.model.rest.TransportKeysDTO;
import capri.fabiani.util.Logger;
import capri.fabiani.util.SenderReceiverUtils;

/**
 * Contains methods to retreive keys during the configuration phase Contains
 * scheduled methods to destroy the configuration controller TODO.
 */
@Component
public class ConfigurationDaemon {

	/** The worker node. */
	@Autowired
	WorkerNode workerNode;

	/** The app context. */
	@Autowired
	private ApplicationContext appContext;

	/** The datum service. */
	@Autowired
	private DatumService datumService;

	/** The num messagges. */
	int messageIndex, numMessagges;

	/** The upper bound interval. */
	long lowBoundInterval = -1, upperBoundInterval = -1; // l'upperbouond è la

	/** The id succ. */
	// chiave
	Identity idPred, idSucc;

	/** The timer. */
	long timer;

	/** The timeout. */
	private long timeout;

	/**
	 * Instantiates a new configuration daemon.
	 */
	public ConfigurationDaemon() {
		initialize();
	}

	/**
	 * Abort.
	 */
	public void abort() {
		if (lowBoundInterval == -1 && upperBoundInterval == -1)
			datumService.deleteByRange(Constants.MIN_VALUE, Constants.MAX_VALUE);
		else
			datumService.deleteByRange(lowBoundInterval, upperBoundInterval);
	}

	/**
	 * Initialize.
	 */
	private void initialize() {
		messageIndex = -1;
		// TODO togliere
		Logger.status("Initializing configuration daemon");
		numMessagges = -1;
		lowBoundInterval = -1;
		upperBoundInterval = -1;
		idPred = null;
		idSucc = null;
		timer = Long.MAX_VALUE;
	}

	/**
	 * Finalize info.
	 */
	private void finalizeInfo() {
		// Vanno inseriti successore e predecessore, va inserito l'intervallo
		// delle chiavi

		BeanDefinitionRegistry factory = (BeanDefinitionRegistry) appContext.getAutowireCapableBeanFactory();
		((DefaultListableBeanFactory) factory).removeBeanDefinition("init");
		// ConfigurationController conf = (ConfigurationController)
		// appContext.getBean("init");
	}

	/**
	 * Hello message.
	 *
	 * @param helloDTO
	 *            the hello dto
	 * @return the response entity
	 */
	public synchronized ResponseEntity<DTO> helloMessage(HelloDTO helloDTO) {
		try {
			// messageIndex = 0;
			numMessagges = helloDTO.getNumMessages();
			if (helloDTO.getIdPred() != null)
				if (helloDTO.getIdPred().getKey() == Constants.MAX_VALUE) {
					lowBoundInterval = 0;
				} else {
					lowBoundInterval = (helloDTO.getIdPred().getKey() + 1) % Constants.MODULUS;

				}
			else {
				lowBoundInterval = 0;// ??

			}
			upperBoundInterval = helloDTO.getUpperBoundInterval();
			idPred = helloDTO.getIdPred();
			idSucc = helloDTO.getIdSucc();
			DTO dto = new DTO();
			dto.setMessage(Constants.HELLO_SUCCEFULL);
			ResponseEntity<DTO> responseEntity = new ResponseEntity<DTO>(dto, HttpStatus.OK);
			Logger.status("Received Hello message. waiting for " + numMessagges + " messages");
			timeout = System.currentTimeMillis() + Constants.SEND_KEYS_TIMEOUT;
			if (numMessagges == 0) {
				TransportKeysDTO transportKeysDTO = new TransportKeysDTO();
				transportKeysDTO.setMessageIndex(messageIndex + 1);
				transportKeysDTO.setDatas(new ArrayList<BusinessDTO>());
				getKeys(transportKeysDTO);
			}
			return responseEntity;
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			DTO dto = new DTO();
			dto.setError(e.toString());
			return new ResponseEntity<DTO>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * Gets the keys.
	 *
	 * @param transportKeysDTO
	 *            the transport keys dto
	 * @return the keys
	 */
	public synchronized ResponseEntity<DTO> getKeys(TransportKeysDTO transportKeysDTO) {
		DTO dto = new DTO();
		ResponseEntity<String> response = null;
		ResponseEntity<DTO> responseEntity;
		boolean isTopredecessorWrunlock = false, isTofingerWrUnlock = false, isTonextNodesWrUnlock = false,
				isToidentityWrUnlock = false, isTostatusCodeWrUnlock = false;
		try {
			if (lowBoundInterval == -1 || upperBoundInterval == -1) {
				// errore la connessione non è inizializzata
				abort();
				initialize();
				dto.setError(Constants.TRANSPORT_NOT_INITIALIZED);
				dto.setMessage(Constants.TRANSPORT_MESSAGE_NOT_INITIALIZED);
				responseEntity = new ResponseEntity<DTO>(dto, HttpStatus.BAD_REQUEST);
				return responseEntity;
			}
			boolean condition = transportKeysDTO.getMessageIndex() != messageIndex + 1;
			if (condition) {
				// abort
				initialize();
				abort();
				dto.setError(Constants.TRANSPORT_ABORT);
				dto.setMessage(Constants.TRANSPORT_ABORT_MESSAGE);
				responseEntity = new ResponseEntity<DTO>(dto, HttpStatus.BAD_REQUEST);
			} else {
				SenderReceiverUtils.receiveInterval(transportKeysDTO, datumService);
				messageIndex++;
				Logger.status("Received message n° " + (messageIndex + 1));
				dto.setMessage(Constants.TRANSPORT_SUCCESS);
				if (messageIndex + 1 >= numMessagges) {
					Logger.status("Finalizing info");
					Logger.status("lowerBound : " + lowBoundInterval + "; upperBound : " + upperBoundInterval
							+ "; idPred : " + (idPred == null ? "null" : idPred.toString()) + "; idSucc : "
							+ (idSucc == null ? "null" : idSucc.toString()) + ".");
					dto.setMessage(Constants.TRANSPORT_FINISHED);
					// notify predecessor in finger table
					InetAddress localAddress;
					try {
						localAddress = InetAddress.getLocalHost();
					} catch (UnknownHostException e) {
						localAddress = InetAddress.getLoopbackAddress();
						Logger.error(Throwables.getStackTraceAsString(e));
					}
					// conviene prendere prima tutti i lock, di modo che
					// l'operazione
					// sia nuclear
					workerNode.predecessorWrLock();
					isTopredecessorWrunlock = true;
					workerNode.fingerWrLock();
					isTofingerWrUnlock = true;
					workerNode.nextNodesWrLock();
					isTonextNodesWrUnlock = true;
					workerNode.identityWrLock();
					isToidentityWrUnlock = true;
					workerNode.statusCodeWrLock();
					isTostatusCodeWrUnlock = true;
					if (idPred != null) {
						URI uri = null;
						try {
							uri = new URI(Constants.HTTP_PRIMER + idPred.getAddress().toString()
									+ Constants.APP_NAME_PATH + Constants.FINGER_NOTIFY_PREDECESSOR);

							FingerNotifyPredecessorDTO fnsdto = new FingerNotifyPredecessorDTO();
							fnsdto.setNotifierId(new IdentityDTO(localAddress.getHostAddress(), upperBoundInterval));
							RequestEntity<FingerNotifyPredecessorDTO> request = RequestEntity.post(uri)
									.accept(MediaType.APPLICATION_JSON).body(fnsdto);
							RestTemplate restTemplate = HttpEntityConfiguration.getTemplate();
							response = restTemplate.exchange(request, String.class);
							Logger.status("Predecessor notified in configuration daemon");
						} catch (URISyntaxException e) {

							Logger.error(Throwables.getStackTraceAsString(e));
						} catch (Exception e) {
							Logger.error("Exception :" + Throwables.getStackTraceAsString(e) + " Response status: "
									+ (response != null ? response.toString() : "null"));
						}
					}

					// upper la chiave nodo in identyty del worker
					workerNode.setIdentity(new Identity(localAddress.getHostAddress(), upperBoundInterval));

					// TODO
					Logger.status("Setting predecessor in getKeys() " + (idPred == null ? "NULL" : idPred.toString()));

					// predecessore(campo a parte con lock) e successore(nodo0
					// della
					// fingertable lock finger)
					workerNode.setPredecessor(idPred);

					if (idSucc != null) {
						workerNode.setStatusCode(2);
						List<Identity> fingerTable = workerNode.getFingerTable();
						fingerTable.add(0, idSucc);
						// TODO togliere
						Logger.status("Added successor in finger table: " + fingerTable.get(0).toString());

						workerNode.setFingerTable(fingerTable);

						workerNode.getNextNodes().add(0, idSucc);
						Logger.status("Added node in next nodes list " + workerNode.getNextNodes().get(0));
					} else {
						workerNode.setStatusCode(1);
					}
					
					InstanceManager im = new InstanceManager();
					im.initialize();
					im.RegisterToAutoscaling();
					
					// ora gli unlock
					workerNode.predecessorWrUnlock();
					isTopredecessorWrunlock = false;
					workerNode.fingerWrUnlock();
					isTofingerWrUnlock = false;
					workerNode.nextNodesWrUnlock();
					isTonextNodesWrUnlock = false;
					workerNode.identityWrUnlock();
					isToidentityWrUnlock = false;
					workerNode.statusCodeWrUnlock();
					isTostatusCodeWrUnlock = false;
					finalizeInfo();
				}
				responseEntity = new ResponseEntity<DTO>(dto, HttpStatus.OK);
				timeout = System.currentTimeMillis() + Constants.SEND_KEYS_TIMEOUT;
			}
		} catch (Exception e) {

			Logger.error(Throwables.getStackTraceAsString(e));
			if (isTopredecessorWrunlock)
				workerNode.predecessorWrUnlock();
			if (isTofingerWrUnlock)
				workerNode.fingerWrUnlock();
			if (isTonextNodesWrUnlock)
				workerNode.nextNodesWrUnlock();
			if (isToidentityWrUnlock)
				workerNode.identityWrUnlock();
			if (isTostatusCodeWrUnlock)
				workerNode.statusCodeWrUnlock();
			dto.setError(e.toString());
			dto.setMessage("Error receiving keys.");
			responseEntity = new ResponseEntity<DTO>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	/**
	 * Timout.
	 */
	@Scheduled(initialDelay = /*
								 * Constants.CONFIGURATION_TIMEOUT_INITIAL_DELAY
								 */2000, fixedDelay = Constants.SEND_KEYS_DELAY_TIMEOUT)
	public synchronized void timout() {
		boolean isToStatusRdUnlock = false;
		int status;
		try {
			workerNode.statusCodeRdLock();
			isToStatusRdUnlock = true;

			status = workerNode.getStatusCode();

			workerNode.statusCodeRdUnlock();
			isToStatusRdUnlock = false;
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			if (isToStatusRdUnlock)
				workerNode.statusCodeRdUnlock();
			status = 0;
		}

		if (status == 0 && System.currentTimeMillis() >= timeout) {
			abort();
			initialize();
		}
	}

}
