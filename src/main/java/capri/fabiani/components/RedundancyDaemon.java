/*
 * @author Dan&Dan
 */
package capri.fabiani.components;

import java.net.URI;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.common.base.Throwables;

import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.AsyncService;
import capri.fabiani.controller.DatumService;
import capri.fabiani.controller.MasterPersistenceService;
import capri.fabiani.model.Identity;
import capri.fabiani.model.RedundancyIdentity;
import capri.fabiani.model.RedundancyManagementData;
import capri.fabiani.model.SlaveCommitMgtDatum;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.IdentityDTO;
import capri.fabiani.threads.PrimeReplicaRunnable;
import capri.fabiani.util.Logger;

/**
 * The Class RedundancyDaemon.
 */
@Component
public class RedundancyDaemon {

	/** The self. */
	@Autowired
	private WorkerNode self;

	/** The datum service. */
	@Autowired
	private DatumService datumService;

	/** The async service. */
	@Autowired
	private AsyncService asyncService;

	/**
	 * Removes RedundancyManagementData from old master nodes.
	 */
	@Scheduled(initialDelay = Constants.INITIAL_SWEEP_REDUNDANCY_DATA_DELAY
			* 2, fixedDelay = Constants.SWEEP_REDUNDANCY_DATA_DELAY)
	public void sweepRedundancyData() {
		boolean isEmployersWrUnlock = false;
		boolean isToMgtDataDismissUnlock = false;
		boolean isToMgtDataPendingCommitsUnlock = false;
		boolean isToMgtDataTimestampUnlock = false;
		boolean isToIdentityRdUnlock = false;
		boolean isToPredecessorRdUnlock = false;

		Iterator<RedundancyManagementData> redIter;

		RedundancyManagementData redMgtData = null;
		List<RedundancyManagementData> goodList = new LinkedList<RedundancyManagementData>();

		boolean expired;

		long myKey;
		long myLowEnd;
		String myAddress;

		Logger.status("Sweeping redundancy data...");

		try {
			self.identityRdLock();
			isToIdentityRdUnlock = true;
			myKey = self.getIdentity().getKey();
			myAddress = self.getIdentity().getAddress();
			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			self.predecessorRdLock();
			isToPredecessorRdUnlock = true;
			myLowEnd = self.getPredecessor().getKey();
			self.predecessorRdUnlock();
			isToPredecessorRdUnlock = false;

			self.employersWrLock();
			isEmployersWrUnlock = true;

			redIter = self.getEmployersTable().values().iterator();

			while (redIter.hasNext()) {
				redMgtData = redIter.next();

				redMgtData.dismissLock();
				isToMgtDataDismissUnlock = true;

				redMgtData.timestampLock();
				isToMgtDataTimestampUnlock = true;

				redMgtData.pendingCommitsLock();
				isToMgtDataPendingCommitsUnlock = true;

				expired = System.currentTimeMillis() - redMgtData.getTimestamp() > Constants.MASTER_TIMEOUT;

				// Remove the master data if there are no pending commits, and
				// the master has dismissed this node or has gone silent for too
				// long.
				if ((expired || redMgtData.isDismissing()) && redMgtData.getPendingCommits().isEmpty()) {
					Logger.status("Master " + redMgtData.toString() + " expired: " + expired);
					redIter.remove();
				} else {
					goodList.add(redMgtData);
				}

				redMgtData.pendingCommitsUnlock();
				isToMgtDataPendingCommitsUnlock = false;

				redMgtData.timestampUnlock();
				isToMgtDataTimestampUnlock = false;

				redMgtData.dismissUnlock();
				isToMgtDataDismissUnlock = false;
			}

			self.employersWrUnlock();
			isEmployersWrUnlock = false;

			myLowEnd = (myLowEnd + 1) % Constants.MODULUS;
			if (myLowEnd < 0) {
				myLowEnd += Constants.MODULUS;
			}

			// Add a RedundancyIdentity with my own range (in case I haven't
			// enlisted myself as a slave yet)
			goodList.add(new RedundancyManagementData(myAddress, myKey, myLowEnd));

			Logger.status("Sweeping redundancy data - masters to keep " + goodList.toString());

			flushGaps(goodList, myKey, myLowEnd);

		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToPredecessorRdUnlock)
				self.predecessorRdUnlock();
			if (isEmployersWrUnlock)
				self.employersWrUnlock();
			if (isToMgtDataTimestampUnlock)
				redMgtData.timestampUnlock();
			if (isToMgtDataDismissUnlock)
				redMgtData.dismissUnlock();
			if (isToMgtDataPendingCommitsUnlock)
				redMgtData.pendingCommitsUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Redundancy data sweep terminated.");
	}

	/**
	 * Removes pending commits whose timers have elapsed.
	 */
	@Scheduled(initialDelay = Constants.COMMIT_TIMEOUT * 2, fixedDelay = Constants.COMMIT_TIMEOUT * 2)
	public void sweepElapsedCommits() {
		boolean isToEmployersRdUnlock = false;
		boolean isToPendingCommitsUnlock = false;

		List<RedundancyManagementData> employers;
		RedundancyManagementData redMgtData = null;
		
		SlaveCommitMgtDatum commitMgtDatum;
		MasterPersistenceService.CommitOpType commitOp;
		BusinessDTO commitDto;
		
		Iterator<RedundancyManagementData> iter;
		Iterator<SlaveCommitMgtDatum> commitIter;

		Logger.status("Sweeping elapsed commits...");
		try {
			employers = new LinkedList<RedundancyManagementData>();

			/*
			 * We just need the RedundancyManagementData for the currently
			 * registered masters. This lock/unlock sequence just ensures that
			 * we get the data from the map safely, as we won't need to modify
			 * the map itself, and we won't care to check for elapsed commit of
			 * new masters registered in the meanwhile (they shall be checked on
			 * the next scheduled execution).
			 */
			self.employersRdLock();
			isToEmployersRdUnlock = true;

			employers.addAll(self.getEmployersTable().values());

			self.employersRdUnlock();
			isToEmployersRdUnlock = false;

			iter = employers.iterator();

			while (iter.hasNext()) {
				redMgtData = iter.next();

				redMgtData.pendingCommitsLock();
				isToPendingCommitsUnlock = true;

				commitIter = redMgtData.getPendingCommits().values().iterator();

				while (commitIter.hasNext()) {
					commitMgtDatum = commitIter.next();
					if (System.currentTimeMillis() - commitMgtDatum.getLastHeard() > Constants.COMMIT_TIMEOUT) {
						//Proceed to commit if the commit state was pre-commit.
						if(commitMgtDatum.getState() == SlaveCommitMgtDatum.SlaveState.PRECOMMIT){
							commitOp = commitMgtDatum.getCommitOpType();
							commitDto = commitMgtDatum.getBusinessData();
							switch(commitOp) {
							case CREATE:
								datumService.create(commitDto);
								break;
							case DELETE:
								datumService.delete(commitDto.getKey());
								break;
							case UPDATE:
								datumService.update(commitDto);
								break;
							}
						}
						commitIter.remove();
					}
				}

				redMgtData.pendingCommitsUnlock();
				isToPendingCommitsUnlock = false;
			}
		} catch (Exception e) {
			if (isToEmployersRdUnlock)
				self.employersRdUnlock();
			if (isToPendingCommitsUnlock)
				redMgtData.pendingCommitsUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Elapsed commit sweet terminated.");
	}

	/**
	 * Dismisses old replicas (e.g. after new nodes have been marked as
	 * replicas)
	 */
	@Scheduled(initialDelay = Constants.INITIAL_DISMISS_OLD_REPLICAS_DELAY, fixedDelay = Constants.DISMISS_OLD_REPLICAS_DELAY)
	public void dismissOldReplicas() {

		boolean isToReplicasToDismissUnlock = false;

		List<RedundancyIdentity> toDismissList = new LinkedList<RedundancyIdentity>();

		Logger.status("Dismissing old replicas...");

		try {
			self.replicasToDismissLock();
			isToReplicasToDismissUnlock = true;

			toDismissList.addAll(self.getReplicasToDismiss());
			self.clearReplicasToDismiss();

			self.replicasToDismissUnlock();
			isToReplicasToDismissUnlock = false;

			for (RedundancyIdentity redId : toDismissList) {
				dismissReplicaSingle(redId);
			}
		} catch (Exception e) {
			if (isToReplicasToDismissUnlock)
				self.replicasToDismissUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Old replica dismissal terminated.");
	}

	/**
	 * Prepares the slave table by picking redundancy nodes from the nextNodes
	 * list.
	 */
	@Scheduled(initialDelay = Constants.REDUNDANCY_LEASE_RENEW_INITIAL_DELAY, fixedDelay = Constants.REDUNDANCY_LEASE_RENEW_DELAY)
	public void enlistSlaves() {

		/*
		 * If I receive less responses than the required redundancy level, then,
		 * assuming fail-stop, no other nodes are present. I should try to have
		 * more nodes instantiated then.
		 * 
		 * For now it is assumed that there are always exactly the required
		 * number of nodes in the redundancy candidates list
		 */

		boolean isToNextNodesRdUnlock = false;
		boolean isToRedundancyWrUnlock = false;
		boolean isToReplicasToDismissUnlock = false;

		boolean isToIdentityRdUnlock = false;
		boolean isToPredecessorRdUnlock = false;

		List<RedundancyIdentity> redIdTable;
		List<Identity> nextNodes;
		Map<Long, RedundancyIdentity> oldTable;
		RedundancyIdentity rId;

		long myKey, myLowEnd;
		String myAddress;

		Logger.status("Putting nodes in charge...");

		try {
			self.identityRdLock();
			isToIdentityRdUnlock = true;

			myKey = self.getIdentity().getKey();
			myAddress = self.getIdentity().getAddress();

			self.identityRdUnlock();
			isToIdentityRdUnlock = false;

			self.predecessorRdLock();
			isToPredecessorRdUnlock = true;

			myLowEnd = (self.getPredecessor().getKey() + 1) % Constants.MODULUS;
			if (myLowEnd < 0) {
				myLowEnd = myLowEnd + Constants.MODULUS;
			}

			self.predecessorRdUnlock();
			isToPredecessorRdUnlock = false;

			self.redundancyWrLock();
			isToRedundancyWrUnlock = true;

			redIdTable = self.getRedundancyTable();

			oldTable = new HashMap<Long, RedundancyIdentity>();

			if (!redIdTable.isEmpty()) {
				for (RedundancyIdentity redId : redIdTable) {
					oldTable.put(redId.getKey(), redId);
				}

				rId = redIdTable.get(0);
			} else { // First time, add myself
				rId = new RedundancyIdentity(myAddress, myKey, true);
				oldTable.put(myKey, rId);
			}

			redIdTable = new LinkedList<RedundancyIdentity>();

			self.nextNodesRdLock();
			isToNextNodesRdUnlock = true;

			nextNodes = self.getNextNodes();

			redIdTable.add(rId);

			Logger.status(nextNodes.size() + " next nodes available...");
			/*
			 * Insert new elements while the key of the element in newOnes is
			 * less than the key of the element in oldOnes, taking care to
			 * determine whether key overflow occurred, then prime all nodes
			 * "not ready" in redTable, and dismiss the rest of oldOnes
			 */
			for (Identity nodeId : nextNodes) {

				// Prepare a new "not ready" entry if the element is not found
				// among the old ones OR if it has a different address.
				if ((rId = oldTable.get(nodeId.getKey())) == null || !rId.getAddress().equals(nodeId.getAddress())) {

					rId = new RedundancyIdentity(nodeId.getAddress(), nodeId.getKey(), false);

				} else { // Remove the entry from oldTable if it's still valid
					oldTable.remove(rId.getKey());
				}

				redIdTable.add(rId);

			}

			self.nextNodesRdUnlock();
			isToNextNodesRdUnlock = false;

			self.setRedundancyTable(redIdTable);

			// Prime nodes. This may include pinging nodes already primed and
			// ready, but it also enforces a soft-state which allows for easy
			// range updates. The function is asyncronous, and shall
			for (RedundancyIdentity redId : redIdTable) {
				asyncService.run(new PrimeReplicaRunnable(redId, myKey, myLowEnd, myAddress, self, datumService));
			}

			self.redundancyWrUnlock();
			isToRedundancyWrUnlock = false;

			self.replicasToDismissLock();
			isToReplicasToDismissUnlock = true;

			redIdTable = self.getReplicasToDismiss();

			// Dismiss old ones. Old valid nodes are not in oldTable anymore
			for (RedundancyIdentity redId : oldTable.values()) {
				redIdTable.add(redId);
			}

			redIdTable = null;

			self.replicasToDismissUnlock();
			isToReplicasToDismissUnlock = false;

		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToPredecessorRdUnlock)
				self.predecessorRdUnlock();
			if (isToRedundancyWrUnlock)
				self.redundancyWrUnlock();
			if (isToNextNodesRdUnlock)
				self.nextNodesRdUnlock();
			if (isToReplicasToDismissUnlock)
				self.replicasToDismissUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
		}

		Logger.status("Redundancy candidates processed.");
	}

	/**
	 * Range updated.
	 *
	 * @param newPredecessor
	 *            the new predecessor
	 */
	@Async
	public void rangeUpdated(IdentityDTO newPredecessor) {
		boolean isToPredecessorWrUnlock = false;

		/*
		 * There would be many cases to cover, which would ultimately require
		 * the master node to put in charge its replicas with the updated range.
		 * However, the update does reach the replicas in any case in the
		 * subsequent scheduled calls to refresh the redundancy table. It would
		 * speed things up to start at once the refresh process, which however
		 * is not going to be covered at the current stage
		 */

		Logger.status("Updating range, new predecessor: " + newPredecessor.toString());
		try {
			self.predecessorWrLock();
			isToPredecessorWrUnlock = true;

			Logger.status("Setting predecessor in rangeUpdated(): " + newPredecessor.toString());

			self.setPredecessor(new Identity(newPredecessor.getAddress(), newPredecessor.getKey()));

			self.predecessorWrUnlock();
			isToPredecessorWrUnlock = false;

		} catch (Exception e) {
			if (isToPredecessorWrUnlock)
				self.predecessorWrUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
		}
		// Next time the processRedundancyCandidates function is invoked,
		// replicas will be primed accordingly.

		Logger.status("Range updated.");
	}

	/**
	 * Dismiss replica single.
	 *
	 * @param redId
	 *            the red id
	 */
	// Not async. This doesn't really need to be done all at once.
	private void dismissReplicaSingle(RedundancyIdentity redId) {

		String url;
		URI uri;
		RequestEntity<Void> request;

		Logger.status("Dismissing replica " + redId.getKey() + " at address " + redId.getAddress());

		try {
			url = Constants.HTTP_PRIMER + redId.getAddress() + Constants.APP_NAME_PATH + Constants.SLAVE_DISMISS + "/"
					+ redId.getKey();
			uri = new URI(url);
			request = RequestEntity.post(uri).build();

			HttpEntityConfiguration.getTemplate().exchange(request, String.class);

			// Not much to do with
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

	private void flushGaps(List<RedundancyManagementData> list, long myKey, long myLowEnd) {
		try {
			long absMinLeft = myLowEnd;
			long roundMinLeft = -1l; // Control value
			long temp, temp2;

			for (RedundancyManagementData data : list) {
				temp = data.getLowEnd();

				if (temp < absMinLeft) {
					absMinLeft = temp;
				}

				if (roundMinLeft != -1l) {
					if (temp > myKey && temp < roundMinLeft) {
						roundMinLeft = temp;
					}
				} else {
					roundMinLeft = temp;
				}
			}

			//Not overlapping the overflow region.
			if (roundMinLeft < 0) {
				// Delete between me and Constants.MAX_VALUE, and between Constants.MIN_VALUE and absMinLeft
				temp = (myKey+1)%Constants.MODULUS;
				if(temp<0) {
					temp+=Constants.MODULUS;
				}
				temp2 = (absMinLeft-1)%Constants.MODULUS;
				if(temp2<0) {
					temp2+=Constants.MODULUS;
				}
				
				if(!(temp==Constants.MIN_VALUE && temp2==Constants.MAX_VALUE)) {
					datumService.deleteByRange(temp, temp2);
				}
			} else {
				// Delete between myself+1 and roundMinLeft-1. The execution
				// flow may effectively only get here if
				// myKey<Constants.MAX_VALUE && roundMinLeft>0, so there's no
				// need to check for overflow.
				if((roundMinLeft - 1)>=(myKey + 1)) {
					datumService.deleteByRange(myKey + 1, roundMinLeft - 1);
				}
			}

		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

}
