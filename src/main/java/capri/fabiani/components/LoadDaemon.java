/*
 * @author Dan&Dan
 */
package capri.fabiani.components;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Throwables;

import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.AsyncService;
import capri.fabiani.controller.DatumService;
import capri.fabiani.controller.FingerTableService;
import capri.fabiani.ec2.InstanceManager;
import capri.fabiani.model.Identity;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.model.rest.HelloDTO;
import capri.fabiani.model.rest.RetirementRequestDTO;
import capri.fabiani.threads.HttpRequestDelegateRunnable;
import capri.fabiani.util.Logger;
import capri.fabiani.util.SenderReceiverUtils;

/**
 * The Class LoadDaemon.
 */
@Component
public class LoadDaemon {
	/*
	 * TODO Questo si controlla IL PROPRIO intervallo di chiavi (i.e. di cui è
	 * il master). Se ha più di <valore da mettere in Constants> chiavi,
	 * istanzia un nuovo nodo con chiave la mediana delle proprie chiavi (
	 * questo si basa sul fatto che un nodo non può avere più di N*max chiavi,
	 * considerando che fa da replica a n-1 nodi).
	 * 
	 * Se sono troppe poche, crasha (il sistema si recupererà da solo) - serve
	 * fare qualcosa tramite Amazon.
	 */

	/** The datum service. */
	@Autowired
	DatumService datumService;
	
	/** The worker node. */
	@Autowired
	WorkerNode workerNode;

	/** The ft service. */
	@Autowired
	private FingerTableService ftService;
	
	/** The async service. */
	@Autowired
	private AsyncService asyncService;

	/**
	 * Instantiates a new load daemon.
	 */
	public LoadDaemon() {
	}

	/**
	 * Check load.
	 */
	@Scheduled(initialDelay = Constants.LOAD_CHECK_INITIAL_DELAY , fixedDelay = Constants.SEND_KEYS_DELAY_TIMEOUT)
	public void checkLoad() {
		boolean isToIdentityRdUnlock = false, isToPredecessorRdUnlock = false;
		try {
			long timeout, start, curr;

			workerNode.identityRdLock();
			isToIdentityRdUnlock = true;
			workerNode.predecessorRdLock();
			isToPredecessorRdUnlock = true;
			long startId = 0;
			if (workerNode.getPredecessor() != null) {
				startId = (workerNode.getPredecessor().getKey() + 1) % Constants.MODULUS;
				if(startId<0)
					startId += Constants.MODULUS;
			}
			long endId = workerNode.getIdentity().getKey();
			workerNode.identityRdUnlock();
			isToIdentityRdUnlock = false;
			workerNode.predecessorRdUnlock();
			isToPredecessorRdUnlock = false;
			Long count = datumService.countByIdRange(startId, endId);
			if (count > Constants.MAX_LOAD_KEYS) {
				cutLoad(false);
			} else if (count < Constants.MIN_LOAD_KEYS) {

				// Wait a random timeout hopefully avoiding synchronizing and
				// deadlocking with other nodes trying to shut down
				timeout = new Double(Math.random()
						* (Constants.LOAD_RETIREMENT_TIREMOUT_MAX - Constants.LOAD_RETIREMENT_TIMEOUT_MIN)).longValue()
						+ Constants.LOAD_RETIREMENT_TIMEOUT_MIN;
				
				Logger.status("Waiting " + timeout + " milliseconds before requesting permission to shutdown...");

				start = curr = System.currentTimeMillis();
				synchronized (this) {
					while (curr - start < timeout) {
						try {
							// Avoid busy wait
							this.wait(timeout);
						} catch (InterruptedException e) {
							Logger.error(Throwables.getStackTraceAsString(e));
						}
						curr = System.currentTimeMillis();
					}
				}

				if (requestShutDownPermission()) {
					Logger.status("Permission to shut down granted. Shutting down...");
					shutDown();
				}
			}
		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				workerNode.identityRdUnlock();
			if (isToPredecessorRdUnlock)
				workerNode.predecessorRdUnlock();
			Logger.error(Throwables.getStackTraceAsString(e));
		}

	}

	/**
	 * Retirement sweep.
	 */
	@Scheduled(initialDelay = Constants.RETIREMENT_SWEEP_INITIAL_DELAY, fixedDelay = Constants.RETIREMENT_SWEEP_DELAY)
	public void retirementSweep() {
		boolean isToRetirementUnlock = false;

		try {
			workerNode.retirementLock();
			isToRetirementUnlock = true;

			if (System.currentTimeMillis()
					- workerNode.getRetirementVotingTimestamp() > Constants.RETIREMENT_VOTE_TIMEOUT) {
				workerNode.setRetirementCandidateKey(null);
			}

			workerNode.retirementUnlock();
			isToRetirementUnlock = false;
		} catch (Exception e) {
			if (isToRetirementUnlock)
				workerNode.retirementUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

	/**
	 * Request shut down permission.
	 *
	 * @return true, if successful
	 */
	private boolean requestShutDownPermission() {

		boolean mayTerminate = false;

		boolean isToRetirementUnlock = false;
		boolean isToIdentityRdUnlock = false;

		final long myKey;
		final String myAddress;
		long start, curr;
		Long retirementCandidate;
		Map<Long, Identity> retirementCouncil;

		String url;
		String firstForwardeeIp;
		URI uri;
		RetirementRequestDTO requestDto;
		RequestEntity<RetirementRequestDTO> request;
		ResponseEntity<String> response;
		
		Runnable runnable;

		Logger.status("Requesting shutdown permission...");

		try {
			workerNode.identityRdLock();
			isToIdentityRdUnlock = true;

			myKey = workerNode.getIdentity().getKey();
			myAddress = workerNode.getIdentity().getAddress();

			workerNode.identityRdUnlock();
			isToIdentityRdUnlock = false;

			firstForwardeeIp = ftService.getForwardee(myKey + 1).getAddress();

			workerNode.retirementLock();
			isToRetirementUnlock = true;

			retirementCandidate = workerNode.getRetirementCandidateKey();
			if (retirementCandidate == null) {
				Logger.status("Asking for shutdown...");

				workerNode.setRetirementCandidateKey(myKey);
				
				workerNode.clearRetirementCouncil();

				// Send the request
				requestDto = new RetirementRequestDTO();
				requestDto.setKey(myKey);
				requestDto.setAddress(myAddress);
				requestDto.setShortCount(Constants.RETIREMENT_QUORUM);

				url = Constants.HTTP_PRIMER + firstForwardeeIp + Constants.APP_NAME_PATH + Constants.RETIREMENT_REQUEST;
				firstForwardeeIp = null;
				uri = new URI(url);
				request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(requestDto);
				try {
					response = HttpEntityConfiguration.getTemplate().exchange(request, String.class);
				}
				catch(Exception e) {					
					workerNode.setRetirementCandidateKey(null);
					throw e;
				}

				if (response.getStatusCode() == HttpStatus.OK) {
					Logger.status("Shutdown request forwarded...");

					// Wait for a quorum to be reached (or not)
					workerNode.setRetirementSignaled(false);
					workerNode.setRetirementVotingTimestamp(start = System.currentTimeMillis());

					while (!workerNode.isRetirementSignaled()
							&& (curr = System.currentTimeMillis()) - start < Constants.RETIREMENT_VOTE_TIMEOUT) {
						// There may be an InterruptedException, which is caught
						// by
						// the
						// whole try-catch macro-block; Abort operation in case.
						workerNode.retirementCondWaitTimed(Constants.RETIREMENT_VOTE_TIMEOUT - (curr - start));
					}

					// May terminate only if the thread has been signaled (-> no
					// timeout) and the required number of votes has been
					// reached
					if (workerNode.isRetirementSignaled()
							&& workerNode.getRetirementCouncil().size() >= Constants.RETIREMENT_QUORUM) {
						Logger.status("Permission granted.");
						mayTerminate = true;
					}
					workerNode.setRetirementCandidateKey(null);
					workerNode.clearRetirementCouncil();

					retirementCouncil = workerNode.getRetirementCouncil();

					// Respond to those in the retirementCouncil
					for (Identity id : retirementCouncil.values()) {
						Logger.status("Responding to voters...");
						
						
						runnable = new HttpRequestDelegateRunnable(prepareResponseToVoter(myKey, id.getAddress()));
						asyncService.run(runnable);
						
					}
				} else {
					Logger.status("Shutdown request not forwarded.");
					// Only tries the first successor for now
					workerNode.setRetirementCandidateKey(null);
				}
			}
			workerNode.retirementUnlock();
			isToRetirementUnlock = false;

		} catch (Exception e) {
			if (isToRetirementUnlock)
				workerNode.retirementUnlock();
			if (isToIdentityRdUnlock)
				workerNode.identityRdUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
			mayTerminate = false;
		}

		Logger.status("May terminate = " + mayTerminate);
		return mayTerminate;
	}

	/**
	 * Prepare response to voter.
	 *
	 * @param myKey the my key
	 * @param voterIp the voter ip
	 * @return the request entity
	 */
	private RequestEntity<DTO> prepareResponseToVoter(long myKey, String voterIp) {
		String url;
		URI uri;
		RequestEntity<DTO> request;

		Logger.status("Responding to retirement voter at ip " + voterIp);
		try {
			url = Constants.HTTP_PRIMER + voterIp + Constants.APP_NAME_PATH + Constants.RETIREMENT_ACK + myKey;
			uri = new URI(url);
			request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(new DTO());
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			return null;
		}
		return request;
	}

	/**
	 * Cut load.
	 *
	 * @param isAlone the is alone
	 */
	private void cutLoad(boolean isAlone) {
		boolean isToIdentityRdUnlock = false;
		boolean isToPredecessorRdUnlock = false;
		boolean isToPredecessorWrUnlock = false;
		boolean isToFingerRdUnlock = false;
		boolean isToFingerWrUnlock = false;
		
		try {
			InstanceManager im = new InstanceManager();
			im.initialize();
			String ip = im.newInstanceRequest();
			ResponseEntity<String> result = null;
			if (!ip.equals("")) {
				Logger.status("Created a new instance with ip: " + ip +(im.getImageName()!=null ? " and image name " + im.getImageName() : "" ));
			}
			long startId = Constants.MIN_VALUE;
			// lock
			workerNode.identityRdLock();
			isToIdentityRdUnlock = true;
			workerNode.predecessorRdLock();
			isToPredecessorRdUnlock = true;
			workerNode.fingerRdLock();
			isToFingerRdUnlock = true;
			
			Identity id = workerNode.getIdentity();
			long endId = id.getKey();
			if (workerNode.getPredecessor() != null) {
				startId = (workerNode.getPredecessor().getKey() + 1) % Constants.MODULUS;
				if (startId < 0) {
					startId = startId + Constants.MODULUS;
				}
			}
			long medianKey = datumService.findMedian(startId, endId);
			long count = datumService.countByIdRange(startId, medianKey);
			HelloDTO helloDTO = new HelloDTO();
			if (isAlone) {
				helloDTO.setIdPred(id);
				Logger.status("Cut load alone");

			} else {
				helloDTO.setIdPred(workerNode.getPredecessor());
				Logger.status("Cut load");
			}
			helloDTO.setIdSucc(id);
			int numMessages = new Double((Math.ceil(((double) count) / Constants.NUMBER_OF_MAX_DATUM_TO_SEND)))
					.intValue();
			helloDTO.setNumMessages(numMessages);
			helloDTO.setUpperBoundInterval(medianKey);
			// unlock
			workerNode.identityRdUnlock();
			isToIdentityRdUnlock = false;
			workerNode.predecessorRdUnlock();
			isToPredecessorRdUnlock = false;
			workerNode.fingerRdUnlock();
			isToFingerRdUnlock = false;
			
			boolean successfullSent = false;
			int retries = Constants.RETRIES;
			while (!successfullSent && retries > 0) {
				retries--;
				URI uri = null;
				try {
					uri = new URI(Constants.HTTP_PRIMER + ip + Constants.APP_NAME_PATH + Constants.INITIALIZATION_PATH
							+ Constants.HELLO);
				} catch (URISyntaxException e) {

					Logger.error(Throwables.getStackTraceAsString(e));
				}
				RequestEntity<HelloDTO> request = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON)
						.body(helloDTO);
				// non è gestito il caso di errore
				RestTemplate restTemplate = HttpEntityConfiguration.getTemplate();

				int helloes = Constants.VM_POLLING_RETRIES;
				while (helloes > 0) {
					try {
						Thread.sleep(Constants.VM_POLLING_TIMER);
					} catch (InterruptedException e1) {
						Logger.error(Throwables.getStackTraceAsString(e1));
					}
					try {
						result = restTemplate.exchange(request, String.class);

						if (result.getStatusCode() == HttpStatus.OK)
							break;
						helloes--;
					} catch (Exception e) {
						helloes--;
						Logger.error(Throwables.getStackTraceAsString(e));
						continue;
					}
				}
				if (helloes == 0) {
					Logger.status(
							"Initialization failed, shutting down... " + (result != null ? result.toString() : "null"));
					im.shutDown(im.getInstanceId());
					return;
				} else {
					Logger.status("Successful sent hello message");
				}
				successfullSent = SenderReceiverUtils.sendInterval(startId, medianKey, Constants.HTTP_PRIMER + ip
						+ Constants.APP_NAME_PATH + Constants.INITIALIZATION_CONTROLLER + Constants.RECEIVE_KEYS,
						datumService);
			}
			if (successfullSent) {
				// mediana+1 a me
				// predecessore è il nuovo nodo
				Logger.status("Successful sent " + count + " keys");
				workerNode.predecessorWrLock();
				isToPredecessorWrUnlock = true;
				
				InetAddress inetAddress = InetAddress.getLoopbackAddress();
				try {
					inetAddress = InetAddress.getByName(ip);
				} catch (UnknownHostException e) {
					Logger.error(Throwables.getStackTraceAsString(e));
				}
				Identity predecessor = new Identity(inetAddress.getHostAddress(), medianKey);
				Logger.status("Setting predecessor in cutLoad(): " + predecessor.toString());
				
				workerNode.setPredecessor(predecessor);
				if (isAlone) {
					workerNode.fingerWrLock();
					isToFingerWrUnlock = true;
					
					List<Identity> fingerTable = workerNode.getFingerTable();
					fingerTable.add(0, predecessor);
					
					workerNode.fingerWrUnlock();
					isToFingerWrUnlock = false;
				}
				workerNode.predecessorWrUnlock();
				isToPredecessorWrUnlock = false;
			} else {
				Logger.status("Initialization failed, shutting down instance " + im.getInstanceId());
				im.shutDown(im.getInstanceId());
			}
		} catch (Exception e) {
			if(isToFingerRdUnlock)
				workerNode.fingerRdUnlock();
			if(isToFingerWrUnlock)
				workerNode.fingerWrUnlock();
			if(isToIdentityRdUnlock)
				workerNode.identityRdUnlock();
			if(isToPredecessorRdUnlock)
				workerNode.predecessorRdUnlock();
			if(isToPredecessorWrUnlock)
				workerNode.predecessorWrUnlock();
			
			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

	/**
	 * Shut down.
	 */
	private void shutDown() {
		InstanceManager instanceManager = new InstanceManager();
		instanceManager.initialize();
		instanceManager.shutDown();
	}

	/**
	 * Check redundancy.
	 */
	@Scheduled(initialDelay = 30000, fixedDelay = Constants.SEND_KEYS_DELAY_TIMEOUT)
	public void checkRedundancy() {

		boolean isIdLocked = false, isPredLocked = false, isFingLocked = false, isStatLocked = false;
		workerNode.identityRdLock();
		isIdLocked = true;
		workerNode.predecessorRdLock();
		isPredLocked = true;
		workerNode.fingerRdLock();
		isFingLocked = true;
		workerNode.statusCodeRdLock();
		isStatLocked = true;
		try {
			Identity predecessor = workerNode.getPredecessor();
			Identity successor = null;
			if (!workerNode.getFingerTable().isEmpty()) {
				successor = workerNode.getFingerTable().get(0);
			}
			boolean areOnlyTwo = (predecessor == null ? false : predecessor.equals(successor));
			boolean isAloneInTheDark = workerNode.getStatusCode() == 1;
			workerNode.identityRdUnlock();
			isIdLocked = false;
			workerNode.predecessorRdUnlock();
			isPredLocked = false;
			workerNode.fingerRdUnlock();
			isFingLocked = false;
			workerNode.statusCodeRdUnlock();
			isStatLocked = false;

			if (successor != null) {
				if (areOnlyTwo) {
					cutLoad(false);
				}
			} else {
				if (isAloneInTheDark) {
					cutLoad(true);
				}
			}

		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			if (isIdLocked)
				workerNode.identityRdUnlock();
			if (isPredLocked)
				workerNode.predecessorRdUnlock();
			if (isFingLocked)
				workerNode.fingerRdUnlock();
			if (isStatLocked)
				workerNode.statusCodeRdUnlock();

		}
	}

}
