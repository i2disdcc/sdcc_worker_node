/*
 * @author Dan&Dan
 */
package capri.fabiani.components;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.common.base.Throwables;

import capri.fabiani.model.Identity;
import capri.fabiani.components.WorkerNode.PendingTask;
import capri.fabiani.configuration.Constants;
import capri.fabiani.model.rest.FingerFillTableDTO;
import capri.fabiani.model.rest.FingerNextNodesDTO;
import capri.fabiani.model.rest.FingerResponseDTO;
import capri.fabiani.model.rest.IdentityDTO;
import capri.fabiani.util.FingerNavigationUtils;
import capri.fabiani.util.Logger;

/**
 * The Class FingerTableDaemon.
 */
@Component
public class FingerTableDaemon {

	/** The self. */
	@Autowired
	private WorkerNode self;

	/**
	 * Refresh ft scheduled.
	 */
	// TODO rimuovi il *10, pure sotto
	@Scheduled(initialDelay = Constants.INITIAL_FT_REFRESH_DELAY * 10, fixedDelay = Constants.FT_REFRESH_DELAY)
	public void refreshFtScheduled() {
		Logger.status("Refreshing finger table, scheduled mode...");
		refreshFt();
	}

	/**
	 * Refresh next nodes scheduled.
	 */
	@Scheduled(initialDelay = Constants.INITIAL_REDUNDANCY_NODES_REFRESH_DELAY, fixedDelay = Constants.REDUNDANCY_NODES_REFRESH_DELAY)
	public void refreshNextNodesScheduled() {
		Logger.status("Refreshing redundancy candidates, scheduled mode...");
		refreshNextNodes();
	}

	// TODO
	//// @Scheduled
	// public void checkPredecessors() {
	// // TODO
	// }

	/**
	 * Refresh ft async.
	 */
	@Async
	public void refreshFtAsync() {
		Logger.status("Refreshing finger table, async mode...");
		refreshFt();
	}

	/**
	 * Refreshnext nodes async.
	 */
	@Async
	public void refreshnextNodesAsync() {
		Logger.status("Refreshing redundancy candidates, async mode...");
		refreshNextNodes();
	}

	/**
	 * Refresh ft.
	 */
	private void refreshFt() {

		boolean isToRefreshUnlock = false;

		boolean isToFRdUnlock = false;
		boolean isToFWrUnlock = false;
		boolean isToTasksUnlock = false;
		boolean isToFingerCandidatesUnlock = false;
		boolean isToIdentityRdUnlock = false;
		boolean isToSingleTaskUnlock = false;

		FingerFillTableDTO fftDto;
		List<Identity> ft;
		IdentityDTO idDto;

		int taskId;
		PendingTask task = null;

		boolean ref;
		long primerKey;

		boolean allClear;

		try {

			self.refreshFingerTableLock();
			isToRefreshUnlock = true;
			ref = self.isRefreshingFingerTable();

			if (!ref)
				self.setRefreshingFingerTable(true);

			self.refreshFingerTableUnlock();
			isToRefreshUnlock = false;
			// If there was not a refreshing operation in progress, we start
			// one.
			if (ref == false) {

				self.identityRdLock();
				isToIdentityRdUnlock = true;

				idDto = new IdentityDTO(self.getIdentity());

				self.identityRdUnlock();
				isToIdentityRdUnlock = false;

				self.fingerRdLock();
				isToFRdUnlock = true;

				ft = self.getFingerTable();

				// Not yet initialized
				if (ft.isEmpty()) {
					self.fingerRdUnlock();
					isToFRdUnlock = false;

					self.refreshFingerTableLock();
					isToRefreshUnlock = true;

					self.setRefreshingFingerTable(false);

					self.refreshFingerTableUnlock();
					isToRefreshUnlock = false;

					return;
				}

				self.fingerRdUnlock();
				isToFRdUnlock = false;

				self.tasksLock();
				isToTasksUnlock = true;

				taskId = self.getTaskIndex();
				task = new PendingTask(taskId, PendingTask.TaskType.SUCCESSOR_FINGER, Constants.F_TABLE_SIZE);
				// Overflow is negligible here: "old" task indexes will have
				// long been forgotten by the time their index comes anew
				self.setTaskIndex(taskId + 1);
				self.getPendingTasks().put(taskId, task);

				self.tasksUnlock();
				isToTasksUnlock = false;

				self.fingerCandidatesLock();
				isToFingerCandidatesUnlock = true;
				self.clearFingerCandidates();
				self.fingerCandidatesUnlock();
				isToFingerCandidatesUnlock = false;

				primerKey = (idDto.getKey() + 1) % Constants.MODULUS;
				if (primerKey < 0) {
					primerKey += Constants.MODULUS;
				}

				fftDto = new FingerFillTableDTO(idDto, taskId, primerKey, 0);

				allClear = FingerNavigationUtils.forwardForFillTable(self, fftDto);

				if (allClear) {

					long start, curr = 0;

					task.taskLock();
					isToSingleTaskUnlock = true;

					task.setSignaled(false);
					start = System.currentTimeMillis();

					// Armed to the teeth against spurious wakeups
					while (!task.isSignaled()
							&& ((curr = System.currentTimeMillis()) - start < Constants.FT_REFRESH_TIMER)) {
						Logger.status("CondWaitTimed inside FingerTableDaemon refreshFt(), curr = " + curr);
						try {
							// Should wait exactly the specified timer, aside
							// from precision problems from
							// System.currentTimeMillis()
							task.condWaitTimed(Constants.FT_REFRESH_TIMER - (curr - start));
						} catch (Exception e) {
							Logger.error("Finger refresh wait interrupted " + Throwables.getStackTraceAsString(e));
						}
					}

					task.taskUnlock();
					isToSingleTaskUnlock = false;

					Logger.status("FT refresh condWait terminated, elapsed time = "
							+ (Constants.FT_REFRESH_TIMER - (curr - start)) + " milliseconds.");

					// For the reminder of the operation, we ensure no new
					// Finger candidates will be accepted for the old task.
					// (i.e. possibly "late" responses shall be discarded)
					self.tasksLock();
					isToTasksUnlock = true;

					self.getPendingTasks().remove(taskId);

					self.tasksUnlock();
					isToTasksUnlock = false;

					// Refresh table
					List<Identity> newFt;
					Map<Integer, Identity> candidatesMap;
					int j, count, size;
					Identity id = null, oldId = null;

					self.fingerCandidatesLock();
					isToFingerCandidatesUnlock = true;
					self.fingerWrLock();
					isToFWrUnlock = true;

					candidatesMap = self.getFingerCandidates();

					Logger.status("Finger table candidates: "
							+ (!candidatesMap.isEmpty() ? candidatesMap.toString() : "EMPTY"));

					// if candidates is empty, may as well leave the old finger
					// table
					if (candidatesMap.isEmpty()) {
						self.fingerWrUnlock();
						isToFWrUnlock = false;
						self.fingerCandidatesUnlock();
						isToFingerCandidatesUnlock = false;

						self.refreshFingerTableLock();
						isToRefreshUnlock = true;
						self.setRefreshingFingerTable(false);
						self.refreshFingerTableUnlock();
						isToRefreshUnlock = false;

						return;
					}

					newFt = self.getNewEmptyFingerTable();

					count = 0;
					size = candidatesMap.size();

					// No repeated entries.
					for (j = -1; count < size && j < Constants.F_TABLE_SIZE; j++) {
						id = candidatesMap.get(j);
						if (id != null && !id.equals(oldId)) {
							Logger.status("New finger table: added node " + id.toString());
							newFt.add(id);
							oldId = id;
							count++;
						}
					}

					self.setFingerTable(newFt);

					self.fingerWrUnlock();
					isToFWrUnlock = false;

					self.clearFingerCandidates();

					self.fingerCandidatesUnlock();
					isToFingerCandidatesUnlock = false;
				} else {
					Logger.status("FT refresh request not forwarded. Aborting operation.");
					// We didn't receive a response when we first launched the
					// refresh operation on the chain. We remove the
					// corresponding task
					self.tasksLock();
					isToTasksUnlock = true;

					self.getPendingTasks().remove(taskId);

					self.tasksUnlock();
					isToTasksUnlock = false;
				}

				self.refreshFingerTableLock();
				isToRefreshUnlock = true;
				self.setRefreshingFingerTable(false);
				self.refreshFingerTableUnlock();
				isToRefreshUnlock = false;
			}
		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToRefreshUnlock)
				self.refreshFingerTableUnlock();
			if (isToFRdUnlock)
				self.fingerRdUnlock();
			if (isToFingerCandidatesUnlock)
				self.fingerCandidatesUnlock();
			if (isToFWrUnlock)
				self.fingerWrUnlock();
			if (isToSingleTaskUnlock) {
				task.taskUnlock();
				if (!isToTasksUnlock) {
					self.tasksLock();
					isToTasksUnlock = true;
				}
				self.getPendingTasks().remove(task.getTaskId());
			}
			if (isToTasksUnlock)
				self.tasksUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
		}

	}

	/**
	 * Refresh next nodes.
	 */
	private void refreshNextNodes() {
		boolean isToRefreshUnlock = false;
		boolean isToNextNodesWrUnlock = false;
		boolean isToNextNodesCandidatesUnlock = false;
		boolean isToTasksUnlock = false;
		boolean isToNextNodesRdUnlock = false;
		boolean isToIdentityRdUnlock = false;
		boolean isToSingleTaskUnlock = false;

		List<Identity> nextNodesTable;
		boolean allClear = false;

		int taskId;
		PendingTask task = null;

		List<FingerResponseDTO> candidatesList;

		boolean ref;

		try {

			self.refreshNextNodesLock();
			isToRefreshUnlock = true;

			ref = self.isRefreshingNextNodes();

			if (!ref)
				self.setRefreshingNextNodes(true);

			self.refreshNextNodesUnlock();
			isToRefreshUnlock = false;

			// If there was not a refreshing operation in progress, we start
			// one.
			if (ref == false) {
				FingerNextNodesDTO fnnDto;

				IdentityDTO idDto;

				self.identityRdLock();
				isToIdentityRdUnlock = true;

				idDto = new IdentityDTO(self.getIdentity());

				self.identityRdUnlock();
				isToIdentityRdUnlock = false;

				self.nextNodesRdLock();
				isToNextNodesRdUnlock = true;

				// An initialized node always has both a successor and a
				// predecessor set. If the next nodes list is empty, then the
				// node has not yet been initialized.
				nextNodesTable = self.getNextNodes();
				if (nextNodesTable.isEmpty()) {
					self.nextNodesRdUnlock();
					isToNextNodesRdUnlock = false;

					self.refreshNextNodesLock();
					isToRefreshUnlock = true;

					self.setRefreshingNextNodes(false);

					self.refreshNextNodesUnlock();
					isToRefreshUnlock = false;

					return;
				}
				nextNodesTable = null;

				self.nextNodesRdUnlock();
				isToNextNodesRdUnlock = false;

				self.tasksLock();
				isToTasksUnlock = true;

				taskId = self.getTaskIndex();
				task = new PendingTask(taskId, PendingTask.TaskType.SUCCESSOR_REPLICATION, Constants.REDUNDANCY_LEVEL);
				// Overflow is negligible here: "old" task indexes will have
				// long been forgotten by the time their index comes anew
				self.setTaskIndex(taskId + 1);
				self.getPendingTasks().put(taskId, task);

				self.tasksUnlock();
				isToTasksUnlock = false;

				self.nextNodesCandidatesLock();
				isToNextNodesCandidatesUnlock = true;
				self.clearNextNodesCandidates();
				self.nextNodesCandidatesUnlock();
				isToNextNodesCandidatesUnlock = false;

				// RedundancyLevel - 1 other nodes is what we need.
				fnnDto = new FingerNextNodesDTO(idDto, taskId, Constants.REDUNDANCY_LEVEL - 1);

				allClear = FingerNavigationUtils.forwardForNextNodes(self, fnnDto);

				if (allClear) {
					Logger.status("Refresh next nodes candidates request successfully forwarded...");

					long start, curr;

					task.taskLock();
					isToSingleTaskUnlock = true;

					task.setSignaled(false);
					start = System.currentTimeMillis();

					// Armed to the teeth against spurious wakeups
					while (!task.isSignaled() && ((curr = System.currentTimeMillis())
							- start < Constants.NEXT_NODES_CANDIDATES_REFRESH_TIMER)) {
						try {
							// Should wait exactly the specified timer, aside
							// from precision problems from
							// System.currentTimeMillis()
							task.condWaitTimed(Constants.NEXT_NODES_CANDIDATES_REFRESH_TIMER - (curr - start));
						} catch (Exception e) {
							Logger.warning("Exception caught while waiting in refreshNextNodes() - "
									+ Throwables.getStackTraceAsString(e));
						}
					}

					task.taskUnlock();
					isToSingleTaskUnlock = false;

				} else {
					Logger.status("Refresh next nodes candidates request not forwarded. Aborting operation.");
				}

				// For the reminder of the operation, we ensure no new
				// Finger candidates will be accepted for the old task.
				// (i.e. possibly "late" responses shall be discarded)
				self.tasksLock();
				isToTasksUnlock = true;

				self.getPendingTasks().remove(taskId);

				self.tasksUnlock();
				isToTasksUnlock = false;

				if (allClear) {

					self.nextNodesCandidatesLock();
					isToNextNodesCandidatesUnlock = true;

					candidatesList = self.getNextNodesCandidates();

					// if candidates is empty, may as well leave the old next
					// nodes list
					if (!candidatesList.isEmpty()) {

						candidatesList.sort(new Comparator<FingerResponseDTO>() {

							@Override
							public int compare(FingerResponseDTO o1, FingerResponseDTO o2) {
								// Decreasing order
								return -((Integer) o1.getExtras()).compareTo((Integer) o2.getExtras());
							}

						});

						nextNodesTable = self.getNextNodesEmptyListType();

						IdentityDTO id;
						for (FingerResponseDTO dto : candidatesList) {
							id = dto.getResponseIdentity();
							Logger.status("New next nodes: added node " + id.toString());
							nextNodesTable.add(new Identity(id.getAddress(), id.getKey()));
						}

						self.nextNodesWrLock();
						isToNextNodesWrUnlock = true;

						self.setNextNodes(nextNodesTable);
						nextNodesTable = null;

						self.nextNodesWrUnlock();
						isToNextNodesWrUnlock = false;

						self.clearNextNodesCandidates();
					}

					self.nextNodesCandidatesUnlock();
					isToNextNodesCandidatesUnlock = false;
				}

				self.refreshNextNodesLock();
				isToRefreshUnlock = true;
				self.setRefreshingNextNodes(false);
				self.refreshNextNodesUnlock();
				isToRefreshUnlock = false;

			}

		} catch (Exception e) {
			if (isToIdentityRdUnlock)
				self.identityRdUnlock();
			if (isToRefreshUnlock)
				self.refreshNextNodesUnlock();
			if (isToNextNodesCandidatesUnlock)
				self.nextNodesCandidatesUnlock();
			if (isToNextNodesRdUnlock)
				self.nextNodesRdUnlock();
			if (isToSingleTaskUnlock) {
				task.taskUnlock();
				if (!isToTasksUnlock) {
					self.tasksLock();
					isToTasksUnlock = true;
				}
				self.getPendingTasks().remove(task.getTaskId());
				// self.tasksUnlock() in the next if statement
			}
			if (isToTasksUnlock)
				self.tasksUnlock();
			if (isToNextNodesWrUnlock)
				self.nextNodesWrUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
		}

	}
}
