/*
 * @author Dan&Dan
 */
package capri.fabiani.components;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.springframework.stereotype.Component;

import capri.fabiani.configuration.Constants;
import capri.fabiani.extra.interfaces.FingerTableReaderLocker;
import capri.fabiani.extra.interfaces.NextNodesReaderLocker;
import capri.fabiani.extra.interfaces.RedundancyWriterLocker;
import capri.fabiani.model.Identity;
import capri.fabiani.model.MasterCommitMgtDatum;
import capri.fabiani.model.RedundancyIdentity;
import capri.fabiani.model.RedundancyManagementData;
import capri.fabiani.model.rest.FingerResponseDTO;

/**
 * The Class WorkerNode.
 */
@Component
public class WorkerNode implements NextNodesReaderLocker, FingerTableReaderLocker, RedundancyWriterLocker {
	
//	private static WorkerNode theNode = null;
	
	/** The status code. */
//0: uninitialized - 1: initialized, alone - 2: initialized, not alone
	private int statusCode;
	
	/** The status code lock. */
	private ReadWriteLock statusCodeLock;
	
	/** The identity. */
	private Identity identity = null;
	
	/** The identity lock. */
	private ReadWriteLock identityLock;
	
	/** The predecessor lock. */
	private ReadWriteLock predecessorLock;
	
	/** The predecessor. */
	private Identity predecessor;
	
	/** The finger table. */
	//Chord finger table. It contains all fingers, including the successor.
	private List<Identity> fingerTable;
	
	/** The finger lock. */
	private ReadWriteLock fingerLock;
	
	/** The finger candidates. */
	private Map<Integer, Identity>fingerCandidates;
	
	/** The finger candidates lock. */
	private Lock fingerCandidatesLock;
	
	/** The refreshing ft. */
	private boolean refreshingFt;
	
	/** The refresh ft lock. */
	private Lock refreshFtLock;
	
	/** The redundancy table. */
	//Table with replica nodes. It includes the current node as its first element.
	private List<RedundancyIdentity> redundancyTable;
	
	/** The redundancy lock. */
	private ReadWriteLock redundancyLock;
	
	/** The replicas to dismiss. */
	//List with replica nodes to dismiss.
	private List<RedundancyIdentity> replicasToDismiss;
	
	/** The replicas to dismiss lock. */
	private Lock replicasToDismissLock;
	
	/** The next nodes candidates. */
	private List<FingerResponseDTO> nextNodesCandidates;
	
	/** The next nodes candidates lock. */
	private Lock nextNodesCandidatesLock;
	
	/** The next nodes. */
	private List<Identity> nextNodes;
	
	/** The next nodes lock. */
	private ReadWriteLock nextNodesLock;
	
	/** The employers table. */
	//Table with nodes to which the current node is a slave.
	private Map<Long, RedundancyManagementData> employersTable;
	
	/** The employers lock. */
	private ReadWriteLock employersLock;
	
	/** The refreshing next nodes. */
	private boolean refreshingNextNodes;
	
	/** The refresh redundancy lock. */
	private Lock refreshRedundancyLock;
	
	/** The commit index lock. */
	private Lock commitIndexLock;
	
	/** The commit index. */
	private int commitIndex;
	
	/** The master commit data lock. */
	private ReadWriteLock masterCommitDataLock;
	
	/** The master commit data. */
	private Map<Integer, MasterCommitMgtDatum> masterCommitData;
	
	/** The keys in wait for a commit*/
	private Set<Long> keysPendingCommit;
	
	/** Lock for the keys under commit*/
	private Lock keysPendingCommitLock;
	
	/** The task lock. */
	//Tasks
	private Lock taskLock;
	
	/** The pending tasks. */
	private Map<Integer, PendingTask> pendingTasks;
	
	/** The task index. */
	private int taskIndex;
	
	/** The retirement lock. */
	//Manage retirement requests
	private Lock retirementLock;
	
	/** The retirement condition. */
	private Condition retirementCondition;
	
	/** The retirement signaled. */
	private boolean retirementSignaled;
	
	/** The retiree key. */
	private Long retireeKey;					//Can give vote if null
	
	/** The retirement voting timestamp. */
	private long retirementVotingTimestamp;
	
	/** The retirement council. */
	private HashMap<Long, Identity> retirementCouncil;
	

	/**
	 * Instantiates a new worker node.
	 */
	private WorkerNode() {		
		statusCode = 0;
		statusCodeLock = new ReentrantReadWriteLock();
		
		fingerTable = new LinkedList<Identity>();
		fingerLock = new ReentrantReadWriteLock(true);
		
		fingerCandidates = new HashMap<Integer, Identity>();
		fingerCandidatesLock = new ReentrantLock(true);
		
		refreshingFt = false;
		refreshFtLock = new ReentrantLock();
		
		redundancyTable = new LinkedList<RedundancyIdentity>();
		redundancyLock = new ReentrantReadWriteLock(true);
		
		replicasToDismiss = new LinkedList<RedundancyIdentity>();
		replicasToDismissLock = new ReentrantLock();
		
		nextNodesCandidates = new LinkedList<FingerResponseDTO>();
		nextNodesCandidatesLock = new ReentrantLock(true);
		
		nextNodes = new LinkedList<Identity>();
		nextNodesLock = new ReentrantReadWriteLock(true);
		
		employersTable = new HashMap<Long, RedundancyManagementData>(Constants.REDUNDANCY_LEVEL);
		employersLock = new ReentrantReadWriteLock(true);
		
		refreshingNextNodes = false;
		refreshRedundancyLock = new ReentrantLock(true);
		
		commitIndexLock = new ReentrantLock(true);
		commitIndex = 1;
		
		masterCommitDataLock = new ReentrantReadWriteLock(true);
		masterCommitData = new HashMap<Integer, MasterCommitMgtDatum>();
		
		keysPendingCommit = new HashSet<Long>();
		keysPendingCommitLock = new ReentrantLock();
		
		pendingTasks = new HashMap<Integer, PendingTask>();
		taskLock = new ReentrantLock(true);
		taskIndex = 1;
		
		predecessorLock = new ReentrantReadWriteLock(true);
	
		identityLock = new ReentrantReadWriteLock(true);
		
		retirementLock = new ReentrantLock(true);
		retirementCondition = retirementLock.newCondition();
		retirementCouncil = new HashMap<Long, Identity>();
		retirementSignaled = false;
	}
	
//	//Obsolete?
//	public static WorkerNode getWorkerNode() {
//		if(theNode == null)
//			new WorkerNode();
//		return theNode;
//	}
	
 	/**
 * Gets the identity.
 *
 * @return the identity
 */
public Identity getIdentity() {
		return identity;
	}

	/**
	 * Sets the identity.
	 *
	 * @param identity the new identity
	 */
	public void setIdentity(Identity identity) {
		this.identity = identity;
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.FingerTableReaderLocker#getFingerTable()
	 */
	public List<Identity> getFingerTable() {
		return this.fingerTable;
	}
	
	/**
	 * Sets the finger table.
	 *
	 * @param ft the new finger table
	 */
	public void setFingerTable(List<Identity> ft) {
		this.fingerTable = ft;
	}
	
	/**
	 * Gets the new empty finger table.
	 *
	 * @return a List<Identity> of the same type as the fingerTable (i.e. a LinkedList as of the current implementation)
	 */
	public List<Identity> getNewEmptyFingerTable() {
		return new LinkedList<Identity>();
	}

	/**
	 * Gets the predecessor.
	 *
	 * @return the predecessor
	 */
	public Identity getPredecessor() {
		return this.predecessor;
	}
	
	/**
	 * Sets the predecessor.
	 *
	 * @param predecessor the new predecessor
	 */
	public void setPredecessor(Identity predecessor) {
		this.predecessor = predecessor;
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.RedundancyWriterLocker#getRedundancyTable()
	 */
	public List<RedundancyIdentity> getRedundancyTable() {
		return this.redundancyTable;
	}
	
	/**
	 * Sets the redundancy table.
	 *
	 * @param redTable the new redundancy table
	 */
	public void setRedundancyTable(List<RedundancyIdentity> redTable) {
		this.redundancyTable = redTable;
	}
	
	/**
	 * Gets the replicas to dismiss.
	 *
	 * @return the replicas to dismiss
	 */
	public List<RedundancyIdentity> getReplicasToDismiss() {
		return this.replicasToDismiss;
	}
	
	/**
	 * Gets the employers table.
	 *
	 * @return the employers table
	 */
	public Map<Long, RedundancyManagementData> getEmployersTable() {
		return this.employersTable;
	}
	
	/**
	 * Gets the commit index.
	 *
	 * @return the commit index
	 */
	public int getCommitIndex() {
		return this.commitIndex;
	}
	
	/**
	 * Sets the commit index.
	 *
	 * @param commitIndex the new commit index
	 */
	public void setCommitIndex(int commitIndex) {
		this.commitIndex = commitIndex;
	}
	
	/**
	 * Gets the master commit mgt data.
	 *
	 * @return the master commit mgt data
	 */
	public Map<Integer, MasterCommitMgtDatum> getMasterCommitMgtData() {
		return this.masterCommitData;
	}
	
	public Set<Long> getKeysPendingCommit() {
		return this.keysPendingCommit;
	}
	
	/**
 * Gets the status code.
 *
 * @return the status code
 */
public int getStatusCode() {
		return statusCode;
	}
	
	/**
	 * Sets the status code.
	 *
	 * @param statusCode the new status code
	 */
	public void setStatusCode(int statusCode) {
		if(statusCode < 0)
			this.statusCode = 0;
		else if(statusCode > 2)
			this.statusCode = 2;
		else
			this.statusCode = statusCode;
	}
	
	/**
	 * Gets the task index.
	 *
	 * @return the task index
	 */
	public int getTaskIndex() {
		return this.taskIndex;
	}
	
	/**
	 * Sets the task index.
	 *
	 * @param taskIndex the new task index
	 */
	public void setTaskIndex(int taskIndex) {
		this.taskIndex = taskIndex;
	}
	
	/**
	 * Gets the pending tasks.
	 *
	 * @return the pending tasks
	 */
	public Map<Integer, PendingTask> getPendingTasks() {
		return this.pendingTasks;
	}
	
	/**
	 * Gets the next nodes candidates.
	 *
	 * @return the next nodes candidates
	 */
	public List<FingerResponseDTO> getNextNodesCandidates() {
		return this.nextNodesCandidates;
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.NextNodesReaderLocker#getNextNodes()
	 */
	public List<Identity> getNextNodes() {
		return this.nextNodes;
	}
	
	/**
	 * Returns an empty list of the same kind desired for nextNodes (LinkedList).
	 *
	 * @return the next nodes empty list type
	 */
	public List<Identity> getNextNodesEmptyListType() {
		return new LinkedList<Identity>();
	}
	
	/**
	 * Sets the next nodes.
	 *
	 * @param nextNodes the new next nodes
	 */
	public void setNextNodes(List<Identity> nextNodes) {
		this.nextNodes = nextNodes;
	}
	
	/**
	 * Gets the finger candidates.
	 *
	 * @return the finger candidates
	 */
	public Map<Integer, Identity> getFingerCandidates() {
		return fingerCandidates;
	}
	
	/**
	 * Gets the retirement candidate key.
	 *
	 * @return the retirement candidate key
	 */
	public Long getRetirementCandidateKey() {
		return retireeKey;
	}
	
	/**
	 * Sets the retirement candidate key.
	 *
	 * @param key the new retirement candidate key
	 */
	public void setRetirementCandidateKey(Long key) {
		this.retireeKey = key;
	}
	
	/**
	 * Gets the retirement voting timestamp.
	 *
	 * @return the retirement voting timestamp
	 */
	public long getRetirementVotingTimestamp() {
		return retirementVotingTimestamp;
	}

	/**
	 * Sets the retirement voting timestamp.
	 *
	 * @param retirementVotingTimestamp the new retirement voting timestamp
	 */
	public void setRetirementVotingTimestamp(long retirementVotingTimestamp) {
		this.retirementVotingTimestamp = retirementVotingTimestamp;
	}
	
	/**
	 * Gets the retirement council.
	 *
	 * @return the retirement council
	 */
	public Map<Long, Identity> getRetirementCouncil() {
		return this.retirementCouncil;
	}
	
	/**
	 * Clears the retirementCouncil Map<Long, Identity> by instantiating a new map.
	 */
	public void clearRetirementCouncil() {
		this.retirementCouncil = new HashMap<Long, Identity>();
	}
	
	/**
	 * Checks if is refreshing finger table.
	 *
	 * @return true, if is refreshing finger table
	 */
	public boolean isRefreshingFingerTable() {
		return refreshingFt;
	}
	
	/**
	 * Sets the refreshing finger table.
	 *
	 * @param refreshing the new refreshing finger table
	 */
	public void setRefreshingFingerTable(boolean refreshing) {
		this.refreshingFt = refreshing;
	}
	
	/**
	 * Checks if is refreshing next nodes.
	 *
	 * @return true, if is refreshing next nodes
	 */
	public boolean isRefreshingNextNodes() {
		return refreshingNextNodes;
	}
	
	/**
	 * Sets the refreshing next nodes.
	 *
	 * @param refreshing the new refreshing next nodes
	 */
	public void setRefreshingNextNodes(boolean refreshing) {
		this.refreshingNextNodes = refreshing;
	}

	/**
	 * Commit index lock.
	 */
	public void commitIndexLock() {
		commitIndexLock.lock();
	}
	
	/**
	 * Commit index unlock.
	 */
	public void commitIndexUnlock() {
		commitIndexLock.unlock();
	}
	
	/**
	 * Employers rd lock.
	 */
	public void employersRdLock() {
		this.employersLock.readLock().lock();
	}
	
	/**
	 * Employers rd unlock.
	 */
	public void employersRdUnlock() {
		this.employersLock.readLock().unlock();
	}
	
	/**
	 * Employers wr lock.
	 */
	public void employersWrLock() {
		this.employersLock.writeLock().lock();
	}
	
	/**
	 * Employers wr unlock.
	 */
	public void employersWrUnlock() {
		this.employersLock.writeLock().unlock();
	}
	
	/**
	 * Identity rd lock.
	 */
	public void identityRdLock() {
		identityLock.readLock().lock();
	}
	
	/**
	 * Identity rd unlock.
	 */
	public void identityRdUnlock() {
		identityLock.readLock().unlock();
	}
	
	/**
	 * Identity wr lock.
	 */
	public void identityWrLock() {
		identityLock.writeLock().lock();
	}
	
	/**
	 * Identity wr unlock.
	 */
	public void identityWrUnlock() {
		identityLock.writeLock().unlock();
	}
	
	/**
	 * Next nodes candidates lock.
	 */
	public void nextNodesCandidatesLock() {
		nextNodesCandidatesLock.lock();
	}
	
	/**
	 * Next nodes candidates unlock.
	 */
	public void nextNodesCandidatesUnlock() {
		nextNodesCandidatesLock.unlock();
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.NextNodesReaderLocker#nextNodesRdLock()
	 */
	public void nextNodesRdLock() {
		nextNodesLock.readLock().lock();
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.NextNodesReaderLocker#nextNodesRdUnlock()
	 */
	public void nextNodesRdUnlock() {
		nextNodesLock.readLock().unlock();
	}
	
	/**
	 * Next nodes wr lock.
	 */
	public void nextNodesWrLock() {
		nextNodesLock.writeLock().lock();
	}
	
	/**
	 * Next nodes wr unlock.
	 */
	public void nextNodesWrUnlock() {
		nextNodesLock.writeLock().unlock();
	}
	
	/**
	 * Finger candidates lock.
	 */
	public void fingerCandidatesLock() {
		fingerCandidatesLock.lock();
	}
	
	/**
	 * Finger candidates unlock.
	 */
	public void fingerCandidatesUnlock() {
		fingerCandidatesLock.unlock();
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.FingerTableReaderLocker#fingerRdLock()
	 */
	public void fingerRdLock() {
		fingerLock.readLock().lock();
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.FingerTableReaderLocker#fingerRdUnlock()
	 */
	public void fingerRdUnlock() {
		fingerLock.readLock().unlock();
	}
	
	/**
	 * Finger wr lock.
	 */
	public void fingerWrLock() {
		fingerLock.writeLock().lock();
	}
	
	/**
	 * Finger wr unlock.
	 */
	public void fingerWrUnlock() {
		fingerLock.writeLock().unlock();
	}
	
	/**
	 * Master commit data rd lock.
	 */
	public void masterCommitDataRdLock() {
		masterCommitDataLock.readLock().lock();
	}
	
	/**
	 * Master commit data rd unlock.
	 */
	public void masterCommitDataRdUnlock() {
		masterCommitDataLock.readLock().unlock();
	}
	
	/**
	 * Master commit data wr lock.
	 */
	public void masterCommitDataWrLock() {
		masterCommitDataLock.writeLock().lock();
	}
	
	/**
	 * Master commit data wr unlock.
	 */
	public void masterCommitDataWrUnlock() {
		masterCommitDataLock.writeLock().unlock();
	}
	
	/**
	 * Lock for the keys with a pending commit set.
	 * */
	public void keysPendingCommitLock() {
		this.keysPendingCommitLock.lock();
	}
	
	/**
	 * Unlock for the keys with a pending commit set.
	 */
	public void keysPendingCommitUnlock() {
		this.keysPendingCommitLock.unlock();
	}
	
	/**
 * Predecessor rd lock.
 */
public void predecessorRdLock() {
		predecessorLock.readLock().lock();
	}
	
	/**
	 * Predecessor rd unlock.
	 */
	public void predecessorRdUnlock() {
		predecessorLock.readLock().unlock();
	}
	
	/**
	 * Predecessor wr lock.
	 */
	public void predecessorWrLock() {
		predecessorLock.writeLock().lock();
	}
	
	/**
	 * Predecessor wr unlock.
	 */
	public void predecessorWrUnlock() {
		predecessorLock.writeLock().unlock();
	}
	
	/**
	 * Redundancy rd lock.
	 */
	public void redundancyRdLock() {
		redundancyLock.readLock().lock();
	}
	
	/**
	 * Redundancy rd unlock.
	 */
	public void redundancyRdUnlock() {
		redundancyLock.readLock().unlock();
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.RedundancyWriterLocker#redundancyWrLock()
	 */
	public void redundancyWrLock() {
		redundancyLock.writeLock().lock();
	}
	
	/* (non-Javadoc)
	 * @see capri.fabiani.extra.interfaces.RedundancyWriterLocker#redundancyWrUnlock()
	 */
	public void redundancyWrUnlock() {
		redundancyLock.writeLock().unlock();
	}
	
	/**
	 * Replicas to dismiss lock.
	 */
	public void replicasToDismissLock() {
		replicasToDismissLock.lock();
	}
	
	/**
	 * Replicas to dismiss unlock.
	 */
	public void replicasToDismissUnlock() {
		replicasToDismissLock.unlock();
	}
	
	/**
	 * Tasks lock.
	 */
	public void tasksLock() {
		taskLock.lock();
	}
	
	/**
	 * Tasks unlock.
	 */
	public void tasksUnlock() {
		taskLock.unlock();
	}
	
	/**
	 * Refresh finger table lock.
	 */
	public void refreshFingerTableLock() {
		refreshFtLock.lock();
	}
	
	/**
	 * Refresh finger table unlock.
	 */
	public void refreshFingerTableUnlock() {
		refreshFtLock.unlock();
	}
	
	/**
	 * Refresh next nodes lock.
	 */
	public void refreshNextNodesLock() {
		refreshRedundancyLock.lock();
	}
	
	/**
	 * Refresh next nodes unlock.
	 */
	public void refreshNextNodesUnlock() {
		refreshRedundancyLock.unlock();
	}
	
	/**
	 * Clear finger candidates.
	 */
	public void clearFingerCandidates() {
		this.fingerCandidates = new HashMap<Integer, Identity>();
	}
	
	/**
	 * Clear next nodes candidates.
	 */
	public void clearNextNodesCandidates() {
		this.nextNodesCandidates = new LinkedList<FingerResponseDTO>();
	}
	
	/**
	 * Clears the list of the replicas to dismiss by instantiating a new List to replace it 
	 * (Any subsequent call to the list previously returned by getReplicasToDismiss will not affect the new list of replicas to dismiss.)
	 */
	public void clearReplicasToDismiss() {
		this.replicasToDismiss = new LinkedList<RedundancyIdentity>();
	}
	
	/**
	 * Status code rd lock.
	 */
	public void statusCodeRdLock() {
		statusCodeLock.readLock().lock();
	}
	
	/**
	 * Status code rd unlock.
	 */
	public void statusCodeRdUnlock() {
		statusCodeLock.readLock().unlock();
	}
	
	/**
	 * Status code wr lock.
	 */
	public void statusCodeWrLock() {
		statusCodeLock.writeLock().lock();
	}
	
	/**
	 * Status code wr unlock.
	 */
	public void statusCodeWrUnlock() {
		statusCodeLock.writeLock().unlock();
	}
	
	/**
	 * Retirement lock.
	 */
	public void retirementLock() {
		retirementLock.lock();
	}
	
	/**
	 * Retirement unlock.
	 */
	public void retirementUnlock() {
		retirementLock.unlock();
	}
	
	/**
	 * Retirement cond signal.
	 */
	public void retirementCondSignal() {
		this.retirementSignaled = true;
		this.retirementCondition.signal();
	}
	
	/**
	 * Only one thread should wait on the retirement Condition.
	 *
	 * @param seconds the seconds
	 * @return true, if successful
	 * @throws InterruptedException the interrupted exception
	 */
	public boolean retirementCondWaitTimed(long seconds) throws InterruptedException {
		return this.retirementCondition.await(seconds, TimeUnit.SECONDS);
	}
	
	/**
	 * Only one thread should wait on the retirement Condition.
	 *
	 * @return true, if is retirement signaled
	 */
	public boolean isRetirementSignaled(){
		return retirementSignaled;
	}
	
	/**
	 * Sets the retirement signaled.
	 *
	 * @param signaled the new retirement signaled
	 */
	public void setRetirementSignaled(boolean signaled) {
		this.retirementSignaled = signaled;
	}
	
	/**
	 * The Class PendingTask.
	 */
	public static class PendingTask {
		
		/**
		 * The Enum TaskType.
		 */
		public enum TaskType {
/** The successor finger. */
SUCCESSOR_FINGER, 
 /** The successor replication. */
 SUCCESSOR_REPLICATION, 
 /** The commit master. */
 /*PREDECESSOR,*/
			COMMIT_MASTER, 
 /** The commit slave. */
 COMMIT_SLAVE};
		
		/** The task id. */
		private int taskId;
		
		/** The type. */
		private TaskType type;
		
		/** The extras. */
		private Object extras;
		
		/** The timestamp. */
		private long timestamp;
		
		/** The task lock. */
		private Lock taskLock;
		
		/** The condition. */
		private Condition condition;
		
		/** The signaled. */
		private boolean signaled = false;
		
		/**
		 * Instantiates a new pending task.
		 */
		public PendingTask() {}
		
		/**
		 * Instantiates a new pending task.
		 *
		 * @param taskId the task id
		 * @param type the type
		 * @param extras the extras
		 */
		public PendingTask(int taskId, TaskType type, Object extras) {
			this.setTaskId(taskId);
			this.setType(type);
			this.setExtras(extras);
			this.timestamp = System.currentTimeMillis();
			this.taskLock = new ReentrantLock();
			this.condition = taskLock.newCondition();
		}
		
		/**
		 * Gets the task id.
		 *
		 * @return the task id
		 */
		public int getTaskId() {
			return taskId;
		}
		
		/**
		 * Sets the task id.
		 *
		 * @param taskId the new task id
		 */
		public void setTaskId(int taskId) {
			this.taskId = taskId;
		}
		
		/**
		 * Gets the type.
		 *
		 * @return the type
		 */
		public TaskType getType() {
			return type;
		}
		
		/**
		 * Sets the type.
		 *
		 * @param type the new type
		 */
		public void setType(TaskType type) {
			this.type = type;
		}
		
		/**
		 * Gets the extras.
		 *
		 * @return the extras
		 */
		public Object getExtras() {
			return extras;
		}
		
		/**
		 * Sets the extras.
		 *
		 * @param extras the new extras
		 */
		public void setExtras(Object extras) {
			this.extras = extras;
		}		
		
		/**
		 * Gets the birth time millis.
		 *
		 * @return the birth time millis
		 */
		public long getBirthTimeMillis() {
			return timestamp;
		}
		
		/**
		 * Task lock.
		 */
		public void taskLock() {
			this.taskLock.lock();
		}
		
		/**
		 * Task unlock.
		 */
		public void taskUnlock() {
			this.taskLock.unlock();
		}
		
		/**
		 * Only one thread should wait on a task's condition.
		 */
		public void condSignal() {
			this.condition.signal();
			this.signaled = true;
		}
		
		/**
		 * Only one thread should wait on a task's condition.
		 *
		 * @param milliSeconds the milli seconds
		 * @return true, if successful
		 * @throws InterruptedException the interrupted exception
		 */
		public boolean condWaitTimed(long milliSeconds) throws InterruptedException {
			return this.condition.await(milliSeconds, TimeUnit.MILLISECONDS);
		}
		
		/**
		 * Only one thread should wait on a task's condition.
		 *
		 * @return true, if is signaled
		 */
		public boolean isSignaled(){
			return signaled;
		}
		
		/**
		 * Sets the signaled.
		 *
		 * @param signaled the new signaled
		 */
		public void setSignaled(boolean signaled) {
			this.signaled = signaled;
		}
	}
	
}
