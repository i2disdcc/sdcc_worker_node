/*
 * @author Dan&Dan
 */
package capri.fabiani.threads;

import org.springframework.http.RequestEntity;

import com.google.common.base.Throwables;

import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.util.Logger;

/**
 * Dumb runnable. It only executes an HTTP request without actually handling its
 * response.
 * 
 * @author Ultimate
 *
 */
public class HttpRequestDelegateRunnable implements Runnable {

	/** The request. */
	private RequestEntity<? extends DTO> request;

	/**
	 * Instantiates a new http request delegate runnable.
	 *
	 * @param request the request
	 */
	public HttpRequestDelegateRunnable(RequestEntity<? extends DTO> request) {
		this.request = request;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		Logger.status(
				"Sending message to " + request.getUrl().toString() + " with body " + request.getBody().toString());
		// TODO ? Nothing to do with the response? Retry or...?
		try {
			HttpEntityConfiguration.getTemplate().exchange(request, String.class);
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
		}

	}

}
