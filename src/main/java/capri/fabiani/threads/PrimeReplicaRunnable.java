/*
 * @author Dan&Dan
 */
package capri.fabiani.threads;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import com.google.common.base.Throwables;

import capri.fabiani.configuration.Constants;
import capri.fabiani.configuration.HttpEntityConfiguration;
import capri.fabiani.controller.DatumService;
import capri.fabiani.extra.interfaces.RedundancyWriterLocker;
import capri.fabiani.model.RedundancyIdentity;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.SlaveInitPrimeDTO;
import capri.fabiani.model.rest.SlavePutInChargeDTO;
import capri.fabiani.model.rest.SlavePutInChargeResponseDTO;
import capri.fabiani.model.rest.TransportKeysDTO;
import capri.fabiani.util.Logger;

/**
 * The Class PrimeReplicaRunnable.
 */
public class PrimeReplicaRunnable implements Runnable{
	
	/** The red id. */
	private RedundancyIdentity redId;
	
	/** The my key. */
	private long myKey;
	
	/** The my low end. */
	private long myLowEnd;
	
	/** The my address. */
	private String myAddress;
	
	/** The writer locker. */
	private RedundancyWriterLocker writerLocker;
	
	/** The datum service. */
	private DatumService datumService;

	/**
	 * Instantiates a new prime replica runnable.
	 *
	 * @param redId the red id
	 * @param myKey the my key
	 * @param myLowEnd the my low end
	 * @param myAddress the my address
	 * @param writerLocker the writer locker
	 * @param datumService the datum service
	 */
	public PrimeReplicaRunnable(RedundancyIdentity redId, long myKey, long myLowEnd, String myAddress, RedundancyWriterLocker writerLocker, DatumService datumService) {
		this.redId = redId;
		this.myKey = myKey;
		this.myLowEnd = myLowEnd;
		this.myAddress = myAddress;
		this.writerLocker = writerLocker;
		this.datumService = datumService;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		boolean isToReadyWrUnlock = false;
		boolean isToRedundancyWrUnlock = false;

		String baseUrl, fullUrl;
		URI uri;
		RequestEntity<SlavePutInChargeDTO> putInChargeRequest;
		RequestEntity<SlaveInitPrimeDTO> primeRequest;
		RequestEntity<TransportKeysDTO> transportRequest;

		ResponseEntity<SlavePutInChargeResponseDTO> putInChargeResponse = null;
		ResponseEntity<String> nextResponse = null;

		SlavePutInChargeDTO putInChargeDto;
		SlaveInitPrimeDTO primeDto;
		TransportKeysDTO transportDto;

		SlavePutInChargeResponseDTO putInChargeRespDto;

		long numRecords;
		int msgIndex, numMsg;
		int tries;
		int scanStep;
		long high, low;
		long meanInterval = 0;

		List<BusinessDTO> dtoList, dtoBuffer;

		List<RedundancyIdentity> redTable;

		Logger.status("Priming replica " + redId.getKey() + ", ip " + redId.getAddress());

		try {
			// Stem url
			baseUrl = Constants.HTTP_PRIMER + redId.getAddress() + Constants.APP_NAME_PATH;

			// Put in charge the replica
			fullUrl = baseUrl + Constants.SLAVE_PUT_IN_CHARGE;
			uri = new URI(fullUrl);

			putInChargeDto = new SlavePutInChargeDTO();
			putInChargeDto.setKey(myKey);
			putInChargeDto.setAddress(myAddress);
			putInChargeDto.setLowEnd(myLowEnd);

			putInChargeRequest = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(putInChargeDto);

			tries = 0;

			Logger.status("Trying to put in charge node " + redId.getKey() + " as replica");
			while (tries < Constants.RETRIES) {
				try {
					putInChargeResponse = HttpEntityConfiguration.getTemplate().exchange(putInChargeRequest,
							SlavePutInChargeResponseDTO.class);

					if (putInChargeResponse.getStatusCode() != HttpStatus.OK) {
						tries++;
					} else {
						break;
					}
				} catch (Exception e) {
					Logger.error(Throwables.getStackTraceAsString(e));
					tries++;
				}
			}
			if (tries >= Constants.RETRIES) {
				writerLocker.redundancyWrLock();
				isToRedundancyWrUnlock = true;

				redTable = writerLocker.getRedundancyTable();
				while (redTable.remove(redId)) {
					// Blank. Removes all entries with this RedundancyIdentity
					// key
				}

				writerLocker.redundancyWrUnlock();
				isToRedundancyWrUnlock = false;

				Logger.status("Replica " + redId.getKey() + " did not respond for " + Constants.RETRIES + " tries.");

				return;
			}

			putInChargeRespDto = putInChargeResponse.getBody();

			// Return if the replica does not require an update
			if (putInChargeRespDto.getCount() == 0) {

				Logger.status("Replica " + redId.getKey() + " does not require updates.");
				redId.readyWrLock();
				isToReadyWrUnlock = true;

				redId.setReady(true);

				redId.readyWrUnlock();
				isToReadyWrUnlock = false;

				return;
			}

			// Determine how many messages it's going to require
			high = putInChargeRespDto.getUpperEnd();
			low = putInChargeRespDto.getLowerEnd();
			numRecords = datumService.countByIdRange(low, high);
			numMsg = new Double((Math.ceil(((double) numRecords) / Constants.NUMBER_OF_MAX_DATUM_TO_SEND))).intValue();
			if (numMsg > 0) {
				if (low <= high) {
					if (low == high) {
						Logger.warning("PutInCharge lower end (" + low + ") equals upper end (" + high + ").");
					}
					meanInterval = (high - low) / numMsg + 1;
				} else {
					meanInterval = Constants.MAX_VALUE - low + 1;
					meanInterval += high + 1;
					meanInterval = meanInterval / numMsg + 1;
				}
			}

			Logger.status("Sending " + numRecords + " records in " + numMsg + " messages, mean search interval: "
					+ meanInterval + "...");

			// Prime the replica (send the number of messages it's going to
			// receive
			fullUrl = baseUrl + Constants.SLAVE_TRANSFER_INIT + myKey;
			uri = new URI(fullUrl);

			primeDto = new SlaveInitPrimeDTO();
			primeDto.setNumMessages(numMsg);

			primeRequest = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(primeDto);

			tries = 0;

			while (tries < Constants.RETRIES) {
				try {
					nextResponse = HttpEntityConfiguration.getTemplate().exchange(primeRequest, String.class);
					if (nextResponse.getStatusCode() != HttpStatus.OK) {
						tries++;
					} else {
						break;
					}
				} catch (Exception e) {
					Logger.error(Throwables.getStackTraceAsString(e));
					tries++;
				}
			}
			if (tries >= Constants.RETRIES) {
				writerLocker.redundancyWrLock();
				isToRedundancyWrUnlock = true;

				redTable = writerLocker.getRedundancyTable();
				while (redTable.remove(redId)) {
					// Blank. Removes all entries with this RedundancyIdentity
					// key
				}

				writerLocker.redundancyWrUnlock();
				isToRedundancyWrUnlock = false;

				Logger.status("Failed to prime replica " + redId.getKey() + ".");

				return;
			}

			msgIndex = 0;
			dtoBuffer = new LinkedList<BusinessDTO>();
			scanStep = 0;
			low = myLowEnd;

			transportDto = new TransportKeysDTO();
			transportDto.setIdSender(myKey);

			fullUrl = baseUrl + Constants.SLAVE_TRANSFER_KEYS;
			uri = new URI(fullUrl);

			while (msgIndex < numMsg) {

				Logger.status("Sending message " + msgIndex + " to replica " + redId.getKey() + " at ip "
						+ redId.getAddress());

				// Retrieve data in numMsg batches. This will distribute the
				// load of retrieval operations if keys are uniformly
				// distributed.
				while (dtoBuffer.size() < Constants.NUMBER_OF_MAX_DATUM_TO_SEND && scanStep < numMsg) {

					// Get the high end of the interval to retrieve
					high = (low + meanInterval) % Constants.MODULUS;
					if (high < 0)
						high = high + Constants.MODULUS;

					if (high > myKey) {
						high = myKey;
					}

					// Boundary zone overlapped
					if (high < low) {
						dtoBuffer.addAll(datumService.findByIdRange(low, Constants.MAX_VALUE));
						dtoBuffer.addAll(datumService.findByIdRange(Constants.MIN_VALUE, high));
					} else {
						dtoBuffer.addAll(datumService.findByIdRange(low, high));
					}

					low = (high + 1) % Constants.MODULUS;
					if (low < 0)
						low = low + Constants.MODULUS;

					scanStep++;
				}

				dtoList = new LinkedList<BusinessDTO>();

				if (msgIndex < numMsg - 1) {
					// Not the last message, we'll send a full batch of records.
					// The buffer will contain AT LEAST enough records for a
					// full batch.
					while (dtoList.size() < Constants.NUMBER_OF_MAX_DATUM_TO_SEND && !dtoBuffer.isEmpty()) {
						dtoList.add(dtoBuffer.remove(0));
					}
				} else {
					// Last message, the buffer may contain AT MOST enough
					// records for a full batch.
					dtoList.addAll(dtoBuffer);
				}

				transportDto.setMessageIndex(msgIndex);
				transportDto.setDatas(dtoList);

				transportRequest = RequestEntity.post(uri).accept(MediaType.APPLICATION_JSON).body(transportDto);

				Logger.status("TransportDTO sender id = " + transportDto.getIdSender() + ", my key = " + myKey);

				tries = 0;

				while (tries < Constants.RETRIES) {
					try {
						nextResponse = HttpEntityConfiguration.getTemplate().exchange(transportRequest, String.class);
						if (nextResponse.getStatusCode() != HttpStatus.OK) {
							tries++;
						} else {
							break;
						}
					} catch (Exception e) {
						Logger.error(Throwables.getStackTraceAsString(e));
						tries++;
					}
				}
				if (tries >= Constants.RETRIES) {
					writerLocker.redundancyWrLock();

					redTable = writerLocker.getRedundancyTable();
					while (redTable.remove(redId)) {
						// Blank. Removes all entries with this
						// RedundancyIdentity key
					}

					writerLocker.redundancyWrUnlock();

					Logger.status("Failed to prime replica " + redId.getKey() + ".");

					return;
				}

				msgIndex++;
			}

			redId.readyWrLock();
			isToReadyWrUnlock = true;

			redId.setReady(true);

			redId.readyWrUnlock();
			isToReadyWrUnlock = false;

			Logger.status("Replica " + redId.getKey() + " ready.");
		} catch (Exception e) {
			if (isToReadyWrUnlock)
				redId.readyWrUnlock();
			if (isToRedundancyWrUnlock)
				writerLocker.redundancyWrUnlock();

			Logger.error(Throwables.getStackTraceAsString(e));
		}
	}

}
