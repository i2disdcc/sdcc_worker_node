/*
 * @author Dan&Dan
 */
package capri.fabiani.threads;

import capri.fabiani.extra.interfaces.NextNodesReaderLocker;
import capri.fabiani.model.rest.FingerNextNodesDTO;
import capri.fabiani.util.FingerNavigationUtils;

/**
 * The Class FingerNextNodesForwarderRunnable.
 */
public class FingerNextNodesForwarderRunnable implements Runnable {

	/** The reader locker. */
	private NextNodesReaderLocker readerLocker;
	
	/** The dto. */
	private FingerNextNodesDTO dto;
	
	/**
	 * Instantiates a new finger next nodes forwarder runnable.
	 *
	 * @param readerLocker the reader locker
	 * @param dto the dto
	 */
	public FingerNextNodesForwarderRunnable(NextNodesReaderLocker readerLocker, FingerNextNodesDTO dto) {
		this.readerLocker = readerLocker;
		this.dto = dto;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		FingerNavigationUtils.forwardForNextNodes(readerLocker, dto);
	}
}
