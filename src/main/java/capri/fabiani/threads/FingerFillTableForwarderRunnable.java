/*
 * @author Dan&Dan
 */
package capri.fabiani.threads;

import capri.fabiani.extra.interfaces.FingerTableReaderLocker;
import capri.fabiani.model.rest.FingerFillTableDTO;
import capri.fabiani.util.FingerNavigationUtils;

/**
 * The Class FingerFillTableForwarderRunnable.
 */
public class FingerFillTableForwarderRunnable implements Runnable {

	/** The reader locker. */
	private FingerTableReaderLocker readerLocker;
	
	/** The dto. */
	private FingerFillTableDTO dto;
	
	/**
	 * Instantiates a new finger fill table forwarder runnable.
	 *
	 * @param readerLocker the reader locker
	 * @param dto the dto
	 */
	public FingerFillTableForwarderRunnable(FingerTableReaderLocker readerLocker, FingerFillTableDTO dto) {
		this.readerLocker = readerLocker;
		this.dto = dto;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		FingerNavigationUtils.forwardForFillTable(readerLocker, dto);
	}
}
