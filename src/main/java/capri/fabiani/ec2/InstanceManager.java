/*
 * @author Dan&Dan
 */
package capri.fabiani.ec2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeImagesRequest;
import com.amazonaws.services.ec2.model.DescribeImagesResult;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Image;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClient;
import com.amazonaws.services.elasticloadbalancingv2.model.RegisterTargetsRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.RegisterTargetsResult;
import com.amazonaws.services.elasticloadbalancingv2.model.TargetDescription;
import com.amazonaws.util.EC2MetadataUtils;
import com.google.common.base.Throwables;

import capri.fabiani.configuration.Constants;
import capri.fabiani.util.Logger;

/**
 * The Class InstanceManager.
 */
public class InstanceManager {

	/** The ec2. */
	private AmazonEC2 ec2;

	/** The this instance. */
	private Instance thisInstance;

	/** The image name. */
	private String imageName;

	BasicAWSCredentials credentials;

	public InstanceManager() {
		super();
		// initialize();
	}

	/**
	 * Initialize.
	 *
	 * @return the string
	 */
	public synchronized String initialize() {
		String accessKey = "", secretKey = "";

		// Get file from resources folder
		String pathToCredentials = getClass().getResource(Constants.CREDENTIAL_PATH).getPath();
		File file = new File(pathToCredentials);

		try (Scanner scanner = new Scanner(file)) {

			accessKey = scanner.nextLine();
			secretKey = scanner.nextLine();
			scanner.close();

		} catch (IOException e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			return "error in reading credential";
		}
		credentials = new BasicAWSCredentials(accessKey, secretKey);
		ec2 = new AmazonEC2Client(credentials);
		ec2.setEndpoint(Constants.EC2_ENDPOINT);
		return "OK";

	}

	/**
	 * New instance request.
	 *
	 * @return the string
	 */
	public synchronized String newInstanceRequest() {
		// TODO try/catch, in caso di errore, ritornare una stringa vuota

		String ip = "";
		// CREATE EC2 INSTANCES
		DescribeImagesRequest request = new DescribeImagesRequest();
		request = request.withOwners(Constants.OWNER_ID);
		DescribeImagesResult images = ec2.describeImages(request);
		List<Image> list = images.getImages();
		String imageId = "";
		float max = 0;
		for (Image image : list) {
			String name = image.getName();
			if (name.contains(Constants.IMAGE_AMI)) {
				String versions = name.substring(name.indexOf('-') + 1);
				try {
					float version = Float.parseFloat(versions);

					if (version > max) {
						max = version;
						imageId = image.getImageId();
						imageName = image.getName();
					}
				}

				catch (Exception e) {
					Logger.error(Throwables.getStackTraceAsString(e));
				}
			}
		}

		RunInstancesRequest runInstancesRequest = new RunInstancesRequest().withInstanceType("t2.micro")
				.withImageId(imageId).withMinCount(1).withMaxCount(1).withSecurityGroupIds("sg-fc80ab84");

		RunInstancesResult runInstances = ec2.runInstances(runInstancesRequest);

		// TAG EC2 INSTANCES
		List<Instance> instances = runInstances.getReservation().getInstances();
		int idx = 1;
		for (Instance instance : instances) {
			CreateTagsRequest createTagsRequest = new CreateTagsRequest();
			createTagsRequest.withResources(instance.getInstanceId()) //
					.withTags(new Tag("Name", "WorkerNode-" + idx)).withTags(new Tag("Type", "workernode"));
			ec2.createTags(createTagsRequest);
			ip = null;
			int i = 0;
			ArrayList<String> instanceIds = new ArrayList<String>();
			instanceIds.add(instance.getInstanceId());
			while (ip == null && i < 7) {
				DescribeInstancesRequest describeRequest = new DescribeInstancesRequest();
				describeRequest.setInstanceIds(instanceIds);
				DescribeInstancesResult result = ec2.describeInstances(describeRequest);
				List<Reservation> reservations = result.getReservations();
				if (!reservations.isEmpty()) {
					List<Instance> resInstances = reservations.get(0).getInstances();
					instance = resInstances.get(0);
					// TODO utilizzare ip privati
					ip = instance.getPrivateIpAddress();
				}
				if (ip == null)
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						Logger.error(Throwables.getStackTraceAsString(e));
					}
			}

			if (ip != null) {
				thisInstance = instance;
			}

			idx++;
		}

		return ip;
	}

	/**
	 * Gets the ip.
	 *
	 * @return the ip
	 */
	public synchronized String getIp() {
		if (thisInstance == null)
			return null;
		else
			return thisInstance.getNetworkInterfaces().get(0).getPrivateIpAddress();

	}

	/**
	 * Gets the instance id.
	 *
	 * @return the instance id
	 */
	public synchronized String getInstanceId() {
		if (thisInstance == null)
			return null;
		else
			return thisInstance.getInstanceId();
	}

	/**
	 * Shut down.
	 */
	public void shutDown() {
		Logger.warning("Shutting down this instance");
		String instanceId = EC2MetadataUtils.getInstanceId();
		TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest()
				.withInstanceIds(instanceId);
		ec2.terminateInstances(terminateInstancesRequest);

	}

	/**
	 * Shut down.
	 *
	 * @param instanceId
	 *            the instance id
	 */
	public void shutDown(String instanceId) {
		Logger.warning("Shutting down instance with id: " + instanceId);
		TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest()
				.withInstanceIds(instanceId);
		ec2.terminateInstances(terminateInstancesRequest);
	}

	/**
	 * Gets the instance public ip.
	 *
	 * @return the instance public ip
	 */
	public synchronized String getInstancePublicIP() {
		return thisInstance.getPublicIpAddress();
	}

	/**
	 * Gets the instance state.
	 *
	 * @return the instance state
	 */
	public synchronized String getInstanceState() {
		ArrayList<String> instanceIds = new ArrayList<String>();
		instanceIds.add(thisInstance.getInstanceId());
		int i = 0;
		String state = null;
		Instance instance;
		while (state == null && i < 7) {
			DescribeInstancesRequest describeRequest = new DescribeInstancesRequest();
			describeRequest.setInstanceIds(instanceIds);
			DescribeInstancesResult result = ec2.describeInstances(describeRequest);
			List<Reservation> reservations = result.getReservations();
			if (!reservations.isEmpty()) {
				List<Instance> resInstances = reservations.get(0).getInstances();
				instance = resInstances.get(0);
				// TODO utilizzare ip privati
				state = instance.getState().getName();
			}
			if (state == null)
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					Logger.error(Throwables.getStackTraceAsString(e));
				}
		}
		return state;
	}

	/**
	 * Gets the image name.
	 *
	 * @return the image name
	 */
	public String getImageName() {
		return imageName;
	}

	public boolean RegisterToAutoscaling() {

		try {
			AmazonElasticLoadBalancingClient client = new AmazonElasticLoadBalancingClient(credentials);
			// client.setRegion(Constants.AWS_REGION);
			client.setEndpoint("elasticloadbalancing.us-west-2.amazonaws.com");
			String instanceId = EC2MetadataUtils.getInstanceId();

			RegisterTargetsRequest request = new RegisterTargetsRequest().withTargetGroupArn(Constants.ARN).withTargets(
					new TargetDescription().withId(instanceId));
			RegisterTargetsResult result = client.registerTargets(request);

			if (result.getSdkHttpMetadata().getHttpStatusCode() !=200)
				return false;

			return true;
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			return false;
		}
	}

	public boolean RegisterToAutoscaling(String id) {

		try {
			AmazonElasticLoadBalancingClient client = new AmazonElasticLoadBalancingClient(credentials);
			// client.setRegion(Constants.AWS_REGION);
			client.setEndpoint("elasticloadbalancing.us-west-2.amazonaws.com");

			RegisterTargetsRequest request = new RegisterTargetsRequest().withTargetGroupArn(Constants.ARN).withTargets(
					new TargetDescription().withId(id));
			RegisterTargetsResult result = client.registerTargets(request);

			if (result.getSdkHttpMetadata().getHttpStatusCode() !=200)
				return false;

			return true;
		} catch (Exception e) {
			Logger.error(Throwables.getStackTraceAsString(e));
			return false;
		}
	}

}
