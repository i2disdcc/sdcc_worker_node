/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import capri.fabiani.components.ConfigurationDaemon;
import capri.fabiani.configuration.Constants;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.model.rest.HelloDTO;
import capri.fabiani.model.rest.TransportKeysDTO;

/**
 * The Class ConfigurationController.
 */
@Controller("init")
@RequestMapping(Constants.INITIALIZATION_CONTROLLER)
public class ConfigurationController implements DisposableBean{
	
	/** The configuration daemon. */
	@Autowired
	public ConfigurationDaemon configurationDaemon;

	/**
	 * Initialize.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.HELLO, method = RequestMethod.POST)
	public ResponseEntity<DTO> initialize(@RequestBody HelloDTO dto) {
		
		return configurationDaemon.helloMessage(dto);
	}
	
	/**
	 * Receive initial keys.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.RECEIVE_KEYS, method = RequestMethod.POST)
	ResponseEntity<DTO> receiveInitialKeys(@RequestBody TransportKeysDTO dto) {
		
		return configurationDaemon.getKeys(dto);
	}
	
	/**
	 * Test method.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ResponseEntity<String> testMethod() {
		
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.DisposableBean#destroy()
	 */
	@Override
	public void destroy() throws Exception {
		
		
	}

	
	
	
}
