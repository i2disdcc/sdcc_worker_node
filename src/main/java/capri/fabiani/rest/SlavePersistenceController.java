/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.SlavePersistenceService;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.MasterCommitDTO;
import capri.fabiani.model.rest.SlaveInitPrimeDTO;
import capri.fabiani.model.rest.SlavePutInChargeDTO;
import capri.fabiani.model.rest.SlavePutInChargeResponseDTO;
import capri.fabiani.model.rest.TransportKeysDTO;

/**
 * The Class SlavePersistenceController.
 */
@Controller
public class SlavePersistenceController {
	
	/** The sps. */
	@Autowired
	private SlavePersistenceService sps;
	
	/**
	 * Dismiss replica.
	 *
	 * @param bossKey the boss key
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.SLAVE_DISMISS+"{bossKey}", method=RequestMethod.POST)
	public ResponseEntity<String> dismissReplica(@PathVariable(value="bossKey")long bossKey) {
		sps.dismiss(bossKey);
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
	
	/**
	 * Commit.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.COMMIT_SLAVE, method = RequestMethod.POST)
	public ResponseEntity<String> commit(@RequestBody MasterCommitDTO dto) {
		sps.commit(dto);
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
	
	/**
	 * Gets the product.
	 *
	 * @param productCredentials the product credentials
	 * @param masterId the master id
	 * @return the product
	 */
	@RequestMapping(value = Constants.SLAVE_GET+"{masterId}", method = RequestMethod.POST)
	public ResponseEntity<String> getProduct(@RequestBody BusinessDTO productCredentials, @PathVariable(value="masterId") long masterId) {
		sps.getProduct(productCredentials, masterId);
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
	
	/**
	 * Put in charge.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.SLAVE_PUT_IN_CHARGE, method = RequestMethod.POST)
	public ResponseEntity<SlavePutInChargeResponseDTO> putInCharge(@RequestBody SlavePutInChargeDTO dto) {
		SlavePutInChargeResponseDTO respDto = sps.putInCharge(dto);
		return new ResponseEntity<SlavePutInChargeResponseDTO>(respDto, HttpStatus.OK);
	}
	
	/**
	 * Transfer init.
	 *
	 * @param dto the dto
	 * @param masterId the master id
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.SLAVE_TRANSFER_INIT+"{masterId}", method = RequestMethod.POST)
	public ResponseEntity<String> transferInit(@RequestBody SlaveInitPrimeDTO dto, @PathVariable(value="masterId")long masterId) {
		String res = sps.transferInit(dto, masterId);
		if(res == null) {
			return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
		}
		
		return new ResponseEntity<String>(res, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Transfer keys.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.SLAVE_TRANSFER_KEYS, method = RequestMethod.POST)
	public ResponseEntity<String> transferKeys(@RequestBody TransportKeysDTO dto) {
		String res = sps.transferKeys(dto);
		
		if(res == null) {
			return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
		}
		
		return new ResponseEntity<String>(res, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
//	public ResponseEntity<String> requestProduct(int productClass) {
//		//TODO
//		//Future developments
//		return null;
//	}
}
