/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import capri.fabiani.configuration.Constants;

/**
 * The Class FillerController.
 */
@Controller
@RequestMapping(Constants.FILL_TABLE_MAPPING)
public class FillerController {
	
	/**
	 * Fill table.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.HELLO, method = RequestMethod.POST)
	public ResponseEntity<String> fillTable(@RequestBody FillDTO dto) {
		
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
}
