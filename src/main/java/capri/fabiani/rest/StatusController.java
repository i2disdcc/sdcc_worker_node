/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.DatumService;
import capri.fabiani.controller.StatusService;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.model.rest.IdentityDTO;
import capri.fabiani.model.rest.StatusDTO;
import capri.fabiani.model.rest.StatusExtDTO;
import capri.fabiani.util.Logger;

/**
 * The Class StatusController.
 */
@Controller
public class StatusController {
	
	/** The status service. */
	@Autowired
	private StatusService statusService;
	
	//TODO togliere
	@Autowired
	private DatumService datumService;
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@RequestMapping(value=Constants.STATUS_GET, method=RequestMethod.GET)
	public ResponseEntity<StatusDTO> getStatus() {
		StatusDTO dto = statusService.getStatus();
		
		return new ResponseEntity<StatusDTO>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(value=Constants.STATUS_EXT_GET, method=RequestMethod.GET)
	public ResponseEntity<StatusExtDTO> getStatusExtended() {
		StatusExtDTO dto = statusService.getStatusExtended();
		
		return new ResponseEntity<StatusExtDTO>(dto, HttpStatus.OK);
	}
	
	/**
	 * Healt check.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.HEALTH_CHECK, method = RequestMethod.GET)
	public ResponseEntity<DTO> healtCheck(HttpServletRequest request) {
		Logger.status("Health check stack trace: " + request.getRemoteAddr());
		DTO respBody= new DTO();
		return new ResponseEntity<DTO>(respBody, HttpStatus.OK);
	}
	
	
	
	//TODO rimuovere
	@RequestMapping(value="testCreate", method=RequestMethod.POST)
	public ResponseEntity<StatusExtDTO> testCreateMethod(@RequestBody BusinessDTO dto) {
		datumService.create(dto);
		
		return new ResponseEntity<StatusExtDTO>(statusService.getStatusExtended(), HttpStatus.OK);
	}
	
	
	//TODO rimuovere
	@RequestMapping(value="testUpdate", method=RequestMethod.POST)
	public ResponseEntity<StatusExtDTO> testUpdateMethod(@RequestBody BusinessDTO dto) {
		datumService.update(dto);
		
		return new ResponseEntity<StatusExtDTO>(statusService.getStatusExtended(), HttpStatus.OK);
	}
}
