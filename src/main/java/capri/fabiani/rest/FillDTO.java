/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import capri.fabiani.model.rest.DTO;

/**
 * The Class FillDTO.
 */
public class FillDTO extends DTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The num keys. */
	@JsonInclude(Include.NON_NULL)
	private long numKeys;
	
	/** The owner. */
	@JsonInclude(Include.NON_NULL)
	private String owner;
	
	/**
	 * Gets the num keys.
	 *
	 * @return the num keys
	 */
	public long getNumKeys() {
		return numKeys;
	}
	
	/**
	 * Sets the num keys.
	 *
	 * @param numKeys the new num keys
	 */
	public void setNumKeys(long numKeys) {
		this.numKeys = numKeys;
	}
	
	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	
	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	
}
