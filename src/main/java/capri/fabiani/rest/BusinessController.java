/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.BusinessService;
import capri.fabiani.model.rest.BusinessDTO;
import capri.fabiani.model.rest.DTO;
import capri.fabiani.util.Logger;

/**
 * The Class BusinessController.
 */
@Controller
public class BusinessController {
	
	/** The business service. */
	@Autowired
	private BusinessService businessService;
	
	/**
	 * Delete produc t.
	 *
	 * @param datumCredentials the datum credentials
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.BUSINESS_DELETE_PRODUCT, method=RequestMethod.POST)
	public ResponseEntity<DTO> deleteProducT(@RequestBody BusinessDTO datumCredentials) {
		DTO body = businessService.deleteProduct(datumCredentials);
		HttpStatus status = (body.getError() == null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
		
		return new ResponseEntity<DTO>(body, status);
	}
	
	/**
	 * Gets the product.
	 *
	 * @param productCredentials the product credentials
	 * @return the product
	 */
	@RequestMapping(value = Constants.BUSINESS_GET_PRODUCT, method = RequestMethod.POST)
	public ResponseEntity<DTO> getProduct(@RequestBody BusinessDTO productCredentials) {
		DTO body = businessService.getProduct(productCredentials);
		HttpStatus status = (body.getError() == null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
		
		return new ResponseEntity<DTO>(body, status);
	}
	
	/**
	 * Update product.
	 *
	 * @param credentialsAndData the credentials and data
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.BUSINESS_UPDATE_PRODUCT, method = RequestMethod.POST)
	public ResponseEntity<DTO> updateProduct(@RequestBody BusinessDTO credentialsAndData) {
		DTO body = businessService.updateProduct(credentialsAndData);
		HttpStatus status = (body.getError() == null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
		
		return new ResponseEntity<DTO>(body, status);
	}
	
	/**
	 * Register product.
	 *
	 * @param newProduct the new product
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.BUSINESS_REGISTER_PRODUCT, method = RequestMethod.POST)
	public ResponseEntity<DTO> registerProduct(@RequestBody BusinessDTO newProduct) {
		DTO body = businessService.registerProduct(newProduct);
		HttpStatus status = (body.getError() == null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
		
		return new ResponseEntity<DTO>(body, status);
	}
	
}
