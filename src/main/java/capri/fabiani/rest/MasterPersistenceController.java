/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.MasterPersistenceService;
import capri.fabiani.model.rest.SlaveCommitDTO;

/**
 * The Class MasterPersistenceController.
 */
@Controller
public class MasterPersistenceController {
	
	/** The mps. */
	@Autowired
	private MasterPersistenceService mps;
	
	/**
	 * Commit.
	 *
	 * @param dto the dto
	 * @param commitIndex the commit index
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.COMMIT_MASTER + "{commitIndex}", method = RequestMethod.POST)
	public ResponseEntity<String> commit(@RequestBody SlaveCommitDTO dto, @PathVariable(value="commitIndex") int commitIndex) {
		mps.commit(dto, commitIndex);
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
	
	//TODO more to come
}
