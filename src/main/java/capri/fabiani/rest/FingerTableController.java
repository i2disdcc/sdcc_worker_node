/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.FingerTableService;
import capri.fabiani.model.rest.FingerFillTableDTO;
import capri.fabiani.model.rest.FingerFindPredecessorDTO;
import capri.fabiani.model.rest.FingerNextNodesDTO;
import capri.fabiani.model.rest.FingerNotifyPredecessorDTO;
import capri.fabiani.model.rest.FingerResponseDTO;
import capri.fabiani.model.rest.FingerNotifySuccessorDTO;

/**
 * The Class FingerTableController.
 */
@Controller
public class FingerTableController {
	
	/** The table service. */
	@Autowired
	private FingerTableService fTableService;
	
	
	/**
	 * Fill table.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.FINGER_FILL_TABLE, method = RequestMethod.POST)
	public ResponseEntity<String> fillTable(@RequestBody FingerFillTableDTO dto) {
		
		String res = fTableService.fillTable(dto);
		
		if(res == null)
			return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
		
		return new ResponseEntity<String>(res, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Node response.
	 *
	 * @param taskId the task id
	 * @param responseDto the response dto
	 * @return the response entity
	 */
	@RequestMapping(value=Constants.FINGER_RESPONSE+"{taskId}", method=RequestMethod.POST)
	public ResponseEntity<String> nodeResponse(@PathVariable(value="taskId") int taskId, @RequestBody FingerResponseDTO responseDto) {
		
		fTableService.nodeResponse(responseDto, taskId);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
		
//		else {
//			throw new FingerResponseException();
//		}
	}
	
	/**
	 * Notify predecessor.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value=Constants.FINGER_NOTIFY_PREDECESSOR, method=RequestMethod.POST)
	public ResponseEntity<String> notifyPredecessor(@RequestBody FingerNotifyPredecessorDTO dto) {
		String res = fTableService.notifyPredecessor(dto);;
		
		if(res == null)
			return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
		
		return new ResponseEntity<String>(res, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Notify successor.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.FINGER_NOTIFY_SUCCESSOR, method = RequestMethod.POST)
	public ResponseEntity<String> notifySuccessor(@RequestBody FingerNotifySuccessorDTO dto) {
		String res = fTableService.notifySuccessor(dto);
		
		if(res==null)
			return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
		
		return new ResponseEntity<String>(res, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Scan next nodes.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.FINGER_NEXT_NODES, method=RequestMethod.POST)
	public ResponseEntity<String> scanNextNodes(@RequestBody FingerNextNodesDTO dto) {
		
		String res = fTableService.scanNextNodes(dto);		
		
		if(res == null)
			return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
		
		return new ResponseEntity<String>(res, HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	/**
	 * Find predecessor.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.FINGER_FIND_PREDECESSOR, method = RequestMethod.POST)
	public ResponseEntity<String> findPredecessor(@RequestBody FingerFindPredecessorDTO dto) {
		String res = fTableService.findPredecessor(dto);
		
		if(res==null)
			return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
		
		return new ResponseEntity<String>(res, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Ping.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value = Constants.FINGER_PING, method = RequestMethod.GET)
	public ResponseEntity<String> ping() {
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
}
