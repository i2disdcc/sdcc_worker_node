/*
 * @author Dan&Dan
 */
package capri.fabiani.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import capri.fabiani.configuration.Constants;
import capri.fabiani.controller.RetirementService;
import capri.fabiani.model.rest.RetirementRequestDTO;
import capri.fabiani.model.rest.RetirementVoteDTO;

/**
 * The Class RetirementController.
 */
@Controller
public class RetirementController {
	
	/** The ret serv. */
	@Autowired
	private RetirementService retServ;
	
	/**
	 * May i retire.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value=Constants.RETIREMENT_REQUEST, method=RequestMethod.POST)
	public ResponseEntity<String> mayIRetire(@RequestBody RetirementRequestDTO dto) {
		retServ.mayIRetire(dto);
		
		//Whatever the result, a non-OK status is returned by handled exceptions.
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
	
	/**
	 * I am done.
	 *
	 * @param retireeKey the retiree key
	 * @return the response entity
	 */
	@RequestMapping(value=Constants.RETIREMENT_ACK+"{retireeKey}", method=RequestMethod.POST)
	public ResponseEntity<String> iAmDone(@PathVariable(value="retireeKey") long retireeKey) {
		retServ.iAmDone(retireeKey);
		
		//Whatever the result, a non-OK status is returned by handled exceptions.
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
	
	/**
	 * You may retire.
	 *
	 * @param dto the dto
	 * @return the response entity
	 */
	@RequestMapping(value=Constants.RETIREMENT_VOTE, method=RequestMethod.POST)
	public ResponseEntity<String> youMayRetire(@RequestBody RetirementVoteDTO dto) {
		retServ.youMayRetire(dto);
		
		return new ResponseEntity<String>(Constants.OK, HttpStatus.OK);
	}
}
