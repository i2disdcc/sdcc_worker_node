/*
 * @author Dan&Dan
 */
package capri.fabiani.extra.interfaces;

import java.util.List;

import capri.fabiani.model.RedundancyIdentity;

/**
 * The Interface RedundancyWriterLocker.
 */
public interface RedundancyWriterLocker {
	
	/**
	 * Gets the redundancy table.
	 *
	 * @return the redundancy table
	 */
	public List<RedundancyIdentity> getRedundancyTable();
	
	/**
	 * Redundancy wr lock.
	 */
	public void redundancyWrLock();
	
	/**
	 * Redundancy wr unlock.
	 */
	public void redundancyWrUnlock();
}
