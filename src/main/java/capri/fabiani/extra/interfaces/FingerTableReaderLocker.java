/*
 * @author Dan&Dan
 */
package capri.fabiani.extra.interfaces;

import java.util.List;

import capri.fabiani.model.Identity;

/**
 * The Interface FingerTableReaderLocker.
 */
public interface FingerTableReaderLocker extends NextNodesReaderLocker {
	
	/**
	 * Gets the finger table.
	 *
	 * @return the finger table
	 */
	public List<Identity> getFingerTable();
	
	/**
	 * Finger rd lock.
	 */
	public void fingerRdLock();
	
	/**
	 * Finger rd unlock.
	 */
	public void fingerRdUnlock();
}
