/*
 * @author Dan&Dan
 */
package capri.fabiani.extra.interfaces;

import java.util.List;

import capri.fabiani.model.Identity;

/**
 * The Interface NextNodesReaderLocker.
 */
public interface NextNodesReaderLocker {
	
	/**
	 * Next nodes rd lock.
	 */
	public void nextNodesRdLock();
	
	/**
	 * Next nodes rd unlock.
	 */
	public void nextNodesRdUnlock();
	
	/**
	 * Gets the next nodes.
	 *
	 * @return the next nodes
	 */
	public List<Identity> getNextNodes();
}
