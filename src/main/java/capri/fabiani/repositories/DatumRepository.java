/*
 * @author Dan&Dan
 */
package capri.fabiani.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import capri.fabiani.model.Datum;

/**
 * The Interface DatumRepository.
 */
@Repository
public interface DatumRepository extends MongoRepository<Datum, Long> {	
	
	/**
	 * Count by key between.
	 *
	 * @param startKey the start key
	 * @param endKey the end key
	 * @return the long
	 */
	public long countByKeyBetween(long startKey, long endKey);
	
	/**
	 * Find by key.
	 *
	 * @param key the key
	 * @return the datum
	 */
	public Datum findByKey(long key);
	
	/**
	 * Find by key between.
	 *
	 * @param startKey the start key
	 * @param endKey the end key
	 * @return the list
	 */
	public List<Datum> findByKeyBetween(long startKey, long endKey);
	
	/**
	 * Find by owner.
	 *
	 * @param owner the owner
	 * @return the list
	 */
	public List<Datum> findByOwner(String owner);
	
	/**
	 * Find by product class.
	 *
	 * @param productClass the product class
	 * @return the list
	 */
	public List<Datum> findByProductClass(long productClass);
	
	/**
	 * Find by product class and owner.
	 *
	 * @param productClass the product class
	 * @param owner the owner
	 * @return the list
	 */
	public List<Datum> findByProductClassAndOwner(long productClass, String owner);
	
	/**
	 * Delete by key.
	 *
	 * @param key the key
	 * @return the long
	 */
	public Long deleteByKey(long key);
	
	/**
	 * Delete by key between.
	 *
	 * @param startKey the start key
	 * @param endKey the end key
	 * @return the long
	 */
	public Long deleteByKeyBetween(long startKey, long endKey);
	
}
